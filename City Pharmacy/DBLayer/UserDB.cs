﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using City_Pharmacy.Models.User;

namespace City_Pharmacy.DBLayer
{
    public class UserDB
    {
        public App myApp;
        private static UserDB userDb;

        private UserDB()
        {
            myApp = (App) Application.Current;
        }

        public static UserDB getInstance()
        {


            if (userDb == null)
            {
                userDb = new UserDB();
            }
           
            return userDb;
           
            
        }


        public ObservableCollection<User> GetAllUsers()
        {
            ObservableCollection<User> allUsers = new ObservableCollection<User>();

            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand("Select " +
                                                               "U.UserID," +
                                                               "U.Name," +
                                                               "U.JoiningDate," +
                                                               "U.PhoneNumber," +
                                                               "U.Address," +
                                                               "U.CNIC," +
                                                               "U.UserName," +
                                                               "U.Password," +
                                                               "U.RoleID," +
                                                               "U.Status," +
                                                               "R.RoleID," +
                                                               "R.Name as RoleName," +
                                                               "R.Status " +

                                                               "From AllUsers U " +

                                                               "INNER JOIN Role R ON (U.RoleID = R.RoleID) " +

                                                               ";", conn))

                    {

                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            
                            while (reader.Read())
                            {

                                User u = new User();

                                u.ID = reader.GetInt32(reader.GetOrdinal(User.USER_ID));
                                u.Name = reader.GetString(reader.GetOrdinal(User.NAME));
                                u.JoiningDate = reader.GetDateTime(reader.GetOrdinal(User.JOINING_DATE));
                                u.PhoneNumber = reader.GetString(reader.GetOrdinal(User.PHONE_NUMBER));
                                u.Address = reader.GetString(reader.GetOrdinal(User.ADDRESS));
                                u.NIC = reader.GetString(reader.GetOrdinal(User.CNIC));
                                u.UserName = reader.GetString(reader.GetOrdinal(User.USER_NAME));
                                u.Password = reader.GetString(reader.GetOrdinal(User.PASSWORD));
                                u.Role = new UserRole();
                                u.Role.RoleID = reader.GetInt32(reader.GetOrdinal(User.ROLE_ID));
                                u.Role.Name = reader.GetString(reader.GetOrdinal("RoleName"));
                                u.Status = reader.GetBoolean(reader.GetOrdinal(User.STATUS));

                                allUsers.Add(u);

                            }

                            return allUsers;

                        }




                    }

                }
            }
            catch (SqlException e)
            {
                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);
                
            }
            return allUsers;

        }

        public ObservableCollection<UserRole> GetUserRoles()
        {
            ObservableCollection<UserRole> roles = new ObservableCollection<UserRole>();

            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand("Select " +
                                                               "RoleID," +
                                                               "Name, " +
                                                               "Status " +

                                                               "From Role " +

                                                               "Where Status= 1 ;", conn))

                    {


                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            UserRole role = new UserRole();
                            role.RoleID = reader.GetInt32(reader.GetOrdinal(UserRole.ROLE_ID));
                            role.Name = reader.GetString(reader.GetOrdinal(UserRole.NAME));
                            role.Status = reader.GetBoolean(reader.GetOrdinal(UserRole.STATUS));

                            roles.Add(role);
                        }

                        return roles;
                    }
                }
            }
            catch (Exception e)
            {
                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);
                
            }
            return null;
        }

        public bool GetUserByUsername(String uName)
        {
            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand("Select " +
                                                               "UserName" +
                                                               " From AllUsers" +
                                                               " Where UserName = @uName;", conn))
                    {
                        command.Parameters.Add(new SqlParameter("uName", uName));

                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);
            }

            return true;
        }

        public User GetUserByID(int id)
        {

            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                     conn.Open();
                    using (SqlCommand command = new SqlCommand("Select " +
                                                                "U.UserID," +
                                                                "U.Name," +
                                                                "U.JoiningDate," +
                                                                "U.PhoneNumber," +
                                                                "U.Address," +
                                                                "U.CNIC," +
                                                                "U.UserName," +
                                                                "U.Password," +
                                                                "U.RoleID," +
                                                                "U.Status," +
                                                                "R.RoleID," +
                                                                "R.Name as RoleName," +
                                                                "R.Status " +

                                                                "From AllUsers U " +

                                                                "INNER JOIN Role R ON (U.RoleID = R.RoleID) " +

                                                                "Where U.UserID = @Uid;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("Uid", id));

                        SqlDataReader reader = command.ExecuteReader();
                        
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                User u = new User();

                                u.ID = reader.GetInt32(reader.GetOrdinal(User.USER_ID));
                                u.Name = reader.GetString(reader.GetOrdinal(User.NAME));
                                u.JoiningDate = reader.GetDateTime(reader.GetOrdinal(User.JOINING_DATE));
                                u.PhoneNumber = reader.GetString(reader.GetOrdinal(User.PHONE_NUMBER));
                                u.Address = reader.GetString(reader.GetOrdinal(User.ADDRESS));
                                u.NIC = reader.GetString(reader.GetOrdinal(User.CNIC));
                                u.UserName = reader.GetString(reader.GetOrdinal(User.USER_NAME));
                                u.Password = reader.GetString(reader.GetOrdinal(User.PASSWORD));
                                u.Role = new UserRole();
                                u.Role.RoleID = reader.GetInt32(reader.GetOrdinal(User.ROLE_ID));
                                u.Role.Name = reader.GetString(reader.GetOrdinal("RoleName"));
                                u.Status = reader.GetBoolean(reader.GetOrdinal(User.STATUS));

                                return u;

                            }

                            return null;
                        }

                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);
               
            }
            return null;
        }

        public async Task<bool> Login(string uname, string pword)
        {
            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                   await conn.OpenAsync();
                    using (SqlCommand command = new SqlCommand("Select " +
                                                                "U.UserID," +
                                                                "U.Name," +
                                                                "U.JoiningDate," +
                                                                "U.PhoneNumber," +
                                                                "U.Address," +
                                                                "U.CNIC," +
                                                                "U.UserName," +
                                                                "U.Password," +
                                                                "U.RoleID," +
                                                                "U.Status," +
                                                                "R.RoleID," +
                                                                "R.Name as RoleName," +
                                                                "R.Status " +

                                                                "From AllUsers U " +

                                                                "INNER JOIN Role R ON (U.RoleID = R.RoleID) " +

                                                                "Where U.UserName = @UName AND U.Password = @Pword ;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("UName", uname));
                        command.Parameters.Add(new SqlParameter("Pword", pword));

                        SqlDataReader reader = await command.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                User u = new User();

                                u.ID = reader.GetInt32(reader.GetOrdinal(User.USER_ID));
                                u.Name = reader.GetString(reader.GetOrdinal(User.NAME));
                                u.JoiningDate = reader.GetDateTime(reader.GetOrdinal(User.JOINING_DATE));
                                u.PhoneNumber = reader.GetString(reader.GetOrdinal(User.PHONE_NUMBER));
                                u.Address = reader.GetString(reader.GetOrdinal(User.ADDRESS));
                                u.NIC = reader.GetString(reader.GetOrdinal(User.CNIC));
                                u.UserName = reader.GetString(reader.GetOrdinal(User.USER_NAME));
                                u.Password = reader.GetString(reader.GetOrdinal(User.PASSWORD));
                                u.Role = new UserRole();
                                u.Role.RoleID = reader.GetInt32(reader.GetOrdinal(User.ROLE_ID));
                                u.Role.Name = reader.GetString(reader.GetOrdinal("RoleName"));
                                u.Status = reader.GetBoolean(reader.GetOrdinal(User.STATUS));

                                myApp.user = u;

                            }

                            return  true;
                        }

                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(DateTime.Now+" :: "+e.Message);
                
            }
            return false;

        }

        public bool AddNewUser(User user)
        {
            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();

                    String date = user.JoiningDate.ToString("yyyy-MM-dd");

                    using (SqlCommand command = new SqlCommand("insert into AllUsers" +
                                                               "(Name, JoiningDate, PhoneNumber, Address, CNIC, UserName, Password, RoleID, Status) " +
                                                               "values" +
                                                               "(@Name, " +
                                                               "@date, " +
                                                               "@phoneNum, " +
                                                               "@Address, " +
                                                               "@NIC, " +
                                                               "@userName, " +
                                                               "@password, " +
                                                               "@RoleID, " +
                                                               "@Status" +
                                                               ");", conn))

                    {
                        command.Parameters.Add(new SqlParameter("Name", user.Name));
                        command.Parameters.Add(new SqlParameter("date", user.JoiningDate));
                        command.Parameters.Add(new SqlParameter("phoneNum", user.PhoneNumber));
                        command.Parameters.Add(new SqlParameter("Address", user.Address));
                        command.Parameters.Add(new SqlParameter("NIC", user.NIC));
                        command.Parameters.Add(new SqlParameter("userName", user.UserName));
                        command.Parameters.Add(new SqlParameter("password", user.Password));
                        command.Parameters.Add(new SqlParameter("RoleID", user.Role.RoleID));
                        command.Parameters.Add(new SqlParameter("Status", user.Status));


                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true; 
                        }
                        
                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);
                
            }
           
            return false;

        }

        public bool UpdateUser(User user)
        {
            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();

                    String date = user.JoiningDate.ToString("yyyy-MM-dd");

                    using (SqlCommand command = new SqlCommand("UPDATE AllUsers " +
                                                               " SET " +
                                                               "Name = @Name , " +
                                                               "JoiningDate = @date , " +
                                                               "PhoneNumber = @phoneNum, " +
                                                               "Address = @Address ," +
                                                               "CNIC = @NIC , " +
                                                               "UserName = @userName , " +
                                                               "Password = @password , " +
                                                               "RoleID = @RoleID ," +
                                                               "Status = @Status " +
                                                               "WHERE UserID = @uid;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("Name", user.Name));
                        command.Parameters.Add(new SqlParameter("date", user.JoiningDate));
                        command.Parameters.Add(new SqlParameter("phoneNum", user.PhoneNumber));
                        command.Parameters.Add(new SqlParameter("Address", user.Address));
                        command.Parameters.Add(new SqlParameter("NIC", user.NIC));
                        command.Parameters.Add(new SqlParameter("userName", user.UserName));
                        command.Parameters.Add(new SqlParameter("password", user.Password));
                        command.Parameters.Add(new SqlParameter("RoleID", user.Role.RoleID));
                        command.Parameters.Add(new SqlParameter("Status", user.Status));
                        command.Parameters.Add(new SqlParameter("uid", user.ID));


                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;
        }
    }
}
