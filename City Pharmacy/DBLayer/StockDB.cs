﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Purchase;
using City_Pharmacy.Models.Transaction;

namespace City_Pharmacy.DBLayer
{
    class StockDB: Database
    {
        private readonly AccountDB accountDb;
        private readonly ProductDB productDb;
        private readonly TransactionDB transDB;

        public StockDB()
        {
            accountDb = new AccountDB();
            productDb = new ProductDB();
            transDB = new TransactionDB();
        }

        private PurchaseOrder CreatePurchase(SqlDataReader reader)
        {

            PurchaseOrder purchase = new PurchaseOrder();

            purchase.ID = reader.GetInt32((reader.GetOrdinal(PurchaseOrder.PurchaseOrderID)));
            Transaction t = transDB.GetTransactionByID(reader.GetInt32(reader.GetOrdinal(PurchaseOrder.PurchaseOrderTransactionID)));
            purchase.Transaction = t;
            purchase.PurchaseList = GetItemListByPurchaseOrderID(purchase.ID);
            purchase.Status = reader.GetBoolean(reader.GetOrdinal(PurchaseOrder.PurchaseOrderStatus));         
            return purchase;
        }

        private ObservableCollection<PurchaseOrder> executeSelectQuery(string query)
        {
            ObservableCollection<PurchaseOrder> purchases = new ObservableCollection<PurchaseOrder>();
            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    purchases.Add(CreatePurchase(reader));
                                }

                                return purchases;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return purchases;
        }
        
        public ObservableCollection<PurchaseOrder> GetAllPurchases()
        {

            ObservableCollection<PurchaseOrder> purchaseOrders = new ObservableCollection<PurchaseOrder>();

            string query = $"SELECT *  FROM PurchaseOrder;";

            purchaseOrders = executeSelectQuery(query);
            
            return purchaseOrders;
        }

        public PurchaseOrder GetPurchaseByID(int ID)
        {
            ObservableCollection<PurchaseOrder> purchaseOrders = new ObservableCollection<PurchaseOrder>();

            string IDQuery = $"select * from PurchaseOrder where PurchaseOrderID = {ID};";

            var purchaseList = executeSelectQuery(IDQuery);
            

            if (purchaseList.Count > 0)
            {
               
                return purchaseList.ElementAt(0);

            }
            else
            {
                return null;
            }

            
        }

        public decimal CurrentValueOfStock()
        {
            decimal Value =0;
            String query = $"SELECT SUM(PriceOfTotalStock) AS 'CurrentValueOfStock' FROM Product";

            try
            {
                using (conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    Value = reader.GetDecimal(reader.GetOrdinal("CurrentValueOfStock"));
                                }

                                
                            }

                        }
                    }
                }

            }
            catch (SqlException e)
            {
                return -1;
            }

            return Value;
        }

        public int AddNewPurchase(ObservableCollection<PurchaseDetail> purchaseList, decimal Amount,DateTime Date)
        {

            var query = $"BEGIN TRANSACTION " +
                        $"Declare @TID int; " +
                        $"Declare @OID int; " +
                        $"Insert Into AllTransactions (TransactionDate,TransactionAmount,TransactionStatus,TransactionCrAccID,TransactionDrAccID,TransactionUserID,TransactionDescription)" +
                        $"VALUES ('{Date.ToString("yyyy-MM-dd")}',{Amount},0,5,2,{myApp.user.ID},'Purchase Transaction');" +
                        $" update Account Set AccountBalance= AccountBalance + {Amount} Where AccountID = 2; " +
                        $" update Account Set AccountBalance= AccountBalance - {Amount} Where AccountID = 5; " +
                        $"SELECT @TID = scope_identity(); " +
                        $"Insert Into PurchaseOrder (PurchaseOrderTransactionID) " +
                        $"OUTPUT inserted.PurchaseOrderID " +
                        $"VALUES (@TID); SELECT @OID = SCOPE_IDENTITY();  " +
                        $"Insert Into PurchaseDetail (PurchaseDetailPurchaseOrderID,PurchaseDetailProductID,PurchaseDetailQuantity,PurchaseDetailCost)  " +
                        $"values " +
                        $" (@OID,{purchaseList.ElementAt(0).Product.ID},{purchaseList.ElementAt(0).Quantity},{purchaseList.ElementAt(0).Cost})";

            var detailQuery = $" ";

            for (int i = 1; i < purchaseList.Count; i++)
            {
                detailQuery += $" , (@OID,{purchaseList.ElementAt(i).Product.ID},{purchaseList.ElementAt(i).Quantity},{purchaseList.ElementAt(i).Cost}) ";
            }
           
            query += detailQuery;

            query += $";";

            var updateQuery = $"";

            foreach (PurchaseDetail detail in purchaseList)
            {
                updateQuery +=
                    $" Update Product Set AvailableStock = AvailableStock+{detail.Quantity}, PurchasePrice = {detail.costPerItem} WHERE ProductID = {detail.Product.ID};  ";
            }

            query += updateQuery;

            query += $" COMMIT";

            using (conn = new SqlConnection(_connString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand(query, conn))
                {


                    int result = command.ExecuteNonQuery();
                    return result;
                }
            }
            
        }
        
        private ObservableCollection<PurchaseDetail> GetItemListByPurchaseOrderID(int OrderID)
        {
            ObservableCollection<PurchaseDetail> PurchaseList = new ObservableCollection<PurchaseDetail>();

            string query = $"Select * From PurchaseDetail WHERE PurchaseDetailPurchaseOrderID = {OrderID}";
            var DetailList = executeSelectDetailQuery(query);
            DetailList.ForEach(d => PurchaseList.Add(d));

            return PurchaseList;
        }

        private List<PurchaseDetail> executeSelectDetailQuery(string query)
        {
            var detailList = new List<PurchaseDetail>();
            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    detailList.Add(CreateDetail(reader));
                                }

                                return detailList;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return detailList;
        }

        private PurchaseDetail CreateDetail(SqlDataReader reader)
        {



            PurchaseDetail detail = new PurchaseDetail();

            detail.ID = reader.GetInt32((reader.GetOrdinal(PurchaseDetail.PurchaseDetailId)));
            detail.Product =
                productDb.SearchProductById(reader.GetInt32(reader.GetOrdinal(PurchaseDetail.PurchaseDetailProductId)));
            detail.Quantity = reader.GetInt32(reader.GetOrdinal(PurchaseDetail.PurchaseDetailQuantity));
            try
            {

                detail.costPerItem = reader.GetDecimal(reader.GetOrdinal(PurchaseDetail.PurchaseDetailCostPerItem));
            }
            catch(InvalidCastException)
            {
            }
            detail.Cost = reader.GetDecimal(reader.GetOrdinal(PurchaseDetail.PurchaseDetailCost));
            detail.Status = reader.GetBoolean(reader.GetOrdinal(PurchaseDetail.PurchaseDetailStatus));
            detail.OrderID = reader.GetInt32(reader.GetOrdinal(PurchaseDetail.PurchaseOrderId));

            return detail;
        }

        /// <summary>
        /// Create an Order opposite some order already created. 
        /// </summary>
        /// <param name="Order">Original Purchase</param>
        /// <returns>OrderID of new purchase</returns>

        public int ReturnPurchase(PurchaseOrder Order)
        {
            /*
             * Checking whether each product's available quantity is enough to return 
             */

            foreach (PurchaseDetail d in Order.PurchaseList)
            {
                if (productDb.GetAvailableStockOfProduct(d.Product.ID) < d.Quantity)
                {
                    return -1;
                }
            }

            var query = $"BEGIN TRANSACTION " +
                        $"Declare @TID int; " +
                        $"Declare @OID int; " +
                        $"Insert Into AllTransactions (TransactionDate,TransactionAmount,TransactionStatus,TransactionCrAccID,TransactionDrAccID,TransactionUserID,TransactionDescription) " +
                        $"VALUES ('{DateTime.Now.ToString("yyyy-MM-dd")}',{Order.Amount},0,2,5,{myApp.user.ID},'Purchase Transaction'); " +
                        $"" +
                        $"  update Account Set AccountBalance= AccountBalance + {Order.Amount} Where AccountID = 5; update Account Set AccountBalance= AccountBalance -{Order.Amount} Where AccountID = 2; " +
                        $"SELECT @TID = scope_identity(); " +
                        $"Insert Into PurchaseOrder (PurchaseOrderTransactionID,PurchaseOrderStatus) " +
                        $"OUTPUT inserted.PurchaseOrderID " +
                        $"VALUES (@TID,0); SELECT @OID = SCOPE_IDENTITY();  " +
                        $"Insert Into PurchaseDetail (PurchaseDetailPurchaseOrderID,PurchaseDetailProductID,PurchaseDetailQuantity,PurchaseDetailCost)  " +
                        $"values " +
                        $" (@OID,{Order.PurchaseList.ElementAt(0).Product.ID},{-(Order.PurchaseList.ElementAt(0).Quantity)},{-(Order.PurchaseList.ElementAt(0).Cost)})";

            var detailQuery = $" ";

            for (int i = 1; i < Order.PurchaseList.Count; i++)
            {
                detailQuery += $" , (@OID,{Order.PurchaseList.ElementAt(i).Product.ID},{-(Order.PurchaseList.ElementAt(0).Quantity)},{-(Order.PurchaseList.ElementAt(0).Cost)}) ";
            }

            query += detailQuery;

            query += $";";

            var updateQuery = $"";

            foreach (PurchaseDetail detail in Order.PurchaseList)
            {
                updateQuery +=
                    $" Update Product Set AvailableStock = AvailableStock-{detail.Quantity} WHERE ProductID = {detail.Product.ID};";
            }

            query += updateQuery;

            query += $" UPDATE AllTransactions SET TransactionModifiedID=@TID, TransactionStatus=0 WHERE TransactionID = {Order.Transaction.ID}; UPDATE PurchaseOrder SET PurchaseOrderStatus = 0, ModifiedOrderID = @OID WHERE PurchaseOrderID = {Order.ID}; " +
                     $" COMMIT";

            using (conn = new SqlConnection(_connString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand(query, conn))
                {


                    int result = command.ExecuteNonQuery();
                    return result;
                }
            }

        }
    }
}
