﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace City_Pharmacy.DBLayer
{
   

    public class Database
    {
        public App myApp;

        public readonly String _connString;

        public SqlConnection conn;

        public Database()
        {
            myApp = (App)Application.Current;
            _connString = myApp.ConnectionString();
            conn = myApp.SqlConnection();
        }

    }
}
