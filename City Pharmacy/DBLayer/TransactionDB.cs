﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Transaction;
using City_Pharmacy.Models.User;

namespace City_Pharmacy.DBLayer
{
    public class TransactionDB : Database
    {  
        private Transaction CreateTransaction(SqlDataReader reader)
        {
            
            Transaction transaction = new Transaction();

            transaction.ID = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_ID));
            transaction.Date = reader.GetDateTime(reader.GetOrdinal(Transaction.TRANSACTION_DATE));
            transaction.Amount = reader.GetDecimal(reader.GetOrdinal(Transaction.TRANSACTION_AMOUNT));
            transaction.Status = reader.GetBoolean(reader.GetOrdinal(Transaction.TRANSACTION_STATUS));
            transaction.Description = reader.GetString(reader.GetOrdinal(Transaction.TRANSACTION_DESCRIPTION));
            try
            {
                transaction.ModifiedID = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_MODIFIED_ID));
            }
            catch (SqlNullValueException e)
            {
                Console.WriteLine(e);
                
            }
            int userID = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_USER_ID));
            int crAccId = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_CR_ACC_ID));
            int drAccId = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_DR_ACC_ID));
            AccountDB accountDb = new AccountDB();
            var u = UserDB.getInstance().GetUserByID(userID);

            transaction.User = u;
            transaction.CrAcc = accountDb.SearchAccountByID(crAccId);
            transaction.DrAcc = accountDb.SearchAccountByID(drAccId);

            
            return transaction;
        }

        private List<Transaction> executeSelectQuery(string query)
        {
            var transactions = new List<Transaction>();
            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    transactions.Add(CreateTransaction(reader));
                                }

                                return transactions;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return transactions;
        }

        public ObservableCollection<Transaction> GetAllTransactions()
        {
            ObservableCollection<Transaction> transactions = new ObservableCollection<Transaction>();

            string query = "SELECT * FROM AllTransactions ;";

            var transactionList = executeSelectQuery(query);
            transactionList.ForEach(t => transactions.Add(t));
            



            return transactions;
        }

        internal int AddTransaction(Transaction transaction)
        {
            using (conn = new SqlConnection(_connString))
            {
                conn.Open();
                
                using (SqlCommand command = new SqlCommand(Transaction.PROC_ADD_TRANSACTION, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("desc", SqlDbType.VarChar)).Value = transaction.Description;
                    command.Parameters.Add(new SqlParameter("amount", SqlDbType.Decimal)).Value = transaction.Amount;
                    command.Parameters.Add(new SqlParameter("cID", SqlDbType.Int)).Value = transaction.CrAcc.ID;
                    command.Parameters.Add(new SqlParameter("date", SqlDbType.Date)).Value = transaction.Date;
                    command.Parameters.Add(new SqlParameter("dId", SqlDbType.Int)).Value = transaction.DrAcc.ID;
                    command.Parameters.Add(new SqlParameter("status", SqlDbType.Bit)).Value = transaction.Status;
                    command.Parameters.Add(new SqlParameter("uID", SqlDbType.Int)).Value = transaction.User.ID;



                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int newID = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_ID));
                            return newID;
                        }
                    }
                }
            }


            return 0;
        }

        public int UpdateTrasaction(Transaction trans)
        {
            using (conn)
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(Transaction.PROC_UPDATE_TRANSACTIONS,conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@modifiedID", trans.ModifiedID));
                    command.Parameters.Add(new SqlParameter("@amount",trans.Amount));
                    command.Parameters.Add(new SqlParameter("@cID",trans.CrAcc.ID));
                    command.Parameters.Add(new SqlParameter("@date",trans.Date));
                    command.Parameters.Add(new SqlParameter("@desc",trans.Description));
                    command.Parameters.Add(new SqlParameter("@dID",trans.DrAcc.ID));
                    command.Parameters.Add(new SqlParameter("@status",trans.Status));
                    command.Parameters.Add(new SqlParameter("@uID",trans.User.ID));

                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            int newID = reader.GetInt32(reader.GetOrdinal(Transaction.TRANSACTION_ID));
                            return newID;
                        }
                    }

                }
            }

            return 0;
        }

        public Transaction GetTransactionByID(Int32 id)
        {
            string query = $"SELECT * FROM AllTransactions WHERE {Transaction.TRANSACTION_ID} = {id};";

            List<Transaction> result = executeSelectQuery(query);
            if (result.Count > 0)
                return result[0];

            return null;
        }

        public Transaction GetModifiedTransaction(int id)
        {
            string query = $"SELECT * FROM AllTransactions WHERE {Transaction.TRANSACTION_MODIFIED_ID} = {id};";

            List<Transaction> result = executeSelectQuery(query);
            if (result.Count > 0)
                return result[0];

            return null;
        }

        public ObservableCollection<Transaction> GetTransactionsByAccount(Account account)
        {

            ObservableCollection<Transaction> transactions = new ObservableCollection<Transaction>();

            string query = $"SELECT * FROM AllTransactions WHERE {Transaction.TRANSACTION_CR_ACC_ID} = {account.ID} OR {Transaction.TRANSACTION_DR_ACC_ID} = {account.ID} ;";

            var transactionList = executeSelectQuery(query);

            transactionList.ForEach(t => transactions.Add(t));
            
            return transactions;
        }

        public int CancelTransaction(Transaction transaction, User user)
        {
            Transaction trans = new Transaction();


            trans.CrAcc = transaction.DrAcc;
            trans.DrAcc = transaction.CrAcc;
            trans.Amount = transaction.Amount;
            trans.Date = DateTime.Now;
            trans.Description = "Cancel transaction of TID: " + transaction.ID;
            trans.ModifiedID = transaction.ID;
            trans.User = user;
            int updatedID = this.UpdateTrasaction(trans);
            return updatedID;
        }
    }
}
