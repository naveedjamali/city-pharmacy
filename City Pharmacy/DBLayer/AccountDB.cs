﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.DBLayer
{
    public class AccountDB: Database
    {
        public AccountDB():base()
        {

        }

        private Account CreateAccount(SqlDataReader reader)
        {
            AccountType accountType = new AccountType();
            Account account = new Account();

            accountType.ID = reader.GetInt32(reader.GetOrdinal(AccountType.ACCOUNT_TYPE_ID));
            try
            {

                accountType.Name = reader.GetString(reader.GetOrdinal(AccountType.ACCOUNT_TYPE_NAME));
            }
            catch (Exception e)
            {
                

            }
            accountType.Status = reader.GetBoolean(reader.GetOrdinal(AccountType.ACCOUNT_TYPE_STATUS));

            account.AccType = accountType;
            account.ID = reader.GetInt32(reader.GetOrdinal(Account.AccountID));
            account.Name = reader.GetString(reader.GetOrdinal(Account.AccountName));
            account.Status = reader.GetBoolean(reader.GetOrdinal(Account.AccountStatus));
            try
            {
                account.Balance = reader.GetDecimal(reader.GetOrdinal(Account.AccountBalance));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                account.Balance = 0;
            }
            

            
            return account;
        }

        private List<Account> executeSelectQuery(string query)
        {
            var accounts = new List<Account>();
            try
            {
                using (SqlConnection conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    accounts.Add(CreateAccount(reader));
                                }

                                return accounts;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return accounts;
        }

        public ObservableCollection<Account> GetAllAccounts()
        {


            ObservableCollection<Account> accounts = new ObservableCollection<Account>();

            string query = "SELECT " +
                           "A." + Account.AccountID + "," +
                           "A." + Account.AccountName + "," +
                           "A." + Account.AccountStatus + ", " +
                           "A." + Account.AccountBalance+" , "+
                           "T." + AccountType.ACCOUNT_TYPE_ID + "," +
                           "T." + AccountType.ACCOUNT_TYPE_NAME + "," +
                           "T." +AccountType.ACCOUNT_TYPE_STATUS+ " " +

                           "FROM Account A " +

                           "INNER JOIN AccountType T ON ( A." + Account.AccountTypeID + " = T." +
                           AccountType.ACCOUNT_TYPE_ID + " ); ";

            var accountsList = executeSelectQuery(query);
            accountsList.ForEach(p => accounts.Add(p));

            return accounts;
        }

        public ObservableCollection<AccountType> GetAllAccountTypes()
        {

            ObservableCollection<AccountType> accountTypes = new ObservableCollection<AccountType>();

            string query = "SELECT * FROM AccountType; ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                AccountType type;
                                while (reader.Read())
                                {
                                    type = new AccountType();
                                    type.ID = reader.GetInt32(reader.GetOrdinal(AccountType.ACCOUNT_TYPE_ID));
                                    type.Name = reader.GetString(reader.GetOrdinal(AccountType.ACCOUNT_TYPE_NAME));
                                    type.Status = reader.GetBoolean(reader.GetOrdinal(AccountType.ACCOUNT_TYPE_STATUS));

                                    accountTypes.Add(type);
                                }


                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }
            return accountTypes;
        }

        public bool UpdateAccount(Account account)
        {
            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("UPDATE Account " +
                                                               " SET " +
                                                               Account.AccountName+" = @Name , " +
                                                               Account.AccountTypeID+" = @TypeID , " +
                                                               Account.AccountStatus+" = @Status " +
                                                               "WHERE "+Account.AccountID+" = @AccID;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("AccID", account.ID));
                        command.Parameters.Add(new SqlParameter("Name", account.Name));
                        command.Parameters.Add(new SqlParameter("TypeID", account.AccType.ID));
                        command.Parameters.Add(new SqlParameter("Status", account.Status));

                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;
        }

        public bool AddNewAccount(Account account)
        {
           
            try
            {
                using (SqlConnection conn = myApp.SqlConnection())
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("insert into Account " +
                                                               "( "+
                                                               Account.AccountName+" , " +
                                                               Account.AccountBalance +" , " +
                                                               Account.AccountTypeID+" , "+
                                                               Account.AccountStatus+" ) "+
                                                               
                                                               " values " +
                                                               " ( @Name , " +
                                                               " @Balance , " +
                                                               " @typeID , " +
                                                               " @Status ) ;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("Name", account.Name));
                        command.Parameters.Add(new SqlParameter("Status", account.Status));
                        command.Parameters.Add(new SqlParameter("typeID", account.AccType.ID));
                        decimal balance = 0;
                        command.Parameters.Add(new SqlParameter("Balance", balance));

                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;

        }

        public Account SearchAccountByID(int id)
        {
            
            ObservableCollection<Account> accounts = new ObservableCollection<Account>();

            var query =
                $"SELECT A.AccountID , A.AccountName , A.AccountStatus , (Select (Select SUM(TransactionAmount) From AllTransactions where TransactionDrAccID = AccountID) - (Select SUM(TransactionAmount) From AllTransactions where TransactionCrAccID = AccountID)) As AccountBalance, T.AccountTypeID, T.AccountTypeName , T.AccountTypeStatus  FROM Account A INNER JOIN AccountType T ON ( A.AccountTypeID = T.AccountTypeID ) WHERE A.AccountID ={id} ; ";

            var accountsList = executeSelectQuery(query);
            accountsList.ForEach(p => accounts.Add(p));
            if (accountsList.Count > 0)
                return accountsList[0];
            return null;
        }
        public bool NameAlreadyExists(string Name)
        {

            ObservableCollection<Account> accounts = new ObservableCollection<Account>();

            var query =
                $"SELECT * FROM Account WHERE AccountName = \'{Name}\' ; ";

            var accountsList = executeSelectQuery(query);
            accountsList.ForEach(p => accounts.Add(p));
            if (accountsList.Count > 0)
                return true;
            return false;
        }


        public ObservableCollection<Account> GetExpenseTransactions(DateTime startDate, DateTime endDate)
        {
            AccountType type = new AccountType();
            type.ID = 4;
            ObservableCollection<Account> AccountList = GetAccountListByType(type);

            foreach (Account account in AccountList)
            {
                account.Balance = GetAccountTrasactionBalance(account.ID,startDate, endDate);
            }


            return AccountList;
        }

        private decimal GetAccountTrasactionBalance(int id,DateTime startDate, DateTime endDate)
        {
            decimal result = 0;
            var query = $"declare @DAmount decimal = 0; " +
                        $"declare @CAmount decimal = 0; " +
                        $"Select @DAmount = SUM(AllTransactions.TransactionAmount)  From AllTransactions where TransactionDrAccID = {id} AND(TransactionDate >= '{startDate.ToString("yyyy-MM-dd")}' And TransactionDate <= '{endDate.ToString("yyyy-MM-dd")}') Group by TransactionDrAccID; " +
                        $"Select @CAmount = SUM(AllTransactions.TransactionAmount) From AllTransactions where TransactionCrAccID = {id} AND(TransactionDate >= '{startDate.ToString("yyyy-MM-dd")}' And TransactionDate <= '{endDate.ToString("yyyy-MM-dd")}') Group by TransactionCrAccID; " +
                        $"declare @result decimal = @DAmount - @CAmount; " +
                        $"Select @result as amount; ";

            using (conn = new SqlConnection(_connString))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            try
                            {

                                result = reader.GetDecimal(reader.GetOrdinal("amount"));
                            }
                            catch (Exception e)
                            {
                                result = 0;

                            }
                        }
                    }
                }
            }

            return result;
        }

        private ObservableCollection<Account> GetAccountListByType(AccountType type)
        {
            ObservableCollection<Account> accounts = new ObservableCollection<Account>();
            ObservableCollection<Account> All = GetAllAccounts();

            foreach (Account account in All)
            {
                if(account.AccType.ID==4)
                accounts.Add(account);
            }

            return accounts;
        }
    }
}
