﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using City_Pharmacy.Models.Sale;
using City_Pharmacy.Models.Transaction;

namespace City_Pharmacy.DBLayer
{

    public class SaleDB : Database
    {

        private readonly AccountDB accountDb;
        private readonly ProductDB productDb;
        private readonly TransactionDB transDB;

        public SaleDB()
        {
            accountDb = new AccountDB();
            productDb = new ProductDB();
            transDB = new TransactionDB();
        }

        private SaleOrder CreateSaleOrder(SqlDataReader reader)
        {
            
            int ID = reader.GetInt32(reader.GetOrdinal(SaleOrder.SaleOrderID));
            Transaction t =
                transDB.GetTransactionByID(reader.GetInt32(reader.GetOrdinal(SaleOrder.SaleOrderTransactionID)));


            SaleOrder sale = new SaleOrder();

            sale.ID = ID;
            sale.Transaction = new Transaction();
            sale.Transaction.ID = t.ID;
            sale.Transaction.Amount = t.Amount;
            sale.Transaction.CrAcc = t.CrAcc;
            sale.Transaction.Date = t.Date;
            sale.Transaction.DrAcc = t.DrAcc;
            sale.Transaction.Description = t.Description;
            sale.Transaction.Status = t.Status;
            sale.Transaction.User = t.User;
            sale.Transaction.ModifiedID = t.ModifiedID;
            //sale.Transaction = t;
            sale.SaleList = GetItemListBySaleOrderID(sale.ID);

            sale.Status = reader.GetBoolean(reader.GetOrdinal(SaleOrder.SaleOrderStatus));

            try
            {
                sale.ModifiedID = reader.GetInt32(reader.GetOrdinal(SaleOrder.ModifiedSaleID));

            }
            catch (Exception e)
            {
                sale.ModifiedID = 0;
                
            }
            

            return sale;
        }

        private ObservableCollection<SaleDetail> GetItemListBySaleOrderID(int saleId)
        {
            ObservableCollection<SaleDetail> SaleList = new ObservableCollection<SaleDetail>();
            string query = $"Select * From SaleDetail WHERE SaleDetailOrderID = {saleId} ";

            SaleList = executeSelectDetailQuery(query);
            return SaleList;
        }

        private ObservableCollection<SaleOrder> executeSelectOrderQuery(string query)
        {
            ObservableCollection<SaleOrder> sales = new ObservableCollection<SaleOrder>();
            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    sales.Add(CreateSaleOrder(reader));
                                }

                                return sales;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return sales;
        }

        public ObservableCollection<SaleOrder> GetAllSales()
        {
            ObservableCollection<SaleOrder> SaleOrders = new ObservableCollection<SaleOrder>();

            string query = $"SELECT [SaleOrderID],[SaleOrderTransactionID],[SaleOrderStatus],[ModifiedSaleID] FROM [CityPharmacyDB].[dbo].[SaleOrder]";

            SaleOrders = executeSelectOrderQuery(query);

            return SaleOrders;
        }

        private ObservableCollection<SaleDetail> executeSelectDetailQuery(string query)
        {
            var detailList = new ObservableCollection<SaleDetail>();
            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    detailList.Add(CreateDetail(reader));
                                }

                                return detailList;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return detailList;
        }

        private SaleDetail CreateDetail(SqlDataReader reader)
        {
            SaleDetail detail = new SaleDetail();
            detail.ID = reader.GetInt32(reader.GetOrdinal(SaleDetail.SaleDetailID));
            detail.Product =
                productDb.SearchProductById(reader.GetInt32(reader.GetOrdinal(SaleDetail.SaleDetailProductID)));
            detail.OrderID = reader.GetInt32(reader.GetOrdinal(SaleDetail.SaleDetailOrderID));
            detail.Quantity = reader.GetInt32(reader.GetOrdinal(SaleDetail.SaleDetailQuantity));
            detail.UnitPrice = reader.GetDecimal(reader.GetOrdinal(SaleDetail.SaleDetailPricePerUnit));
            detail.TotalAmount = reader.GetDecimal(reader.GetOrdinal(SaleDetail.SaleDetailTotalAmount));
            detail.Status = reader.GetBoolean(reader.GetOrdinal(SaleDetail.SaleDetailStatus));
            detail.Profit = reader.GetDecimal(reader.GetOrdinal(SaleDetail.PROFIT));
            detail.ProfitPerItem = reader.GetDecimal(reader.GetOrdinal(SaleDetail.PROFIT_PER_ITEM));
            

            return detail;
        }
        
        public SaleOrder GetSaleByID(int ID)
        {
            
            string IDQuery = $"select * from SaleOrder where {SaleOrder.SaleOrderID} = {ID};";

            var purchaseList = executeSelectOrderQuery(IDQuery);


            if (purchaseList.Count > 0)
            {

                return purchaseList.ElementAt(0);

            }
                return null;
            

        }

        public int AddNewSale(ObservableCollection<SaleDetail> orderDetailList, decimal totalPayableAmount, DateTime Date)
        {

            var query = $"BEGIN TRANSACTION " +
                        $"Declare @TID int; " +
                        $"Declare @OID int; " +
                        $"Insert Into AllTransactions (TransactionDate,TransactionAmount,TransactionStatus,TransactionCrAccID,TransactionDrAccID,TransactionUserID,TransactionDescription) " +
                        $"VALUES " +
                        $"('{Date.ToString("yyyy-MM-dd")}',{totalPayableAmount},0,1,5,{myApp.user.ID},'Sale Transaction'); " +
                        $" update Account Set AccountBalance= AccountBalance + {totalPayableAmount} Where AccountID = 5; " +
                        $" update Account Set AccountBalance= AccountBalance - {totalPayableAmount} Where AccountID = 1; " +
                        $"SELECT @TID = scope_identity(); " +
                        $"Insert Into SaleOrder(SaleOrderTransactionID,SaleOrderStatus) " +
                        $" " +
                        $"VALUES(@TID,1) ;" +
                        $"SELECT @OID = SCOPE_IDENTITY(); " +
                        $"Insert INTO SaleDetail (SaleDetailOrderID,SaleDetailProductID,SaleDetailQuantity,SaleDetailPricePerUnit,SaleDetailStatus,ProfitPeritem) " +
                        $"values (@OID, {orderDetailList.ElementAt(0).Product.ID},{orderDetailList.ElementAt(0).Quantity},{orderDetailList.ElementAt(0).UnitPrice},0,{orderDetailList.ElementAt(0).ProfitPerItem}) ";

            var detailQuery = $" ";

            for (int i = 1; i < orderDetailList.Count; i++)
            {
                detailQuery += $" , (@OID,{orderDetailList.ElementAt(i).Product.ID},{orderDetailList.ElementAt(i).Quantity},{orderDetailList.ElementAt(i).UnitPrice},0,{orderDetailList.ElementAt(i).ProfitPerItem}) ";
            }

            query += detailQuery+" ;";
            
            var updateQuery = $"";

            foreach (SaleDetail detail in orderDetailList)
            {
                updateQuery +=
                    $" Update Product Set AvailableStock = AvailableStock-{detail.Quantity} WHERE ProductID = {detail.Product.ID};";
            }

            query += updateQuery;

            query += $" Select @TID as LastTID,@OID as LastOID;" +
                     $"COMMIT";

            {
                using (conn = new SqlConnection(_connString))
                {
                    
                conn.Open();

                using (SqlCommand command = new SqlCommand(query, conn))
                {


                    SqlDataReader reader = command.ExecuteReader();
                    int result=-1;
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result = reader.GetInt32(reader.GetOrdinal("LastOID"));
                            break;
                        }
                    }
                    return result;
                }
                }
            }
            
        }

        public int ReturnSale(SaleOrder Order)
        {


            var query = $"BEGIN TRANSACTION" +
                        $" " +
                        $"Declare @TID int; " +
                        $"Declare @OID int; " +
                        $"Insert Into AllTransactions (TransactionDate,TransactionAmount,TransactionStatus,TransactionCrAccID,TransactionDrAccID,TransactionUserID,TransactionDescription) " +
                        $"VALUES " +
                        $"('{Order.Transaction.Date.ToString("yyyy-MM-dd")}',{Order.Amount},0,5,1,{myApp.user.ID},'Sale Return Transaction'); " +
                        $" update Account Set AccountBalance= AccountBalance + {Order.Amount} Where AccountID = 1; " +
                        $" update Account Set AccountBalance= AccountBalance - {Order.Amount} Where AccountID = 5; " +
                        $"SELECT @TID = scope_identity(); " +
                        $"Insert Into SaleOrder(SaleOrderTransactionID,SaleOrderStatus) " +
                        $"OUTPUT inserted.SaleOrderID " +
                        $"VALUES(@TID,1) " +
                        $"SELECT @OID = SCOPE_IDENTITY(); " +
                        $"Insert INTO SaleDetail (SaleDetailOrderID,SaleDetailProductID,SaleDetailQuantity,SaleDetailPricePerUnit,SaleDetailStatus,ProfitPeritem) " +
                        $"values (@OID, {Order.SaleList.ElementAt(0).Product.ID},{-Order.SaleList.ElementAt(0).Quantity},{Order.SaleList.ElementAt(0).UnitPrice},0,{Order.SaleList.ElementAt(0).ProfitPerItem}) ";

            var detailQuery = $" ";

            for (int i = 1; i < Order.SaleList.Count; i++)
            {
                detailQuery += $" , (@OID,{Order.SaleList.ElementAt(i).Product.ID},{Order.SaleList.ElementAt(i).Quantity},{Order.SaleList.ElementAt(i).UnitPrice},0,{Order.SaleList.ElementAt(i).ProfitPerItem}) ";
            }

            query += detailQuery + " ;";

            var updateQuery = $"";

            foreach (SaleDetail detail in Order.SaleList)
            {
                updateQuery +=
                    $" Update Product Set AvailableStock = AvailableStock+{detail.Quantity} WHERE ProductID = {detail.Product.ID};";
            }

            query += updateQuery;

            var BlockQuery = $" Update SaleOrder set ModifiedSaleID=@OID, SaleOrderStatus=0 Where SaleOrderID = {Order.ID};";

            query += BlockQuery;

            query += $" COMMIT";

            {
                using (conn = new SqlConnection(_connString))
                {
                     conn.Open();

                using (SqlCommand command = new SqlCommand(query, conn))
                {


                    int result = command.ExecuteNonQuery();
                    return result;
                }
                }
               
            }
            
        }

        public int PartialReturnSale(SaleOrder order, ObservableCollection<SaleDetail> orderDetailList, decimal totalRefundableAmount, DateTime Date)
        {




            var query = $"BEGIN TRANSACTION" +
                        $" " +
                        $"Declare @TID int; " +
                        $"Declare @OID int; " +
                        $"Insert Into AllTransactions (TransactionDate,TransactionAmount,TransactionStatus,TransactionCrAccID,TransactionDrAccID,TransactionUserID,TransactionDescription) " +
                        $"VALUES " +
                        $"('{Date.ToString("yyyy-MM-dd")}',{totalRefundableAmount},0,5,1,{myApp.user.ID},'Sale Return Transaction'); " +
                        $" update Account Set AccountBalance= AccountBalance + {totalRefundableAmount} Where AccountID = 1; " +
                        $" update Account Set AccountBalance= AccountBalance - {totalRefundableAmount} Where AccountID = 5; " +
                        $"SELECT @TID = scope_identity(); " +
                        $"Insert Into SaleOrder(SaleOrderTransactionID,SaleOrderStatus) " +
                        $"OUTPUT inserted.SaleOrderID " +
                        $"VALUES(@TID,1) " +
                        $"SELECT @OID = SCOPE_IDENTITY(); " +
                        $"Insert INTO SaleDetail (SaleDetailOrderID,SaleDetailProductID,SaleDetailQuantity,SaleDetailPricePerUnit,SaleDetailStatus,ProfitPeritem) " +
                        $"values (@OID, {orderDetailList.ElementAt(0).Product.ID},{-orderDetailList.ElementAt(0).Quantity},{orderDetailList.ElementAt(0).UnitPrice},0,{orderDetailList.ElementAt(0).ProfitPerItem}) ";

            var detailQuery = $" ";

            for (int i = 1; i < orderDetailList.Count; i++)
            {
                detailQuery += $" , (@OID,{orderDetailList.ElementAt(i).Product.ID},{orderDetailList.ElementAt(i).Quantity},{orderDetailList.ElementAt(i).UnitPrice},0,{orderDetailList.ElementAt(i).ProfitPerItem}) ";
            }

            query += detailQuery + " ;";

            var updateQuery = $"";

            foreach (SaleDetail detail in orderDetailList)
            {
                updateQuery +=
                    $" Update Product Set AvailableStock = AvailableStock+{detail.Quantity} WHERE ProductID = {detail.Product.ID};";
            }

            query += updateQuery;

            var BlockQuery = $" Update SaleOrder set ModifiedSaleID=@OID, SaleOrderStatus=0 Where SaleOrderID = {order.ID};";

            query += BlockQuery;

            query += $" COMMIT";

            {
                using (conn = new SqlConnection(_connString))
                {
                    
                
                conn.Open();

                using (SqlCommand command = new SqlCommand(query, conn))
                {


                    int result = command.ExecuteNonQuery();
                    return result;
                }
                }
            }
        }

        public decimal GetTotalSales(DateTime startDate, DateTime endDate)
        {
            var query =
                $"SELECT SUM(SaleDetail.SaleDetailTotalAmount) AS TotalSales " +
                $"FROM SaleDetail " +
                $"LEFT OUTER JOIN SaleOrder ON SaleDetail.SaleDetailOrderID = SaleOrder.SaleOrderID " +
                $"LEFT OUTER JOIN AllTransactions ON SaleOrder.SaleOrderTransactionID = AllTransactions.TransactionID " +
                $"Where AllTransactions.TransactionDate >= '{startDate.ToString("yyyy-MM-dd")}' AND AllTransactions.TransactionDate <= '{endDate.ToString("yyyy-MM-dd")}'; ";

            using (conn = new SqlConnection(_connString))
            {
                
            
            conn.Open();

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    decimal result = 0;

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result = reader.GetDecimal(reader.GetOrdinal("TotalSales"));
                        }


                    }

                    return result;
                }

            }
            
        }

        public decimal GetTotalSalesCost(DateTime startDate, DateTime endDate)
        {
            var query =
                $"SELECT SUM(SaleDetail.SaleDetailTotalAmount - Profit) AS TotalSalesCost " +
                $"FROM SaleDetail " +
                $"LEFT OUTER JOIN SaleOrder ON SaleDetail.SaleDetailOrderID = SaleOrder.SaleOrderID " +
                $"LEFT OUTER JOIN AllTransactions ON SaleOrder.SaleOrderTransactionID = AllTransactions.TransactionID " +
                $"Where AllTransactions.TransactionDate >= '{startDate.ToString("yyyy-MM-dd")}' AND AllTransactions.TransactionDate <= '{endDate.ToString("yyyy-MM-dd")}'; ";

            using (conn = new SqlConnection(_connString))
            {
                conn.Open();
                

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    decimal result = 0;

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result = reader.GetDecimal(reader.GetOrdinal("TotalSalesCost"));
                        }


                    }
                    return result;

                }

            }

            

        }
    }
}
