﻿using System;
using System.Collections.Generic;
using City_Pharmacy.Models;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.DBLayer
{
    
    
    public class ProductDB:Database
    {
        

        public ProductDB():base()
        {
           
        }


        private Product CreateProduct(SqlDataReader reader)
        {
            Product product = new Product();

            product.ID = reader.GetInt32(reader.GetOrdinal(Product.PRODUCT_ID));
            product.Name = reader.GetString(reader.GetOrdinal(Product.NAME));
            product.Status = reader.GetBoolean(reader.GetOrdinal(Product.STATUS));
            product.RackNum = reader.GetString(reader.GetOrdinal(Product.RACK_NUMBER));
            product.LastModified = reader.GetDateTime(reader.GetOrdinal(Product.LAST_MODIFIED));
            product.MinTemp = reader.GetInt32(reader.GetOrdinal(Product.MIN_TEMPERATURE));
            product.MaxTemp = reader.GetInt32(reader.GetOrdinal(Product.MAX_TEMPERATURE));
            product.Description = reader.GetString(reader.GetOrdinal(Product.DESCRIPTION));
            try
            {
                product.PriceOfTotalStock = reader.GetDecimal(reader.GetOrdinal(Product.PRICE_OF_TOTAL_STOCK));
            }
            catch (Exception e)
            {
                product.PriceOfTotalStock = 0;
            }
            
            try
            {
                product.Price = reader.GetDecimal(reader.GetOrdinal(Product.PRICE));
            }
            catch (Exception e)
            {
                product.Price = 0;
            }

            
            product.AvailableStock = reader.GetInt32(reader.GetOrdinal(Product.AVAILABLE_STOCK));
            product.ProductType = new ProductType();
            product.ProductType.ID = reader.GetInt32(reader.GetOrdinal(ProductType.PRODUCT_TYPE_ID));
            product.ProductType.Name = reader.GetString(reader.GetOrdinal(ProductType.NAME));
            product.Vendor = new ProductVendor();
            product.Vendor.ID = reader.GetInt32(reader.GetOrdinal(ProductVendor.VENDOR_ID));
            product.Vendor.Name = reader.GetString(reader.GetOrdinal(ProductVendor.NAME));
            try
            {
                product.PurchasePrice = reader.GetDecimal(reader.GetOrdinal(Product.PURCHASE_PRICE));
            }
            catch (SqlNullValueException e)
            {
                product.PurchasePrice = new decimal(0.0);
            }

            return product;
        }

        public ObservableCollection<Product> SearchProductByName(string productName)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            
            if (productName.Length > 0)
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand("SELECT " +
                                                               "P."+Product.PRODUCT_ID+"," +
                                                               "P."+Product.NAME+"," +
                                                               "P."+Product.STATUS+"," +
                                                               "P."+Product.RACK_NUMBER+"," +
                                                               "P."+Product.LAST_MODIFIED+"," +
                                                               "P."+Product.MIN_TEMPERATURE+"," +
                                                               "P."+Product.MAX_TEMPERATURE+"," +
                                                               "P."+Product.DESCRIPTION+"," +
                                                               "P."+Product.PRICE_OF_TOTAL_STOCK+"," +
                                                               "P."+Product.PRICE+"," +
                                                               "P."+Product.AVAILABLE_STOCK+"," +
                                                               "P."+Product.PRODUCT_TYPE_ID+"," +
                                                               "P."+Product.VENDOR_ID+"," +
                                                               "P."+Product.PURCHASE_PRICE+","+
                                                               "T."+ProductType.PRODUCT_TYPE_ID+"," +
                                                               "T."+ProductType.NAME+", " +
                                                               "V."+ProductVendor.VENDOR_ID+"," +
                                                               "V."+ProductVendor.NAME+" " +

                                                               "FROM Product P " +

                                                               "INNER JOIN ProductType T ON (P."+Product.PRODUCT_TYPE_ID+" = T."+ProductType.PRODUCT_TYPE_ID+") " +
                                                               "INNER JOIN ProductVendor V ON (P."+Product.VENDOR_ID+"  = V."+ProductVendor.VENDOR_ID+") " +

                                                               "WHERE P."+Product.NAME+" LIKE \'%" + productName+"%\'",conn))
                    {
                        
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if(reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    products.Add(CreateProduct(reader));
                                }
                            }
                            else
                            {
                                return products;
                            }
                            
                        }
                    }
                }    
            }
            else
            {
                return null ;
            }

            return products;
        }
        
        public ObservableCollection<Product> GetAllProducts()
        {

            ObservableCollection<Product> products = new ObservableCollection<Product>();

            string query = "SELECT " +
                           "P." + Product.PRODUCT_ID + "," +
                           "P." + Product.NAME + "," +
                           "P." + Product.STATUS + "," +
                           "P." + Product.RACK_NUMBER + "," +
                           "P." + Product.LAST_MODIFIED + "," +
                           "P." + Product.MIN_TEMPERATURE + "," +
                           "P." + Product.MAX_TEMPERATURE + "," +
                           "P." + Product.DESCRIPTION + "," +
                           "P." + Product.PRICE_OF_TOTAL_STOCK + "," +
                           "P." + Product.PRICE + "," +
                           "P." + Product.AVAILABLE_STOCK + "," +
                           "P." + Product.PRODUCT_TYPE_ID + "," +
                           "P." + Product.VENDOR_ID + "," +
                           "P."+Product.PURCHASE_PRICE+","+
                           "T." + ProductType.PRODUCT_TYPE_ID + "," +
                           "T." + ProductType.NAME + ", " +
                           "V." + ProductVendor.VENDOR_ID + "," +
                           "V." + ProductVendor.NAME + " " +

                           "FROM Product P " +

                           "INNER JOIN ProductType T ON (P." + Product.PRODUCT_TYPE_ID + " = T." +
                           ProductType.PRODUCT_TYPE_ID + ") " +
                           "INNER JOIN ProductVendor V ON (P." + Product.VENDOR_ID + "  = V." +
                           ProductVendor.VENDOR_ID + "); ";

            var productList = executeSelectQuery(query);
            productList.ForEach(p => products.Add(p));

            return products;

        }

        public Product SearchProductById(int productId)
        {
            var query =
                $"SELECT P.ProductID, P.ProductName,P.ProductStatus,P.PurchasePrice,P.RackNumber,P.LastModifiedDate,P.MinTemperature,P.MaxTemperature,P.ProductDescription,P.PriceOfTotalStock,P.ProductPrice,P.AvailableStock,P.ProductTypeID,P.VendorID,T.ProductTypeID,T.ProductTypeName, V.VendorID,V.ProductVendorName FROM Product P INNER JOIN ProductType T ON (P.ProductTypeID = T.ProductTypeID) INNER JOIN ProductVendor V ON (P.VendorID  = V.VendorID) WHERE P.ProductID = {productId};";

            var products = executeSelectQuery(query);

            return products.Count > 0 ? products[0] : null;
        }
        
        private List<Product> executeSelectQuery(string query)
        {
            var products = new List<Product>();
            try
            {
                using ( conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    products.Add(CreateProduct(reader));
                                }

                                return products;
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return products;
        }

        public ObservableCollection<ProductType> GetAllProductTypes()
        {

            ObservableCollection<ProductType> productTypes = new ObservableCollection<ProductType>();

            string query = "SELECT * FROM ProductType; ";

            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                ProductType type;
                                while (reader.Read())
                                {
                                    type = new ProductType();
                                    type.ID = reader.GetInt32(reader.GetOrdinal(ProductType.PRODUCT_TYPE_ID));
                                    type.Name = reader.GetString(reader.GetOrdinal(ProductType.NAME));
                                    type.Status = reader.GetBoolean(reader.GetOrdinal(ProductType.Product_Type_Status));

                                    productTypes.Add(type);
                                }

                                
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }
            return productTypes;

        }

        public ObservableCollection<ProductVendor> GetAllProductVendors()
        {

            ObservableCollection<ProductVendor> productVendor = new ObservableCollection<ProductVendor>();

            string query = "SELECT * FROM ProductVendor; ";

            try
            {
                using ( conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                 
                                while (reader.Read())
                                {
                                    ProductVendor vendor = new ProductVendor();
                                    vendor.ID = reader.GetInt32(reader.GetOrdinal(ProductVendor.VENDOR_ID));
                                    vendor.Name = reader.GetString(reader.GetOrdinal(ProductVendor.NAME));
                                    vendor.Status = reader.GetBoolean(reader.GetOrdinal(ProductVendor.Vendor_Status));

                                    productVendor.Add(vendor);
                                }


                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }
            return productVendor;

        }

        public bool UpdateProduct(Product product)
        {
            try
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("UPDATE Product " +
                                                               " SET " +
                                                               "ProductName = @Name , " +
                                                               "RackNumber = @rack , " +
                                                               "LastModifiedDate = @date, " +
                                                               "MinTemperature = @minTemp ," +
                                                               "MaxTemperature = @maxTemp , " +
                                                               "ProductDescription = @desc , " +
                                                               "ProductPrice = @ProductPrice , " +
                                                               "ProductTypeID = @TypeID ," +
                                                               "ProductStatus = @Status ," +
                                                               "VendorID = @VendorID "+
                                                               "WHERE ProductID = @ProductID;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("ProductID", product.ID));
                        command.Parameters.Add(new SqlParameter("Name", product.Name));
                        command.Parameters.Add(new SqlParameter("rack", product.RackNum));
                        command.Parameters.Add(new SqlParameter("date", product.LastModified));
                        command.Parameters.Add(new SqlParameter("minTemp", product.MinTemp));
                        command.Parameters.Add(new SqlParameter("maxTemp", product.MaxTemp));
                        command.Parameters.Add(new SqlParameter("desc", product.Description));
                        command.Parameters.Add(new SqlParameter("ProductPrice", product.Price));
                        command.Parameters.Add(new SqlParameter("TypeID", product.ProductType.ID));
                        command.Parameters.Add(new SqlParameter("Status", product.Status));
                        command.Parameters.Add(new SqlParameter("VendorID", product.Vendor.ID));


                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;
        }

        public bool UpdateStockOfProduct(int ProductID, int AddStock)
        {
            try
            {
                using (conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("UPDATE Product " +
                                                               " SET " +
                                                               "AvailableStock = AvailableStock + @AddStock , " +
                                                               "LastModifiedDate = @date "+
                                                               "WHERE ProductID = @ProductID;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("ProductID", ProductID));
                        command.Parameters.Add(new SqlParameter("AddStock", AddStock));
                        DateTime updateTime = DateTime.Now;
                        command.Parameters.Add(new SqlParameter("date", updateTime));


                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;
        }

        public bool AddNewProduct(Product product)
        {
            try
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();
                    
                    using (SqlCommand command = new SqlCommand("insert into Product " +
                                                               "(ProductName, ProductStatus, RackNumber, LastModifiedDate, MinTemperature, MaxTemperature, ProductDescription, ProductPrice, ProductTypeID, VendorID) " +
                                                               "values" +
                                                               "(@Name, " +
                                                               "@Status, " +
                                                               "@Rack, " +
                                                               "@ModifiedDate, " +
                                                               "@MinTemp, " +
                                                               "@MaxTemp, " +
                                                               "@Desc, " +
                                                               "@Price, " +
                                                               "@TypeID, " +
                                                               "@VendorID " +
                                                               ");", conn))

                    {
                        
                        command.Parameters.Add(new SqlParameter("Name", product.Name));
                        command.Parameters.Add(new SqlParameter("Status", product.Status));
                        command.Parameters.Add(new SqlParameter("Rack",product.RackNum));
                        command.Parameters.Add(new SqlParameter("ModifiedDate",product.LastModified));
                        command.Parameters.Add(new SqlParameter("Mintemp",product.MinTemp));
                        command.Parameters.Add(new SqlParameter("MaxTemp",product.MaxTemp));
                        command.Parameters.Add(new SqlParameter("Desc",product.Description));
                        command.Parameters.Add(new SqlParameter("Price",product.Price));
                        command.Parameters.Add(new SqlParameter("TypeID",product.ProductType.ID));
                        command.Parameters.Add(new SqlParameter("VendorID", product.Vendor.ID));

                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;

        }

        public bool AddNewProductType(ProductType pType)
        {
            try
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("insert into ProductType " +
                                                               "(ProductTypeName, ProductTypeStatus) " +
                                                               "values" +
                                                               "(@Name," +
                                                               "@Status);", conn))

                    {
                        command.Parameters.Add(new SqlParameter("Name", pType.Name));
                        command.Parameters.Add(new SqlParameter("Status", pType.Status));

                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;

        }

        public bool UpdateProductType(ProductType productType)
        {

            try
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("UPDATE ProductType " +
                                                               " SET " +
                                                               "ProductTypeName = @Name , " +
                                                               "ProductTypeStatus = @Status " +
                                                               "WHERE ProductTypeID = @TypeID;", conn))

                    {
                        command.Parameters.Add(new SqlParameter("TypeID", productType.ID));
                        command.Parameters.Add(new SqlParameter("Name", productType.Name));
                        command.Parameters.Add(new SqlParameter("Status", productType.Status));


                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;
        }

        public ProductType SearchProductTypeById(int id)
        {

           
            ProductType pType;
            try
            {
                using ( conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand("Select * From ProductType " +
                                                               "Where ProductTypeID = @id", conn))
                    {
                        command.Parameters.Add(new SqlParameter("id", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    pType = new ProductType();
                                    pType.ID = reader.GetInt32(reader.GetOrdinal(ProductType.PRODUCT_TYPE_ID));
                                    pType.Name = reader.GetString(reader.GetOrdinal(ProductType.NAME));
                                    pType.Status =
                                        reader.GetBoolean(reader.GetOrdinal(ProductType.Product_Type_Status));

                                    return pType;
                                }

                                
                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return null;

        }

        public bool UpdateProductVendor(ProductVendor vendor)
        {


            try
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("UPDATE ProductVendor " +
                                                               "SET " +
                                                               "ProductVendorName = @Name , " +
                                                               "ProductVendorStatus = @Status " +
                                                               " Where VendorID = @vendorID;", conn))

                    {

                        command.Parameters.Add(new SqlParameter("Name", vendor.Name));
                        command.Parameters.Add(new SqlParameter("Status", vendor.Status));
                        command.Parameters.Add(new SqlParameter("vendorID", vendor.ID));
                       
                        
                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;
        }

        public bool AddNewProductVendor(ProductVendor vendor)
        {
            try
            {
                using ( conn = new SqlConnection(_connString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("insert into ProductVendor " +
                                                               "(ProductVendorName, ProductVendorStatus) " +
                                                               "values" +
                                                               "(@Name," +
                                                               "@Status);", conn))

                    {
                        command.Parameters.Add(new SqlParameter("Name", vendor.Name));
                        command.Parameters.Add(new SqlParameter("Status", vendor.Status));

                        if (command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }

                    }
                }

            }
            catch (SqlException e)
            {

                String msg = e.Message;
                Console.WriteLine(DateTime.Now + " :: " + msg);

            }

            return false;


        }

        public ProductVendor SearchProductVendorById(int id)
        {

            ProductVendor v;
            try
            {
                using ( conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand("Select * From ProductVendor " +
                                                               "Where VendorID = @id", conn))
                    {
                        command.Parameters.Add(new SqlParameter("id", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    v = new ProductVendor();
                                    v.ID = reader.GetInt32(reader.GetOrdinal(ProductVendor.VENDOR_ID));
                                    v.Name = reader.GetString(reader.GetOrdinal(ProductVendor.NAME));
                                    v.Status =
                                        reader.GetBoolean(reader.GetOrdinal(ProductVendor.Vendor_Status));

                                    return v;
                                }


                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return null;

        }

        public int GetAvailableStockOfProduct(int ProductID)
        {


            try
            {
                using (conn = new SqlConnection(_connString))
                {

                    conn.Open();
                    using (SqlCommand command = new SqlCommand($"Select AvailableStock From Product where ProductID = @id", conn))
                    {
                        command.Parameters.Add(new SqlParameter("id", ProductID));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    int Stock = reader.GetInt32(reader.GetOrdinal(Product.AVAILABLE_STOCK));
                                    
                                    return Stock;
                                }


                            }

                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }

            return -1;

        }

    }
}