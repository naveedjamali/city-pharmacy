﻿using System;

namespace City_Pharmacy.Models.Product
{
    public class Product
    {
        /**
         * Database field names in the Product Table
         */
        public const string PRODUCT_ID = "ProductID";
        public const string NAME = "ProductName";
        public const string STATUS = "ProductStatus";
        public const string RACK_NUMBER = "RackNumber";
        public const string LAST_MODIFIED = "LastModifiedDate";
        public const string MIN_TEMPERATURE = "MinTemperature";
        public const string MAX_TEMPERATURE = "MaxTemperature";
        public const string DESCRIPTION = "ProductDescription";
        public const string PRICE_OF_TOTAL_STOCK = "PriceOfTotalStock";
        public const string PRICE = "ProductPrice";
        public const string AVAILABLE_STOCK = "AvailableStock";
        public const string PRODUCT_TYPE_ID = "ProductTypeID";
        public const string VENDOR_ID = "VendorID";
        public const string PURCHASE_PRICE = "PurchasePrice";

        /**
         * class properties.
         */
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string RackNum { get; set; }
        public DateTime LastModified { get; set; }
        public int MinTemp { get; set; }
        public int MaxTemp { get; set; }
        public string Description { get; set; }
        public decimal PriceOfTotalStock { get; set; }
        public decimal Price { get; set; }
        public int AvailableStock { get; set; }
        public ProductType ProductType { get; set; }
        public ProductVendor Vendor { get; set; }
        public decimal PurchasePrice { get; set; }

        
    }
}