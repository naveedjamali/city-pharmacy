﻿namespace City_Pharmacy.Models.Product
{
    public class ProductVendor
    {
        /**
         * Table Properties;
         */
        public const string VENDOR_ID = "VendorID";
        public const string NAME = "ProductVendorName";
        public const string Vendor_Status = "ProductVendorStatus";


        /**
         * Class Properties;
         */
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}