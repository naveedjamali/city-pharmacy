﻿namespace City_Pharmacy.Models.Product
{
    public class ProductType
    {
        /**
         * Table Properties
         */
        public const string PRODUCT_TYPE_ID = "ProductTypeID";
        public const string NAME = "ProductTypeName";
        public const string Product_Type_Status = "ProductTypeStatus";

        /**
         * Class Properties
         */
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}