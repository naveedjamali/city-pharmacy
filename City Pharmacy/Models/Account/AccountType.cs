﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.Account
{
    public class AccountType
    {
        public const string ACCOUNT_TYPE_ID = "AccountTypeID";
        public const string ACCOUNT_TYPE_NAME = "AccountTypeName";
        public const string ACCOUNT_TYPE_STATUS = "AccountTypeStatus";

        public int ID { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
