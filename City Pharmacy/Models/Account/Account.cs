﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.Account
{
    public class Account
    {
        public const string AccountID = "AccountID";
        public const string AccountName = "AccountName";
        public const string AccountTypeID = "AccountTypeID";
        public const string AccountStatus = "AccountStatus";
        public const string AccountBalance = "AccountBalance";

        public int ID { get; set; }
        public string Name { get; set; }
        public AccountType AccType { get; set; }
        public bool Status { get; set; }
        public decimal Balance { get; set; }
        
    }
}
