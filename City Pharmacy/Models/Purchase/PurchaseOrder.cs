﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using City_Pharmacy.DBLayer;

namespace City_Pharmacy.Models.Purchase
{
    public class PurchaseOrder
    {
        public const string PurchaseOrderID = "PurchaseOrderID";
        public const string PurchaseOrderDate = "PurchaseOrderDate";
        public const string PurchaseOrderTransactionID = "PurchaseOrderTransactionID";
        public const string PurchaseOrderStatus = "PurchaseOrderStatus";

        
        

        public int ID { get; set; }
        public Transaction.Transaction Transaction;
        public bool Status { get; set; }
        public decimal Amount
        {
            get { return Transaction.Amount; }
        }
        public DateTime Date
        {
            get { return Transaction.Date; }
        }
        public ObservableCollection<PurchaseDetail> PurchaseList { get; set; }
    }
}
