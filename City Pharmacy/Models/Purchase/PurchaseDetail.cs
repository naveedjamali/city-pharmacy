﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.Purchase
{
    public class PurchaseDetail
    {
        public const string PurchaseDetailId = "PurchaseDetailID";
        public const string PurchaseOrderId = "PurchaseDetailPurchaseOrderID";
        public const string PurchaseDetailProductId = "PurchaseDetailProductID";
        public const string PurchaseDetailQuantity = "PurchaseDetailQuantity";
        public const string PurchaseDetailCost = "PurchaseDetailCost";
        public const string PurchaseDetailCostPerItem = "PurchaseDetailCostPerItem";
        public const string PurchaseDetailStatus = "PurchaseDetailStatus";

        public PurchaseDetail()
        {

        }

        public PurchaseDetail(PurchaseDetail p)
        {
            ID = p.ID;
            OrderID = p.OrderID;
            Product = p.Product;
            Quantity = p.Quantity;
            Cost = p.Cost;
            costPerItem = Math.Round( Cost / Quantity, 3);
            Status = p.Status;
        }

        public int ID { get; set; }

        public int OrderID
        {
            get; set;
        }
        public Product.Product Product
        {
            get; set;
        }
        public int Quantity
        {
            get; set;
        }
        public decimal Cost
        {
            get; set;
        }

        public decimal costPerItem { get; set; }

        public bool Status { get; set; }



    }

}
