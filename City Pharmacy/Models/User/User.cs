﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.User
{
    public class User
    {
        /**
         * Table Fields in the User Table
         */
        public const string USER_ID = "UserID";
        public const string NAME = "Name";
        public const string JOINING_DATE = "JoiningDate";
        public const string PHONE_NUMBER = "PhoneNumber";
        public const string ADDRESS = "Address";
        public const string CNIC = "CNIC";
        public const string USER_NAME = "UserName";
        public const string PASSWORD = "Password";
        public const string ROLE_ID = "RoleID";
        public const string STATUS = "Status";
         
        /**
         * Class properties
         */

        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime JoiningDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string NIC { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
        public bool Status { get; set; }

        public bool isAdmin()
        {
            return Role.isAdmin();
        }

        /**
         *
         */
        

    }
}
