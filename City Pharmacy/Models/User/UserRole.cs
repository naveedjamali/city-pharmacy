﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.User
{
    public class UserRole
    {
        /**
         * Table Properties;
         */
        public const string ROLE_ID = "RoleID";
        public const string NAME = "Name";
        public const string STATUS = "Status";
        /**
         * Class Properties;
         */

        public int RoleID { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool isAdmin()
        {
            if (Name.Equals("ADMIN"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
