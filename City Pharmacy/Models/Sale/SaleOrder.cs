﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.Sale
{
    public class SaleOrder
    {
        /// <summary>
        /// String representing Column name of SaleOrder table in the CityPharmacy database. 
        /// </summary>
        public const string SaleOrderID = "SaleOrderID";
        /// <summary>
        /// String representing Column name of SaleOrder table in the CityPharmacy database. 
        /// </summary>
        public const string SaleOrderTransactionID = "SaleOrderTransactionID";
        /// <summary>
        /// String representing Column name of SaleOrder table in the CityPharmacy database. 
        /// </summary>
        public const string SaleOrderStatus = "SaleOrderStatus";
        /// <summary>
        /// String representing Column name of SaleOrder table in the CityPharmacy database. 
        /// </summary>
        public const string ModifiedSaleID = "ModifiedSaleID";

        /// <summary>
        /// Sale Order ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Sale Transaction
        /// </summary>
        public Transaction.Transaction Transaction { get; set; }
        /// <summary>
        /// Current Status of Sale Order
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// Parent Sale Order ID
        /// </summary>
        public int ModifiedID { get; set; }

        /// <summary>
        /// Sales Amount
        /// </summary>
        public decimal Amount
        {
            get { return Transaction.Amount; }
        }
        /// <summary>
        /// Date on which sale recorded.
        /// </summary>
        public DateTime Date
        {
            get { return Transaction.Date; }
        }

        /// <summary>
        /// Contains detail of each product with its quantity, Total Amount, Price Per Unit.
        /// </summary>
        public ObservableCollection<SaleDetail> SaleList { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public SaleOrder()
        {

        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="Order">Create a deep copy of order</param>
        public SaleOrder(SaleOrder Order)
        {
            ID = Order.ID;
            Transaction = Order.Transaction;
            Status = Order.Status;
            ModifiedID = Order.ModifiedID;
            SaleList = new ObservableCollection<SaleDetail>();
            
            foreach (SaleDetail saleDetail in Order.SaleList)
            {
                SaleList.Add(new SaleDetail(saleDetail));
            }


        }

    }
}
