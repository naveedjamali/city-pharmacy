﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.Sale
{
    public class SaleDetail
    {

        public const string SaleDetailID = "SaleDetailID";
        public const string SaleDetailOrderID = "SaleDetailOrderID";
        public const string SaleDetailProductID = "SaleDetailProductID";
        public const string SaleDetailQuantity = "SaleDetailQuantity";
        public const string SaleDetailPricePerUnit = "SaleDetailPricePerUnit";
        public const string SaleDetailTotalAmount = "SaleDetailTotalAmount";
        public const string SaleDetailStatus = "SaleDetailStatus";
        public const string PROFIT_PER_ITEM = "ProfitPeritem";
        public const string PROFIT = "Profit";

        public int ID { get; set; }
        public int OrderID { get; set; }
        public Product.Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalAmount { get; set; }
        public bool Status { get; set; }
        public decimal ProfitPerItem { get; set; }
        public decimal Profit { get; set; }


        public SaleDetail()
        {
            
        }
        public SaleDetail(SaleDetail d)
        {
            ID = d.ID;
            OrderID = d.OrderID;
            Product = d.Product;
            Quantity = d.Quantity;
            UnitPrice = d.UnitPrice;
            TotalAmount = d.TotalAmount;
            Status = d.Status;
            Profit = d.Profit;
            ProfitPerItem = d.ProfitPerItem;

        }

        
    }
}
