﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.Models.Transaction
{
    public class Transaction
    {
        public const  string PROC_ADD_TRANSACTION = "AddTransaction";
        public const string TRANSACTION_ID = "TransactionID";
        public const string TRANSACTION_DATE = "TransactionDate";
        public const string TRANSACTION_AMOUNT = "TransactionAmount";
        public const string TRANSACTION_STATUS = "TransactionStatus";
        public const string TRANSACTION_CR_ACC_ID = "TransactionCrAccID";
        public const string TRANSACTION_DR_ACC_ID = "TransactionDrAccID";
        public const string TRANSACTION_USER_ID = "TransactionUserID";
        public const string TRANSACTION_DESCRIPTION = "TransactionDescription";
        public const string TRANSACTION_MODIFIED_ID = "TransactionModifiedID";
        public const string PROC_UPDATE_TRANSACTIONS = "UpdateTransactions";

        public int ID { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public bool Status { get; set; }
        public Account.Account CrAcc { get; set; }
        public Account.Account DrAcc { get; set; }
        public User.User User { get; set; }
        public string Description { get; set; }
        public int ModifiedID { get; set; }


    }
}
