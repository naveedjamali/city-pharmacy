﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.User;

namespace City_Pharmacy.Views.UserViews
{
    /// <summary>
    ///     Interaction logic for NewUser.xaml
    /// </summary>
    public partial class NewUser : Page
    {
        private readonly MainWindow parent;
        private readonly User newUser;
        private string Name;
        private DateTime date;
        private string phone;
        private string address;
        private string cnic;
        private string userName;
        private string password;
        private UserRole role;
        private bool status;

        private bool uNameAlreadyExist;

        private readonly ObservableCollection<UserRole> roles;

        public NewUser(MainWindow _parent)
        {
            InitializeComponent();
            newUser = new User();
            parent = _parent;
            joiningDatePicker.SelectedDate = DateTime.Today;
            roles = UserDB.getInstance().GetUserRoles();
            UserRoleCombobox.ItemsSource = roles;
            uNameAlreadyExist = true;
        }

        private void ChangeUserRole(object sender, RoutedEventArgs e)
        {
            role = roles.ElementAt(UserRoleCombobox.SelectedIndex);
        }

        private void changeJoiningDate(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                date = joiningDatePicker.SelectedDate.Value;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                
            }
           
        }

        private bool validateFields()
        {
            var msg = "message";
            if (NameTB.Text.Length > 0)
            {
                if (UserNameTB.Text.Length > 0)
                {
                    if (!uNameAlreadyExist)
                    {

                        if (PasswordTB.Password.Length > 0)
                        {
                            if (ConfirmPasswordTB.Password.Equals(PasswordTB.Password))
                            {
                                if (CnicTB.Text.Length == 13)
                                    return true;
                                else
                                {
                                    msg = "CNIC length must be 13 letters long OR Leave it empty";
                                    CnicTB.Focus();
                                }


                            }
                            else
                            {
                                ConfirmPasswordTB.Focus();
                                msg = "Confirm Password field does not match with Password field";
                            }
                        }
                        else
                        {
                            PasswordTB.Focus();
                            msg = "Password cannot be empty";
                        }
                    }
                    else
                    {
                        msg = "User Name already exist";
                    }
                    
                    
                   
                }
                else
                {
                    UserNameTB.Focus();
                    msg = "UserName cannot be empty";
                }
            }
            else
            {
                NameTB.Focus();
                msg = "Name cannot be empty.";
            }

            MessageBox.Show(msg);
            return false;
        }


        /**
         * To check the input for numbers only input.
         */
        private void textChanged(object sender, TextChangedEventArgs e)
        {
            if (e.Source.Equals(phoneNumTB))
                phoneNumTB.Text = Regex.Replace(phoneNumTB.Text, "[^0-9]+", "");
            else if (e.Source.Equals(CnicTB)) CnicTB.Text = Regex.Replace(CnicTB.Text, "[^0-9]+", "");
        }

        private  void AddUser(object sender, RoutedEventArgs e)
        {
            uNameAlreadyExist = UserDB.getInstance().GetUserByUsername(UserNameTB.Text);

            if (validateFields())
            {
                newUser.Name = NameTB.Text;
                newUser.JoiningDate = date;
                newUser.PhoneNumber = phoneNumTB.Text;
                newUser.Address = addressTB.Text;
                newUser.NIC = CnicTB.Text;
                newUser.UserName = UserNameTB.Text;
                newUser.Password = PasswordTB.Password;
                newUser.Role = role;
                newUser.Status = userStatus.IsChecked.Value;


                if (UserDB.getInstance().AddNewUser(newUser))
                {
                    MessageBox.Show("Success");
                }
                else
                {
                    MessageBox.Show("Internal Error occured while adding new user");
                }
            }
            else
            {
                MessageBox.Show("Something went wrong");
            }

        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            NameTB.Clear();
            phoneNumTB.Clear();
            addressTB.Clear();
            CnicTB.Clear();
            UserNameTB.Clear();
            PasswordTB.Clear();
            ConfirmPasswordTB.Clear();
            userStatus.IsChecked = false;
            UserRoleCombobox.SelectedIndex = 0;
            

        }

    }
}