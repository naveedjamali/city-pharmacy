﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.User;

namespace City_Pharmacy.Views.UserViews
{
    /// <summary>
    /// Interaction logic for AllUsers.xaml
    /// </summary>
    public partial class AllUsers : Page
    {
        private MainWindow parent;
        private ObservableCollection<User> allUsersList;

        public AllUsers(MainWindow _parent)
        {
            InitializeComponent();
            parent = _parent;

            allUsersList = UserDB.getInstance().GetAllUsers();

            lvAllUsers.ItemsSource = allUsersList;
            
        }

        private void ViewUser(object sender, MouseButtonEventArgs e)
        {
            
            Button button = (Button) sender;
            int id = (int)button.Tag;

            int index=0;
            foreach (var v in allUsersList)
            {
                if (id == v.ID)
                {
                    index = allUsersList.IndexOf(v);
                    break;
                }
                    
            }

           // lvAllUsers.SelectedIndex = index;
            
           // MessageBox.Show(allUsersList.ElementAt(index).Name);
            parent.bodyFrame.NavigationService.Navigate(new ViewUser(parent, allUsersList.ElementAt(index)));
        }

        private void OpenViewUser(object sender, RoutedEventArgs e)
        {
            
            ViewUser(sender,null);
        }


        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            parent.TextChanged(sender,e);
        }

        private async void SearchUser(object sender, RoutedEventArgs e)
        {
            if (searchUserTB.Text.Length > 0)
            {

                int id = Int32.Parse(searchUserTB.Text);
                searchUserTB.IsEnabled = false;
                searchUserBtn.IsEnabled = false;

                User u = UserDB.getInstance().GetUserByID(id);

                if (u != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewUser(parent, u));
                }
                else
                {
                    MessageBox.Show("No User exists with ID: " + id, "User not found!", MessageBoxButton.OK,
                        MessageBoxImage.Information);


                }

                searchUserTB.IsEnabled = true;
                searchUserBtn.IsEnabled = true;
                searchUserTB.Focus();
            }
        }

        private void SearchUserFromTb(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
                SearchUser(null, null);
        }

    }
}
