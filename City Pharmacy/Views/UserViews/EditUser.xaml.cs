﻿using City_Pharmacy.Models.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;

namespace City_Pharmacy.Views.UserViews
{


    /// <summary>
    /// EditUser is used to edit user data.
    /// 
    /// </summary>
    public partial class EditUser : Page
    {
        private MainWindow parent;
        private User user;
        private string uName, uAddress, uPhone, uNIC, uPassword;
        private DateTime uJoiningDate;
        private UserRole uRole;
        private bool uStatus;
        

        private readonly ObservableCollection<UserRole> roles;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_parent">Parent window, Main Window. used to access App and Parent Window data.</param>
        /// <param name="_user">The user, whose data is going to be edited.</param>
        public EditUser(MainWindow _parent, User _user)
        {
            parent = _parent;
            user = _user;
            roles = UserDB.getInstance().GetUserRoles();
            InitializeComponent();

            UserRoleCombobox.ItemsSource = roles;
            setInterface();
            if (!parent.GetAppUser().isAdmin())
            {
                NameTB.IsEnabled = false;
                joiningDatePicker.IsEnabled = false;
                CnicTB.IsEnabled = false;
                UserRoleCombobox.IsEnabled = false;
                userStatus.IsEnabled = false;

            }
        }

        private void ChangeUserState(object sender, SelectionChangedEventArgs e)
        {
            var s = userStatus.SelectedIndex;
            switch (s)
            {
                case 0:

                    uStatus = false;
                    break;
                case 1:
                    uStatus = true;
                    break;
            }
        }

        private void textChanged(object sender, TextChangedEventArgs e)
        {

            TextBox tb = (TextBox) sender;

            tb.Text = Regex.Replace(tb.Text, "[^0-9]+", "");
            
        }
        private void ChangeUserRole(object sender, RoutedEventArgs e)
        {
            uRole = roles.ElementAt(UserRoleCombobox.SelectedIndex);
        }
        private void changeJoiningDate(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                uJoiningDate = joiningDatePicker.SelectedDate.Value;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);

            }

        }
        ///
        private void updateUser(object sender, RoutedEventArgs e)
        {

            if (validateFields())
            {
               MessageBoxResult result = MessageBox.Show("Do you want to Update the user data? Once changed cannot be undone.", "Confirm Changes",
                MessageBoxButton.YesNo);



              /**
               * Checking that user have Admin rights to change specific data.
               */

                if (parent.GetAppUser().isAdmin())
                {
                    uName = NameTB.Text;
                    uJoiningDate = joiningDatePicker.SelectedDate.Value;
                    uNIC = CnicTB.Text;
                    uRole = roles.ElementAt(UserRoleCombobox.SelectedIndex);
                    uStatus = userStatus.SelectedIndex == 1 ? true : false;
                }
                else
                {
                    uName = user.Name;
                    uJoiningDate = user.JoiningDate;
                    uNIC = user.NIC;
                    uRole = user.Role;
                    uStatus = user.Status;

                }
                     /**
                     * Admin and Non-Admin users can change following data.
                     */
                
                    uAddress = addressTB.Text;
                    uPhone = phoneNumTB.Text;
                    uPassword = PasswordTB.Password;
               

                /**
                 * Check User confirmation to update the data.
                 */

                if (result == MessageBoxResult.Yes)
                {
                    
                    User u = new User();
                    u.ID = user.ID;
                    u.Name = uName;
                    u.Address = uAddress;
                    u.JoiningDate = uJoiningDate;
                    u.NIC = uNIC;
                    u.PhoneNumber = uPhone;
                    u.UserName = user.UserName;
                    u.Password = uPassword;
                    u.Role = uRole;
                    u.Status = uStatus;


                    if (UserDB.getInstance().UpdateUser(u))
                    {
                        MessageBox.Show("User Updated Successfully;");
                        user = u;
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong!");
                    }
                }
            }

        }

        /// <summary>
        /// Validates all the fields that are filled  with some data.
        /// </summary>
        /// <returns></returns>
        private bool validateFields()
        {
            var msg = "message";
            if (NameTB.Text.Length > 0)
            {
                if (PasswordTB.Password.Length > 0)
                {
                    if (ConfirmPasswordTB.Password.Equals(PasswordTB.Password))
                    {
                        if (CnicTB.Text.Length == 13)
                            return true;
                        else
                        {
                            msg = "CNIC length must be 13 letters long OR Leave it empty";
                            CnicTB.Focus();
                        }


                    }
                    else
                    {
                        ConfirmPasswordTB.Focus();
                        msg = "Confirm Password field does not match with Password field";
                    }
                }
                else
                {
                    PasswordTB.Focus();
                    msg = "Password cannot be empty";
                }
            }
            else
            {
                NameTB.Focus();
                msg = "Name cannot be empty.";
            }

            MessageBox.Show(msg);
            return false;
        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            /**
             * Set Interface with user data.
             */
            setInterface();
        }


        /// <summary>
        /// Initiate all the inputs with user data to set up screen.
        /// </summary>
        public void setInterface()
        {
            IDTB.Text = user.ID.ToString();
            NameTB.Text = user.Name;
            joiningDatePicker.SelectedDate = user.JoiningDate;
            phoneNumTB.Text = user.PhoneNumber;
            addressTB.Text = user.Address;
            CnicTB.Text = user.NIC;
            UserNameTB.Text = user.UserName;
            PasswordTB.Password = user.Password;
            ConfirmPasswordTB.Password = user.Password;
            
                for (int i = 0; i < roles.Count; i++)
                {
                    if (user.Role.Name == roles[i].Name)
                    {
                        UserRoleCombobox.SelectedIndex = i;
                        break;
                    }
                }

            

            if (user.Status == true)
            {
                userStatus.SelectedIndex = 1;
            }
            else
            {
                userStatus.SelectedIndex = 0;
            }

        }


        private void showPassword(object sender, MouseButtonEventArgs e)
        {

            MessageBox.Show("Password: "+PasswordTB.Password, "Show Password", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void showConfirmPassword(object sender, MouseButtonEventArgs e)
        {

            MessageBox.Show("Password: " + ConfirmPasswordTB.Password,"Show Confirm Password", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
