﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.Models.User;

namespace City_Pharmacy.Views.UserViews
{
    

    public partial class ViewUser : Page
    {
        private MainWindow parent;
        private User user;

        public ViewUser(MainWindow _parent, User _user)
        {
            InitializeComponent();
            parent = _parent;
            user = _user;

            setInterface(user);
        }

        public void setInterface(User u)
        {
            IdLabel.Content = u.ID;
            Name.Content = u.Name;
            joiningDateLbl.Content = u.JoiningDate.ToString();
            phoneLbl.Content = u.PhoneNumber;
            addressLbl.Content = u.Address;
            CNICLbl.Content = u.NIC;
            usernameLbl.Content = u.UserName;
            RoleLbl.Content = u.Role.Name;
            statusLbl.Content = u.Status ? "Enabled" : "Disabled";
        }

        private void EditUser(object sender, RoutedEventArgs e)
        {

            parent.bodyFrame.NavigationService.Navigate(new EditUser(parent, user));
        }

        private void goBack(object sender, MouseButtonEventArgs e)
        {
            
            parent.bodyFrame.NavigationService.GoBack();
        }

        private void showPassword(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Password: " + user.Password,"Show Password",MessageBoxButton.OK,MessageBoxImage.Information);
        }
    }
}
