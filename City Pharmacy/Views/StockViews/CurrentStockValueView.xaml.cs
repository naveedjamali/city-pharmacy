﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;
using City_Pharmacy.Views.ProductViews;

namespace City_Pharmacy.Views.StockViews
{
    /// <summary>
    /// Interaction logic for CurrentStockValueView.xaml
    /// </summary>
    public partial class CurrentStockValueView : Page
    {
        private ObservableCollection<Product> itemsList;
        private MainWindow parent;
        private StockDB stockDb;

        public CurrentStockValueView(MainWindow _parent, ProductDB productDb)
        {
            InitializeComponent();
            parent = _parent;
            itemsList = productDb.GetAllProducts();
            productsList.ItemsSource = itemsList;

             stockDb = new StockDB();

            decimal CurrentValueOfStock = stockDb.CurrentValueOfStock();
            CurrentValueOfStockLabel.Text = "Rs. " + CurrentValueOfStock + "/-";


        }
        /// <summary>
        /// Opens the Product Detail in ViewProduct page in MainWindow
        /// </summary>
        /// <param name="sender">Event Sender Element</param>
        /// <param name="e">Arguments</param>
        private void viewProduct(object sender, RoutedEventArgs e)
        {

            ContentControl b = (ContentControl)sender;
            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProduct(parent, itemsList.ElementAt(i)));

                    break;
                }
            }


        }


    }
}
