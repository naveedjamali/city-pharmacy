﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;
using City_Pharmacy.Models.Sale;

namespace City_Pharmacy.Views.SaleViews
{
    /// <summary>
    /// Interaction logic for EditSale.xaml
    /// </summary>
    public partial class EditSale : Page
    {
        private ObservableCollection<Product> SoldProductList;
        private SaleOrder Order;
        private Product SelectedProduct;
        private ObservableCollection<SaleDetail> OrderDetailList;
        private decimal TotalRefundableAmount;

        public EditSale(SaleOrder _Order)
        {
            InitializeComponent();
            Order = _Order;
            SoldProductList = new ObservableCollection<Product>();
            foreach (SaleDetail detail in Order.SaleList)
            {
                SoldProductList.Add(detail.Product);
            }

            SelectProductCombobox.ItemsSource = SoldProductList;
            OrderDetailList = new ObservableCollection<SaleDetail>();
            
            OrderDetailListView.ItemsSource = OrderDetailList;
            TotalRefundableAmount=0;





        }

        private void SelectProductCombobox_OnSelected(object sender, SelectionChangedEventArgs e)
        {

            SelectedProduct = (Product)SelectProductCombobox.SelectedItem;
            if (SelectedProduct == null)
                return;
            SelectedProductUnitPriceLabel.Content = SelectedProduct.Price + "/- ";
            ProductPurchaseQuantityTB.IsEnabled = true;
            ProductPurchaseQuantityTB.Text = "";

        }

        private void ProductSaleQuantityTB_OnTextChanged(object sender, TextChangedEventArgs e)
        {


            ProductPurchaseQuantityTB.Text = Regex.Replace(((TextBox)sender).Text, "[^0-9]+", "");

            int qty = 0;

            Int32.TryParse(ProductPurchaseQuantityTB.Text, out qty);

            if (ProductPurchaseQuantityTB.IsEnabled)

            {
                try
                {

                    newItemTotalCost.Content = qty * SelectedProduct.Price;
                }
                catch (Exception exception)
                {
                    

                }
            }


            AddProductToListBtn.IsEnabled = qty > 0;
        }

        private void AddItemToSaleList(object sender, RoutedEventArgs e)
        {


            if (ExistInList(SelectedProduct))
            {
                MessageBox.Show("Product Already Exist in the list.", "Duplicate Entry", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;

            }

            int QtyLevel = 0;
            foreach (SaleDetail detail in Order.SaleList)
            {
                if (SelectedProduct.ID == detail.Product.ID)
                {
                    QtyLevel = detail.Quantity;
                    break;
                }
            }

            int returnQty = Int32.Parse(ProductPurchaseQuantityTB.Text) ;

            if (returnQty > QtyLevel ||returnQty < 0)
            {
                MessageBox.Show("Return Quantity of the product "+SelectedProduct.Name+"+ must be between 1 or less than or equal sold quantity" + QtyLevel, "Return Quantity exceeds return limit", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                ProductPurchaseQuantityTB.Text = QtyLevel+ " ";
                ProductPurchaseQuantityTB.Focus();
                return;
            }


            if (isDetailEnterFormValid())
            {
                SaleDetail detail = new SaleDetail();
                detail.Product = SelectedProduct;
                detail.UnitPrice = SelectedProduct.Price;
                detail.Quantity = Int32.Parse(ProductPurchaseQuantityTB.Text);
                detail.TotalAmount = detail.Quantity * detail.UnitPrice;
                detail.Status = true;
                detail.ProfitPerItem = detail.Product.Price - detail.Product.PurchasePrice;

                OrderDetailList.Add(detail);

                SetTotal();
                
                AddProductToListBtn.IsEnabled = false;



            }




        }
        private bool ExistInList(Product _p)
        {
            foreach (SaleDetail s in OrderDetailList)
            {
                if (s.Product.ID == _p.ID)
                {
                    return true;
                }
            }

            return false;
        }
        private bool isDetailEnterFormValid()
        {
            if (SelectedProduct == null)
            {
                MessageBox.Show("You must select a product to add in the list", "Product not selected",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (ProductPurchaseQuantityTB.Text.Length < 1)
            {
                MessageBox.Show("You must enter the quantity of the product", "Product quantity is empty",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        private void SetTotal()
        {

            TotalItemsCountLbl.Content = OrderDetailList.Count;
            decimal cost = 0;
            foreach (SaleDetail p in OrderDetailList)
            {
                cost += p.TotalAmount;
            }

            TotalRefundableAmount = Order.Amount - cost;

            AllItemsTotalCostLbl.Content = "Rs. "+TotalRefundableAmount + "/-";
            
        }

        private void OpenDateDropDown(object sender, KeyEventArgs e)
        {


            ReturnDatePicker.IsDropDownOpen = true;
            try
            {
                e.Handled = true;
            }
            catch (Exception exception)
            {
                
            }
        }


        private void SaveSale(object sender, RoutedEventArgs e)
        {

            if (ReturnDatePicker.SelectedDate != null)
            {
                DateTime date = ReturnDatePicker.SelectedDate.Value;
                if (date <= DateTime.Now)
                {
                    if (OrderDetailList.Count > 0)
                    {

                       
                            SaleDB saleDb = new SaleDB();
                           

                        int ReturnID = saleDb.PartialReturnSale(Order, OrderDetailList, TotalRefundableAmount, date);

                            //SaleOrder order = saleDb.GetSaleByID(ReturnID);

                            /**
                             * Write Printer Call Here.
                             */
                            MessageBox.Show("Stock Added Successfully", "Success", MessageBoxButton.OK, MessageBoxImage.Information);

                            ResetAll(null, null);
                        
                    }
                    else
                    {
                        MessageBox.Show("Error: Items list is empty...", "Error", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Date, Date must be today or previous date", "Invalid Date", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }


            }
            else
            {
                MessageBox.Show("Empty Date is not allowed...");
            }




            e.Handled = true;
        }

        private void ResetAll(object sender, RoutedEventArgs e)
        {

            OrderDetailList.Clear();
            SelectProductCombobox.SelectedIndex = -1;
            SelectedProduct = null;
            ProductPurchaseQuantityTB.Text = "";
           
            SelectedProductUnitPriceLabel.Content = "";
            newItemTotalCost.Content = "";
            SetTotal();
            try
            {
                e.Handled = true;

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);

            }

        }


        private void AddButton_OnKeyDown(object sender, KeyEventArgs e)
        {
            
            AddItemToSaleList(sender, e);
        }
        private void BlockNavigation(object sender, KeyEventArgs e)
        {

            /*
             * This method just mark the envet as handled.
             * Purpose to use this method is to prevent back navigation when BackSpace is pressed.
             */
            e.Handled = true;

        }

        private void RemoveItemFromList(object sender, RoutedEventArgs e)
        {

            Button b = (Button)sender;

            int tag = (Int32)b.Tag;
            for (int i = 0; i < OrderDetailList.Count; i++)
            {
                if (OrderDetailList.ElementAt(i).Product.ID == tag)
                {
                    OrderDetailList.RemoveAt(i);
                    break;
                }
            }

            SetTotal();
        }


        private void EditPurchaseDetailItem(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            int tag = (Int32)b.Tag;
            SaleDetail p;

            for (int i = 0; i < OrderDetailList.Count; i++)
            {
                if (OrderDetailList.ElementAt(i).Product.ID == tag)
                {
                    p = new SaleDetail(OrderDetailList.ElementAt(i));
                    
                    {
                        foreach (Product product in SoldProductList)
                        {
                            if (p.Product.ID == product.ID)
                            {
                                SelectProductCombobox.SelectedIndex = SoldProductList.IndexOf(product);
                            }
                        }
                    }
                    ProductPurchaseQuantityTB.Text = p.Quantity + "";
                    
                    OrderDetailList.RemoveAt(i);

                    break;
                }

            }
            SetTotal();

        }
    }
}
