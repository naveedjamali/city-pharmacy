﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Sale;

namespace City_Pharmacy.Views.SaleViews
{
    /// <summary>
    /// Interaction logic for ViewSale.xaml
    /// </summary>
    public partial class ViewSale : Page
    {
        private readonly SaleOrder Order;
        private MainWindow Parent;

        public ViewSale(MainWindow _parent, SaleOrder _order)
        {
            InitializeComponent();
            Parent = _parent;
            Order = _order;

            DetailListLV.ItemsSource = Order.SaleList;

            OrderIdLabel.Content = Order.ID;
            DateLabel.Content = Order.Transaction.Date.ToShortDateString();
            TotalCostLabel.Content = "Rs. " + Order.Transaction.Amount + "/-";
            TotalItemsLabel.Content = Order.SaleList.Count;

            if (Order.ModifiedID > 0)
                ModifiedOrderIdLabel.Content = "Return ID: "+Order.ModifiedID;
            ReturnPurchaseBtn.IsEnabled = Order.Status;
        }

        private void ReturnPurchase(object sender, RoutedEventArgs e)
        {
            

            int i = new SaleDB().ReturnSale(Order);
            if (i > 0)
            {
                MessageBox.Show("Sale Returned Successfully", "Successful", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Error", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
        }
        private void PrintPurchase(object sender, RoutedEventArgs e)
        {

            Print.Print print = new Print.Print();



            print.PrintSale(Order);

        }

        private void EditSale(object sender, RoutedEventArgs e)
        {
            Parent.bodyFrame.NavigationService.Navigate(new EditSale(Order));

        }
    }
}
