﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Sale;

namespace City_Pharmacy.Views.SaleViews
{
    /// <summary>
    /// Interaction logic for AllSales.xaml
    /// </summary>
    public partial class AllSales : Page
    {
        private ObservableCollection<SaleOrder> itemsList;
        private MainWindow parent;
        private SaleDB saleDb;

        public AllSales(MainWindow _parent)
        {
            InitializeComponent();


            parent = _parent;

            saleDb = new SaleDB();

            itemsList = saleDb.GetAllSales();

            SaleListView.ItemsSource = itemsList;
        }

        private void viewSale(object sender, RoutedEventArgs e)
        {

            Button b = (Button)sender;
            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewSale(parent,itemsList.ElementAt(i)));

                    break;
                }
            }

        }
        private void SearchSaleTV(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                SearchSaleByID(sender, null);
            }
        }
        private void SearchSaleByID(object sender, RoutedEventArgs e)
        {

            if (searchSale.Text.Length > 0)
            {

                SaleOrder s = new SaleDB().GetSaleByID(Int32.Parse(searchSale.Text));

                if (s != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewSale(parent, s));
                }
                else
                {
                    MessageBox.Show("No Sale exist with ID: \"" + searchSale.Text + "\".");
                    searchSale.Focus();
                }
            }


        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            
            parent.TextChanged(sender,e);
        }
    }
}
