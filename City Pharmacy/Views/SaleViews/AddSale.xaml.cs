﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.Annotations;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;
using City_Pharmacy.Models.Purchase;
using City_Pharmacy.Models.Sale;

namespace City_Pharmacy.Views.SaleViews
{
    /// <summary>
    /// Interaction logic for AddSale.xaml
    /// </summary>
    public partial class AddSale : Page, INotifyPropertyChanged
    {
        private readonly ProductDB productDb;
        public ObservableCollection<Product> searchedProductsList;
        public ObservableCollection<Product> SearchedProductsList
        
        {
            get => searchedProductsList;
            set
            {
                searchedProductsList = value;
                NotifyPropertyChanged("SearchedProductsList");
            }
        }
        private Product SelectedProduct;
        public decimal TotalPayableAmount;
        decimal cashReceived;
        public DateTime purchaseDate;
        public ObservableCollection<SaleDetail> OrderDetailList;
        public event PropertyChangedEventHandler PropertyChanged;
        private MainWindow parent;


        public AddSale(MainWindow _parent)
        {
           
            InitializeComponent();
            productDb = new ProductDB();
            cashReceived = 0;

            SearchedProductsList = new ObservableCollection<Product>();
            SelectProductCombobox.ItemsSource = SearchedProductsList;
            SelectProductCombobox.IsEnabled = false;

            purchaseDate = DateTime.Now;

            OrderDetailList = new ObservableCollection<SaleDetail>();


            OrderDetailListView.ItemsSource = OrderDetailList;

            SelectProductCombobox.ItemsSource = SearchedProductsList;

            SetTotal();
            

        }

       

        private void EditPurchaseDetailItem(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            int tag = (Int32)b.Tag;
            SaleDetail p;

            for (int i = 0; i < OrderDetailList.Count; i++)
            {
                if (OrderDetailList.ElementAt(i).Product.ID == tag)
                {
                    p = new SaleDetail(OrderDetailList.ElementAt(i));
                    SelectProductCombobox.SelectedItem = p.Product;
                    ProductPurchaseQuantityTB.Text = p.Quantity + "";

                    searchProductTV.Text = p.Product.Name;
                    ProductPurchaseQuantityTB.Text = p.Quantity+"";

                    OrderDetailList.RemoveAt(i);

                    break;
                }

            }
            SetTotal();

        }

        private void CashReceived_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            CashReceivedTV.Text = Regex.Replace(((TextBox)sender).Text, "[^0-9]+", "");
            
            SetTotal();

        }

        private void SetTotal()
        {

            TotalItemsCountLbl.Content = OrderDetailList.Count;
            decimal cost = 0;
            foreach (SaleDetail p in OrderDetailList)
            {
                cost += p.TotalAmount;
            }

            TotalPayableAmount = cost;
            AllItemsTotalCostLbl.Content = cost + "";



            decimal.TryParse(CashReceivedTV.Text, out cashReceived);

            CashChangeLabel.Content = ("Rs. " + (cashReceived - TotalPayableAmount) + "/-");

        }

        private void SearchProduct_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (searchProductTV.Text.Length > 0) {
                string itemName;


                SearchedProductsList.Clear();
                itemName = searchProductTV.Text;

                SearchedProductsList = new ProductDB().SearchProductByName(itemName);
                SelectProductCombobox.IsEnabled = true;
                SelectProductCombobox.ItemsSource = SearchedProductsList;
            }
            else
            {

                SelectProductCombobox.IsEnabled = false;
                SearchedProductsList.Clear();
                SelectedProduct = null;
                ProductPurchaseQuantityTB.IsEnabled = false;
            }




        }

        private void AddButton_OnKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Enter || e.Key==Key.Return)
            {
                AddItemToSaleList(sender, e);
            }
        }

        private void AddItemToSaleList(object sender, RoutedEventArgs e)
        {

            if (ExistInList(SelectedProduct))
            {
                MessageBox.Show("Product Already Exist in the list.", "Duplicate Entry", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;

            }

            int qty = 0;
            try
            {
               
                qty = Int32.Parse(ProductPurchaseQuantityTB.Text);
            }
            catch (Exception)
            {
                
            }

            if (qty < 1)
            {
                return;
            }
            
            if( SelectedProduct.AvailableStock < qty)
            {
                MessageBox.Show("This product is not available in required quantity, Please set the quantity "+SelectedProduct.AvailableStock+" or below", "Product out of stock", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                ProductPurchaseQuantityTB.Text = SelectedProduct.AvailableStock + " ";
                ProductPurchaseQuantityTB.Focus();
                return;
            }


            if (isDetailEnterFormValid())
            {
                SaleDetail detail = new SaleDetail();
                detail.Product = SelectedProduct;
                detail.UnitPrice = SelectedProduct.Price;
                detail.Quantity = Int32.Parse(ProductPurchaseQuantityTB.Text);
                detail.TotalAmount = detail.Quantity * detail.UnitPrice;
                detail.Status = true;
                detail.ProfitPerItem = detail.Product.Price - detail.Product.PurchasePrice;

                OrderDetailList.Add(detail);

                SetTotal();

                searchProductTV.Text = "";
                searchProductTV.Focus();
                AddProductToListBtn.IsEnabled = false;



            }
            
        }
        
        private void SelectProductCombobox_OnSelected(object sender, SelectionChangedEventArgs e)
        {
            SelectedProduct = (Product) SelectProductCombobox.SelectedItem;
            if (SelectedProduct == null)
                return;
            SelectedProductUnitPriceLabel.Content = SelectedProduct.Price + "/- ";
            ProductPurchaseQuantityTB.IsEnabled = true;
            ProductPurchaseQuantityTB.Text = "";
        }

        private void ProductSaleQuantityTB_OnTextChanged(object sender, TextChangedEventArgs e)
        {

            ProductPurchaseQuantityTB.Text = Regex.Replace(((TextBox)sender).Text, "[^0-9]+", "");

            int qty = 0;

            Int32.TryParse(ProductPurchaseQuantityTB.Text, out qty);

            if (ProductPurchaseQuantityTB.IsEnabled)
            {
                newItemTotalCost.Content = qty * SelectedProduct.Price;
            }
            

            AddProductToListBtn.IsEnabled = qty > 0;
        }

        private void RemoveItemFromList(object sender, RoutedEventArgs e)
        {

            Button b = (Button)sender;

            int tag = (Int32)b.Tag;
            for (int i = 0; i < OrderDetailList.Count; i++)
            {
                if (OrderDetailList.ElementAt(i).Product.ID == tag)
                {
                    OrderDetailList.RemoveAt(i);
                    break;
                }
            }

            SetTotal();
        }

        private void ResetAll(object sender, RoutedEventArgs e)
        {

            OrderDetailList.Clear();
            SelectProductCombobox.SelectedIndex = -1;
            SelectedProduct=null;
            ProductPurchaseQuantityTB.Text = "";
            searchProductTV.Text="";
            searchProductTV.Focus();
            SelectedProductUnitPriceLabel.Content = "";
            newItemTotalCost.Content = "";
            CashReceivedTV.Text = "";
            SetTotal();
            try
            {
                e.Handled = true;

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
             
            }
           
        }

        private void SaveSale(object sender, RoutedEventArgs e)
        {

            if (SaleDatePicker.SelectedDate != null)
            {
                DateTime date = SaleDatePicker.SelectedDate.Value;
                if (date <= DateTime.Now)
                {
                    if (OrderDetailList.Count > 0)
                    {

                        if (cashReceived >= TotalPayableAmount)
                        {
                            SaleDB saleDb = new SaleDB();

                            int SaleID = saleDb.AddNewSale(OrderDetailList, TotalPayableAmount, date);


                            if (PrintCheckBox.IsChecked != null && (bool) PrintCheckBox.IsChecked)
                            {
                                SaleOrder order = saleDb.GetSaleByID(SaleID);

                                Print.Print print = new Print.Print();



                                print.PrintSale(order);
                            }
                            
                            
                            MessageBox.Show("Thankyou for purchasing...", "Success", MessageBoxButton.OK, MessageBoxImage.Information);

                            ResetAll(null, null);
                        }


                        else
                        {
                            MessageBox.Show("Cash Received must be more than or equal to "+TotalPayableAmount, "Received Cash is not enough", MessageBoxButton.OK,
                                MessageBoxImage.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Error: Items list is empty...", "Error", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Date, Date must be today or previous date", "Invalid Date", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }

               
            }
            else
            {
                MessageBox.Show("Empty Date is not allowed...");
            }


            

            e.Handled = true;
        }

        public void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }


        private void SearchProductTV_OnKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                string itemName;
                if (searchProductTV.Text.Length > 0)
                {

                    SearchedProductsList.Clear();
                    itemName = searchProductTV.Text;

                    SearchedProductsList = new ProductDB().SearchProductByName(itemName);

                    SelectProductCombobox.ItemsSource = SearchedProductsList;




                }
            }
            
        }

        /// <summary>
        /// Checks that data entering in the sale list is valid
        /// </summary>
        /// <returns>True/False</returns>

        private bool isDetailEnterFormValid()
        {
            if (SelectedProduct == null)
            {
                MessageBox.Show("You must select a product to add in the list", "Product not selected",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (ProductPurchaseQuantityTB.Text.Length < 1)
            {
                MessageBox.Show("You must enter the quantity of the product", "Product quantity is empty",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }


        private void FocusToAddButton(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
                if(ProductPurchaseQuantityTB.Text.Length>0)
                AddProductToListBtn.Focus();
        }


        private void BlockNavigation(object sender, KeyEventArgs e)
        {
            
            /*
             * This method just mark the envet as handled.
             * Purpose to use this method is to prevent back navigation when BackSpace is pressed.
             */
            e.Handled = true;
        }

        private void OpenDateDropDown(object sender, KeyEventArgs e)
        {

            SaleDatePicker.IsDropDownOpen = true;
            e.Handled = true;
        }

        private bool ExistInList(Product _p)
        {
            foreach (SaleDetail s in OrderDetailList)
            {
                if (s.Product.ID == _p.ID)
                {
                    return true;
                }
            }

            return false;
        }


        private void SelectProductCombobox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
            {
                e.Handled = true;
            }
            
        }

        private void ProductPurchaseQuantityTB_OnKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                AddItemToSaleList(null, null);
            }
        }
    }
    
}