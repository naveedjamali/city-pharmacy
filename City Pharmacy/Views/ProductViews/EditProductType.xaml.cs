﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for EditProductType.xaml
    /// </summary>
    public partial class EditProductType : Page
    {

        private ProductType pType;
        private readonly ProductDB _productDb;

        public EditProductType(ProductType _pType)
        {
            InitializeComponent();
            pType = _pType;

            _productDb = new ProductDB();

            IDTB.Text = pType.ID.ToString();
            NameTB.Text = pType.Name;

            productTypeStatusCombobox.SelectedIndex = !pType.Status ? 0 : 1;
        }

        private void resetFields(object sender, RoutedEventArgs e)
        {

            IDTB.Text = pType.ID.ToString();
            NameTB.Text = pType.Name;

            productTypeStatusCombobox.SelectedIndex = !pType.Status ? 0 : 1;

        }

        private void updateProductType(object sender, RoutedEventArgs e)
        {

            if (NameTB.Text.Length > 0)
            {
                ProductType p = new ProductType();
                p.ID = pType.ID;
                p.Name = NameTB.Text;
                if (productTypeStatusCombobox.SelectedIndex == 1)
                {
                    p.Status = true;
                }
                else
                {
                    p.Status = false;
                }

                try
                {

                    if (_productDb.UpdateProductType(p))
                    {
                        MessageBox.Show("\'" + p.Name + "\' updated successfully!", "Operation Successful", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show("\'" + p.Name + "\' updating failed due to some error!", "Operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (SqlException sqlException)
                {

                    MessageBox.Show("Some Database Error; Please try again...");
                }


            }
        }
    }
}
