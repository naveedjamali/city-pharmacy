﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for EditProduct.xaml
    /// </summary>
    public partial class EditProduct : Page
    {
        private Product product;
        private readonly ObservableCollection<ProductType>productTypes;
        private readonly ObservableCollection<ProductVendor> productVendors;
        private readonly ProductDB _productDb;


        public EditProduct(Product _product)
        {
            InitializeComponent();
            _productDb = new ProductDB();
            product = _product;
            productTypes = _productDb.GetAllProductTypes();
            productTypeCombobox.ItemsSource = productTypes;
            productVendors = _productDb.GetAllProductVendors();
            productVendorCombobox.ItemsSource = productVendors;

            IDTB.Text = product.ID.ToString();
            NameTB.Text = product.Name;
            lastModifiedDate.SelectedDate = product.LastModified;
            rackNumTB.Text = product.RackNum;
            minTempTB.Text = product.MinTemp.ToString();
            maxTempTB.Text = product.MaxTemp.ToString();
            descriptionTB.Text = product.Description;
            productPrice.Text = product.Price.ToString();
            
            foreach (ProductType t in productTypeCombobox.ItemsSource)
            {
                if (product.ProductType.ID == t.ID)
                {
                    productTypeCombobox.SelectedItem = t;
                }
            }
            

            foreach (ProductVendor vendor in productVendorCombobox.ItemsSource)
            {
                if (product.Vendor.ID == vendor.ID)
                {
                    productVendorCombobox.SelectedItem = vendor;
                }
            }
    

            productStatusCombobox.SelectedIndex = !product.Status ? 0 : 1;
            


        }

        
        private void textChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox) sender;

           
                tb.Text = Regex.Replace(tb.Text, "[^0-9]+", "");
            
        }

       
        private void resetFields(object sender, RoutedEventArgs e)
        {
            IDTB.Text = product.ID.ToString();
            NameTB.Text = product.Name;
            lastModifiedDate.SelectedDate = product.LastModified;
            rackNumTB.Text = product.RackNum;
            minTempTB.Text = product.MinTemp.ToString();
            maxTempTB.Text = product.MaxTemp.ToString();
            descriptionTB.Text = product.Description;
            productPrice.Text = product.Price.ToString();



            for (int i = 0; i < productTypes.Count; i++)
            {
                if (product.ProductType.ID == productTypes[i].ID)
                {
                    productTypeCombobox.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < productVendors.Count; i++)
            {
                if (product.Vendor.ID == productVendors[i].ID)
                {
                    productVendorCombobox.SelectedIndex = i;
                    break;
                }
            }



            if (product.Status)
            {
                productTypeCombobox.SelectedIndex = 1;
            }
            else
            {
                productTypeCombobox.SelectedIndex = 0;
            }

        }
        /// <summary>
        /// Update the Product instance and sends it to DB to update.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateProduct(object sender, RoutedEventArgs e)
        {
            if (ValidateForm())
            {
                Product p = new Product();

                p.ID = product.ID;
                p.Name = NameTB.Text;

                if (productStatusCombobox.SelectedIndex == 1)
                {
                    p.Status = true;
                }
                else
                {
                    p.Status = false;
                }

                p.RackNum = rackNumTB.Text;

                p.LastModified = DateTime.Now;

                try
                {
                    if(minTempTB.Text.Length>0)
                    p.MinTemp = Int32.Parse(minTempTB.Text);
                }
                catch (InvalidCastException exception)
                {
                    Console.WriteLine(exception.Message);
                    p.MinTemp = product.MinTemp;


                }

                try
                {
                    if(maxTempTB.Text.Length>0)
                    p.MaxTemp = Int32.Parse(maxTempTB.Text);
                }
                catch (InvalidCastException exception)
                {
                    Console.WriteLine(exception.Message);
                    p.MaxTemp = product.MaxTemp;


                }

                p.Description = descriptionTB.Text;

                try
                {
                    p.Price = decimal.Parse(productPrice.Text);
                }
                catch (InvalidCastException invalidCastException)
                {
                    Console.WriteLine(invalidCastException);
                    p.Price = product.Price;

                }

                p.ProductType = (ProductType) productTypeCombobox.SelectedItem;
                p.Vendor = (ProductVendor)productVendorCombobox.SelectedItem;

                try
                {
                    
                    if (_productDb.UpdateProduct(p))
                    {
                        MessageBox.Show("\'" + p.Name + "\' updated successfully!", "Operation Successful", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show("\'" + p.Name + "\' updating failed due to some error!", "Operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (SqlException sqlException)
                {

                    MessageBox.Show("Some Database Error; Please try again...");
                }

            }
        }

        /// <summary>
        /// Performs validation of the edit form.
        /// </summary>
        /// <returns>True if all the entries are valid to create a product</returns>
        private bool ValidateForm()
        {
            String errorMsg = "Error: ";
            
            if (NameTB.Text.Length > 0)
            {
                if (productPrice.Text.Length > 0)
                {
                    decimal price;
                    if (decimal.TryParse(productPrice.Text,NumberStyles.Currency,null,out price))
                    {
                        
                        if (price >= 0)
                        {

                            return true;
                        }
                        else
                        {
                            productPrice.Focus();
                            errorMsg += "Price cannot be negative";
                        }
                    }
                    else
                    {
                        productPrice.Focus();
                        errorMsg += "Value entered in price is not a number. Please re-enter the price ";
                    }
                }
               else
                {
                    productPrice.Focus();
                    errorMsg += "Please enter Price";
                }
            }
            else
            {
                NameTB.Focus();
                errorMsg += "Please enter name";
            }

            MessageBox.Show(errorMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            return false;
        }
    }
}
