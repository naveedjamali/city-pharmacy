﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for ViewProductType.xaml
    /// </summary>
    public partial class ViewProductType : Page
    {
        private MainWindow parent;
        private ProductType pType;

        public ViewProductType(MainWindow _parent, ProductType _pType)
        {
            InitializeComponent();

            parent = _parent;
            pType = _pType;


            IdLabel.Content = pType.ID;
            nameLBL.Content = pType.Name;
            statusLbl.Content = pType.Status ? "Enabled" : "Disabled";
        }

        private void EditProductType(object sender, RoutedEventArgs e)
        {
            parent.bodyFrame.NavigationService.Navigate(new EditProductType(pType));

        }
    }
}
