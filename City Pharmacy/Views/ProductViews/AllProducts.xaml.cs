﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for AllProducts.xaml
    /// </summary>
    public partial class AllProducts : Page
    {
        private ObservableCollection<Product> itemsList;
        private MainWindow parent;

        public AllProducts(MainWindow _parent, ProductDB productDb)
        {
            InitializeComponent();
            parent = _parent;
            itemsList = productDb.GetAllProducts();
            productsList.ItemsSource = itemsList;
            
            
        }
        /// <summary>
        /// Opens the Product Detail in ViewProduct page in MainWindow
        /// </summary>
        /// <param name="sender">Event Sender Element</param>
        /// <param name="e">Arguments</param>
        private void viewProduct(object sender, RoutedEventArgs e)
        {
          
            ContentControl b = (ContentControl) sender;
            int tag = Int32.Parse(b.Tag.ToString());

            
            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProduct(parent,itemsList.ElementAt(i)));
                    
                    break;
                }
            }
            
            
        }


        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            parent.TextChanged(sender,e);
        }

        private void SearchProductFromTb(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                SearchProduct(null, null);
            }
        }
        private void SearchProduct(object sender, RoutedEventArgs e)
        {

            if (searchProductTB.Text.Length > 0)
            {
                var p = parent.ProductDb.SearchProductById(Int32.Parse(searchProductTB.Text));
                if (p != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProduct(parent, p));
                }
                else
                {
                    MessageBox.Show("No Product exist with ID: \"" + searchProductTB.Text + "\".");
                    searchProductTB.Focus();
                }
            }

        }
    }
}
