﻿using System.Windows.Controls;
using System.Windows.Input;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// ViewProduct class takes a Product Object and shows its data in ViewProduct Page.
    /// </summary>
    public partial class ViewProduct : Page
    {
        private MainWindow parent;
        private Product product;
        public ViewProduct(MainWindow _parent, Product _product)
        {
            InitializeComponent();
            parent = _parent;
            product = _product;

            setInterface(product);
        }

        private void setInterface(Product p)
        {

            IdLabel.Content = p.ID;
            nameLBL.Content = p.Name;
            ProductTypeLabel.Content = p.ProductType.Name;
            ProductVendorLabel.Content = p.Vendor.Name;
            lastModifiedLabel.Content = p.LastModified.ToString();
            minTempLabel.Content = p.MinTemp;
            maxTempLabel.Content = p.MaxTemp;
            descrptionLabel.Content = p.Description;
            pricePerUnitLabel.Content = p.Price;
            availableStockLbl.Content = p.AvailableStock;
            priceOfTotalStockLabel.Content = p.PriceOfTotalStock;
            statusLbl.Content = p.Status ? "Enabled" : "Disabled";

        }

        private void goBack(object sender, MouseButtonEventArgs e)
        {
            
            parent.bodyFrame.GoBack();
            
        }

        private void EditProduct(object sender, System.Windows.RoutedEventArgs e)
        {

            parent.bodyFrame.NavigationService.Navigate(new EditProduct(product));
        }
    }
}
