﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for AddProductType.xaml
    /// </summary>
    public partial class AddProductType : Page
    {

        private readonly ProductDB _productDb;

        public AddProductType()
        {
            InitializeComponent();

            _productDb = new ProductDB();
        }

        private void resetFields(object sender, RoutedEventArgs e)
        {

            NameTB.Text = "";
        }

        private void AddNewProductType(object sender, RoutedEventArgs e)
        {
            if (NameTB.Text.Length > 0 && NameTB.Text.Length<50)
            {
                ProductType pType = new ProductType();
                pType.Name = NameTB.Text;

                if (productTypeStatusCombobox.SelectedIndex == 0)
                {
                    pType.Status = false;
                }
                else if (productTypeStatusCombobox.SelectedIndex == 1)
                {
                    pType.Status = true;
                }

                if (_productDb.AddNewProductType(pType))
                {
                    MessageBox.Show("\'" + pType.Name + "\' added successfully", "Operation Successful",
                        MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    NameTB.Text = "";
                    pType = null;
                }
            }
            else
            {
                MessageBox.Show("Name length must be between 1 and 50!", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
    }
}
