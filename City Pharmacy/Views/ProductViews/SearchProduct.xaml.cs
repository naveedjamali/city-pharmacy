﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for SearchProduct.xaml
    /// </summary>
    public partial class SearchProduct : Page
    {
        private ObservableCollection<Product> itemsList;
        private App myApp;
        private readonly ProductDB _productDb;
        public SearchProduct(ProductDB productDb)
        {
            _productDb = productDb;
            InitializeComponent();
            itemsList = new ObservableCollection<Product>();
            lvSearchedItems.ItemsSource = itemsList;
            myApp = (App)Application.Current;
            searchbox.Focus();

        }

        private void Reset_OnClick(object sender, RoutedEventArgs e)
        {
            searchbox.Text = "";
            itemsList.Clear();
            searchLabel.Text = "";
            searchCount.Text = "";
            lvSearchedItems.Visibility = Visibility.Collapsed;
            Listlabels.Visibility = Visibility.Collapsed;


        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            string itemName;
            if (searchbox.Text.Length > 0)
            {
                
                itemsList.Clear();
                itemName = searchbox.Text;
                
                itemsList = _productDb.SearchProductByName(itemName);
                searchLabel.Text = "Showing results for  \"" + itemName + "\"";

                if (itemsList.Any())
                {
                    
                    searchCount.Text = (itemsList.Count>1)?(itemsList.Count + " items found"):("1 item found");
                    lvSearchedItems.ItemsSource = itemsList;
                    lvSearchedItems.Visibility = Visibility.Visible;
                    Listlabels.Visibility = Visibility.Visible;
                }
                
                else
                {
                    searchCount.Text = "No items found";
                    lvSearchedItems.Visibility = Visibility.Collapsed;
            Listlabels.Visibility = Visibility.Collapsed;
                }
            }


        }

        private void EnterKeyPressedOnSearch(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Return || e.Key == Key.Enter)
            {
                searchButton_Click(null,null);
            }
            else if (e.Key == Key.Escape)
            {
                Reset_OnClick(null,null);
            }
        }
    }

     class SearchedItem
     {
         public string Rack { get; set; }
         public string Name { get; set; }

         public SearchedItem(string rack, string name)
         {
             Rack = rack;
             Name = name;
         }
         

     }
}
