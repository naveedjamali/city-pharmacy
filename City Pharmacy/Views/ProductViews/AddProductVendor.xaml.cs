﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for AddProductVendor.xaml
    /// </summary>
    public partial class AddProductVendor : Page
    {
        private readonly ProductDB _productDb;

        public AddProductVendor()
        {
            InitializeComponent();
            _productDb = new ProductDB();

        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            NameTB.Text = "";

        }

        private void Save(object sender, RoutedEventArgs e)
        {

            if (NameTB.Text.Length > 0 && NameTB.Text.Length < 50)
            {
                ProductVendor v = new ProductVendor();
                v.Name = NameTB.Text;

                if (StatusCombobox.SelectedIndex == 0)
                {
                    v.Status = false;
                }
                else if (StatusCombobox.SelectedIndex == 1)
                {
                    v.Status = true;
                }

                if (_productDb.AddNewProductVendor(v))
                {
                    MessageBox.Show("\'" + v.Name + "\' added successfully", "Operation Successful",
                        MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    NameTB.Text = "";
                    v = null;
                }
            }
            else
            {
                MessageBox.Show("Name length must be between 1 and 50!", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
    }
}
