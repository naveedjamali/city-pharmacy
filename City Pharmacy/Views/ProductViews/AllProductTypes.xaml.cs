﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for AllProductTypes.xaml
    /// </summary>
    public partial class AllProductTypes : Page
    {
        private ObservableCollection<ProductType> itemsList;
        private MainWindow parent;

        public AllProductTypes(MainWindow _parent, ProductDB productDb)
        {
            InitializeComponent();

            parent = _parent;
            itemsList = productDb.GetAllProductTypes();
            productTypesList.ItemsSource = itemsList;


        }

        private void viewProductType(object sender, RoutedEventArgs routedEventArgs)
        {
            ContentControl b = (ContentControl)sender;
            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProductType(parent, itemsList.ElementAt(i)));
                    
                    break;
                }
            }
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            parent.TextChanged(sender,e);
        }


        private void SearchProductTypeFromTb(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                SearchProductType(null, null);
            }
        }

        private void SearchProductType(object sender, RoutedEventArgs e)
        {
            if (searchProductTypeTB.Text.Length > 0)
            {
                ProductType p = parent.ProductDb.SearchProductTypeById(Int32.Parse(searchProductTypeTB.Text));
                if (p != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProductType(parent, p));
                }
                else
                {
                    MessageBox.Show("No Product Type exist with ID: \"" + searchProductTypeTB.Text + "\".");
                    searchProductTypeTB.Focus();
                }
            }
        }

    }
}
