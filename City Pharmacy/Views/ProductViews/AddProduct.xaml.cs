﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for AddProduct.xaml
    /// </summary>
    public partial class AddProduct : Page
    {

        private Product product;
        private readonly ObservableCollection<ProductType> productTypes;
        private readonly ObservableCollection<ProductVendor> productVendors;
        private readonly ProductDB _productDb;


        public AddProduct()
        {
            InitializeComponent();

            _productDb = new ProductDB();
            productTypes = _productDb.GetAllProductTypes();
            productTypeCombobox.ItemsSource = productTypes;
            productVendors = _productDb.GetAllProductVendors();
            productVendorCombobox.ItemsSource = productVendors;

            productStatusCombobox.SelectedIndex = 0;
            productTypeCombobox.SelectedIndex = 0;
            productVendorCombobox.SelectedIndex = 0;

        }
        private void textChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;


             tb.Text = Regex.Replace(tb.Text, "[^0-9]+", "");

        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            NameTB.Text = "";
            rackNumTB.Text = "";
            minTempTB.Text = "";
            maxTempTB.Text = "";
            descriptionTB.Text = "";
            productPrice.Text = "";

            productStatusCombobox.SelectedIndex = 0;
            productTypeCombobox.SelectedIndex = 0;
            productVendorCombobox.SelectedIndex = 0;
        }

        private void AddNewProduct(object sender, RoutedEventArgs e)
        {
            if (ValidateForm())
            {
                Product p = new Product();
                
                p.Name = NameTB.Text;

                if (productStatusCombobox.SelectedIndex == 1)
                {
                    p.Status = true;
                }
                else
                {
                    p.Status = false;
                }

                p.RackNum = rackNumTB.Text;

                p.LastModified = DateTime.Now;

                try
                {
                    if (minTempTB.Text.Length > 0)
                        p.MinTemp = Int32.Parse(minTempTB.Text);
                }
                catch (InvalidCastException exception)
                {
                    Console.WriteLine(exception.Message);
                    p.MinTemp = product.MinTemp;


                }

                try
                {
                    if (maxTempTB.Text.Length > 0)
                        p.MaxTemp = Int32.Parse(maxTempTB.Text);
                }
                catch (InvalidCastException exception)
                {
                    Console.WriteLine(exception.Message);
                    p.MaxTemp = product.MaxTemp;


                }

                p.Description = descriptionTB.Text;

                try
                {
                    p.Price = decimal.Parse(productPrice.Text);
                }
                catch (InvalidCastException invalidCastException)
                {
                    Console.WriteLine(invalidCastException);
                    p.Price = product.Price;

                }

                p.ProductType = (ProductType)productTypeCombobox.SelectedItem;
                p.Vendor = (ProductVendor)productVendorCombobox.SelectedItem;

                try
                {
                    if (_productDb.AddNewProduct(p))
                    {
                        MessageBox.Show("\'"+p.Name + "\' added successfully!","Operation Successful",MessageBoxButton.OK,MessageBoxImage.Asterisk);
                        NameTB.Text = "";
                        rackNumTB.Text = "";
                        minTempTB.Text = "";
                        maxTempTB.Text = "";
                        descriptionTB.Text = "";
                        productPrice.Text = "";

                        p = null;

                    }
                    else
                    {
                        MessageBox.Show("\'" + p.Name + "\' not added successfully!", "Operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (SqlException sqlException)
                {

                    MessageBox.Show("Some Database Error; Please try again...");
                }

            }

        }

        /// <summary>
        /// Performs validation of the edit form.
        /// </summary>
        /// <returns>True if all the entries are valid to create a product</returns>
        private bool ValidateForm()
        {
            String errorMsg = "Error: ";

            if (NameTB.Text.Length > 0)
            {
                if (productPrice.Text.Length > 0)
                {
                    decimal price;
                    if (decimal.TryParse(productPrice.Text, NumberStyles.Currency, null, out price))
                    {

                        if (price >= 0)
                        {

                            return true;
                        }
                        else
                        {
                            productPrice.Focus();
                            errorMsg += "Price cannot be negative";
                        }
                    }
                    else
                    {
                        productPrice.Focus();
                        errorMsg += "Value entered in price is not a number. Please re-enter the price ";
                    }
                }
                else
                {
                    productPrice.Focus();
                    errorMsg += "Please enter Price";
                }
            }
            else
            {
                NameTB.Focus();
                errorMsg += "Please enter name";
            }

            MessageBox.Show(errorMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            return false;
        }


    }
}
