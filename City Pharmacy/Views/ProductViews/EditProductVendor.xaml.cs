﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for EditProductVendor.xaml
    /// </summary>
    public partial class EditProductVendor : Page
    {
        private ProductVendor vendor;
        private readonly ProductDB _productDb;

        public EditProductVendor(ProductVendor _vendor)
        {
            InitializeComponent();

            vendor = _vendor;
            _productDb = new ProductDB();

            IDTB.Text = vendor.ID.ToString();
            NameTB.Text = vendor.Name;

            productVendorStatusCombobox.SelectedIndex = !vendor.Status ? 0 : 1;
        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            IDTB.Text = vendor.ID.ToString();
            NameTB.Text = vendor.Name;

            productVendorStatusCombobox.SelectedIndex = !vendor.Status ? 0 : 1;

        }

        private void updateProductVendor(object sender, RoutedEventArgs e)
        {
            if (NameTB.Text.Length > 0)
            {
                ProductVendor v = new ProductVendor();
                v.ID = vendor.ID;
                v.Name = NameTB.Text;
                if (productVendorStatusCombobox.SelectedIndex == 1)
                {
                    v.Status = true;
                }
                else
                {
                    v.Status = false;
                }

                try
                {

                    if (_productDb.UpdateProductVendor(v))
                    {
                        MessageBox.Show("\'" + v.Name + "\' updated successfully!", "Operation Successful",
                            MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        vendor = v;

                    }
                    else
                    {
                        MessageBox.Show("\'" + v.Name + "\' updating failed due to some error!", "Operation failed",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (SqlException sqlException)
                {

                    MessageBox.Show("Some Database Error; Please try again...");
                }



            }
        }
    }
}
