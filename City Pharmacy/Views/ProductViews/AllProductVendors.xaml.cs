﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.ProductViews
{
    /// <summary>
    /// Interaction logic for AllProductVendors.xaml
    /// </summary>
    public partial class AllProductVendors : Page
    {
        private ObservableCollection<ProductVendor> itemsList;
        private MainWindow parent;

        public AllProductVendors(MainWindow _parent, ProductDB productDb)
        {
            InitializeComponent();

            parent = _parent;
            itemsList = productDb.GetAllProductVendors();
            productVendorsList.ItemsSource = itemsList;
        }

        private void viewProductVendor(object sender, RoutedEventArgs routedEventArgs)
        {
            ContentControl b = (ContentControl)sender;

            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProductVendor(parent, itemsList.ElementAt(i)));
                    //MessageBox.Show(itemsList[i].Name);
                    break;
                }
            }
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            
            parent.TextChanged(sender,e);
        }

        private void SearchProductVendorFromTb(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                SearchProductVendor(null, null);
            }
        }

        private void SearchProductVendor(object sender, RoutedEventArgs e)
        {
            if (searchProductVendorTB.Text.Length > 0)
            {
                ProductVendor p = parent.ProductDb.SearchProductVendorById(Int32.Parse(searchProductVendorTB.Text));
                if (p != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewProductVendor(parent, p));
                }
                else
                {
                    MessageBox.Show("No Product Vendor exist with ID: \"" + searchProductVendorTB.Text + "\".");
                    searchProductVendorTB.Focus();
                }
            }
        }

    }
}
