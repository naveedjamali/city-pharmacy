﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Views.PurchaseViews;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Section = System.Windows.Documents.Section;
using Table = MigraDoc.DocumentObjectModel.Tables.Table;

namespace City_Pharmacy.Views.ReportViews
{
    /// <summary>
    /// Interaction logic for ProfitView.xaml
    /// </summary>
    public partial class ProfitView : Page
    {

        private ObservableCollection<Account> itemsList;
        private MainWindow parent;
        private readonly AccountDB accountDb;
        private DateTime StartDate, EndDate;


        private SaleDB saleDb;

        private decimal Sales;
        private decimal SalesCost;
        private decimal GrossProfit;
        private decimal Expenses;
        private decimal NetProfit;


        public ProfitView()
        {
            InitializeComponent();

            accountDb = new AccountDB();

            Sales = 0;

            SalesCost = 0;

            saleDb = new SaleDB();

            accountsList.ItemsSource = itemsList;

            StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            StartDateDP.SelectedDate = StartDate;
            StartDateDP.DisplayDate = StartDate;

            EndDateDP.SelectedDate = EndDate;
            EndDateDP.DisplayDate = EndDate;



        }

        private void CalculateProfit(object sender, RoutedEventArgs e)
        {
            if (StartDate != null)
            {
                if (EndDate != null)
                {

                    StartDate = StartDateDP.SelectedDate.Value;
                    EndDate = EndDateDP.SelectedDate.Value;

                    Sales = saleDb.GetTotalSales(StartDate, EndDate);
                    SalesCost = saleDb.GetTotalSalesCost(StartDate, EndDate);

                    GrossProfit = Sales - SalesCost;


                    SaleAmountLbl.Text = Sales + "";
                    SaleAmountTotalLbl.Text = Sales + "";

                    SalesCostAmountLbl.Text = SalesCost + "";
                    SaleCostAmountTotalLbl.Text = SalesCost + "";

                    GrossProfitLbl.Text = (Sales - SalesCost) + " ";

                    itemsList = accountDb.GetExpenseTransactions(StartDate, EndDate);

                    accountsList.ItemsSource = itemsList;

                    foreach (Account account in itemsList)
                    {
                        if (account.Name.Equals("Purchase") || account.Balance==0)
                        {
                            itemsList.Remove(account);
                            break;
                        }

                    }

                    Expenses = 0;
                    if (itemsList != null)
                    {
                        foreach (Account account in itemsList)
                        {
                            try
                            {
                                Expenses += account.Balance;
                            }
                            catch (Exception exception)
                            {


                            }
                        }
                    }

                    if (Expenses == 0)
                    {


                        accountsList.Visibility = Visibility.Collapsed;
                    }

                    TotalExpensesLbl.Text = Expenses + " ";

                    NetProfit = GrossProfit - Expenses;

                    NetProfitLbl.Text = NetProfit + "";


                    ProfitViewPanel.Visibility = Visibility.Visible;
                }
            }
        }


        private void PrintProfit(object sender, RoutedEventArgs e)
        {
            Print.Print printer = new Print.Print();

            printer.PrintProfit(StartDate, EndDate, Sales, SalesCost, GrossProfit, Expenses, NetProfit, itemsList);
        }
    }

}
