﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Transaction;

namespace City_Pharmacy.Views.TransactionViews
{
    /// <summary>
    /// Interaction logic for ViewTransaction.xaml
    /// </summary>
    public partial class ViewTransaction : Page
    {
        private MainWindow parent;
        private Transaction transaction;

        public ViewTransaction(MainWindow window,Transaction _transaction)
        {
            InitializeComponent();
            parent = window;
            transaction = _transaction;

            IdLabel.Content = transaction.ID;
            amountLBL.Content = transaction.Amount;
            statusLbl.Content = transaction.Status?"Enabled":"Blocked";
            DrAccName.Content = transaction.DrAcc.Name;
            CrAccName.Content = transaction.CrAcc.Name;
            DateLbl.Content = transaction.Date.ToString("dd-MMM-yyyy");
            DescLbl.Text = transaction.Description;
            UserLbl.Content = transaction.User.Name;
            ModifiedLabel.Content = transaction.ModifiedID == 0 ? "No Previous Record" : transaction.ModifiedID+" ";
            ModifiedIdLayout.Visibility = transaction.ModifiedID > 1 ? Visibility.Visible : Visibility.Collapsed;
            EditTransactionBTN.IsEnabled = transaction.Status;

        }

        private void EditTransaction(object sender, RoutedEventArgs e)
        {
            if (transaction.Status)
            {
                parent.bodyFrame.NavigationService.Navigate(new EditTransaction(parent, transaction));
            }
            /*else
            {
                Transaction modifiedTransaction = new TransactionDB().GetTransactionByID(transaction.ModifiedID);
               

                MessageBoxResult result = MessageBox.Show("You cannot modify this transaction. This transaction is already modified.\n" +
                                                          "Do you want to open modified transaction?","Transaction is Blocked", MessageBoxButton.YesNo,MessageBoxImage.Exclamation);
                if (result.Equals(MessageBoxResult.Yes))
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewTransaction(parent,modifiedTransaction));
                }

            }*/
            
        }

        private void ViewModifiedTransaction(object sender, RoutedEventArgs routedEventArgs)
        {
            if (transaction.ModifiedID != 0 )
            {
                Transaction previousTransaction = new TransactionDB().GetTransactionByID(transaction.ModifiedID);
                if (previousTransaction != null)
                {

                parent.bodyFrame.NavigationService.Navigate(new ViewTransaction(parent,previousTransaction));
                }
                else
                {
                    MessageBox.Show("No Transaction found with TID: " + transaction.ModifiedID);
                }
            }
        }
    }
}
