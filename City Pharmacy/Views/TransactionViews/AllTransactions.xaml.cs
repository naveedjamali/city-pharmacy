﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Transaction;
using City_Pharmacy.Views.AccountViews;

namespace City_Pharmacy.Views.TransactionViews
{
    /// <summary>
    /// Interaction logic for AllTransactions.xaml
    /// </summary>
    public partial class AllTransactions : Page
    {
        private ObservableCollection<Transaction> itemsList;
        private MainWindow parent;
        private TransactionDB transactionDb;

        public AllTransactions(MainWindow _parent)
        {
            InitializeComponent();
            
            parent = _parent;

            transactionDb = new TransactionDB();

            itemsList = transactionDb.GetAllTransactions();
            
            trasactionsListView.ItemsSource = itemsList;
        }

        public AllTransactions(MainWindow _parent, Account account)
        {
            InitializeComponent();

            parent = _parent;

            transactionDb = new TransactionDB();

            itemsList = transactionDb.GetTransactionsByAccount(account);

            trasactionsListView.ItemsSource = itemsList;
        }
        

        private void viewTransaction(object sender, RoutedEventArgs routedEventArgs)
        {
            ContentControl b = (ContentControl)sender;
            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                  
                    parent.bodyFrame.NavigationService.Navigate(new ViewTransaction(parent, itemsList.ElementAt(i)));
                    //MessageBox.Show(itemsList[i].ID + ": " + itemsList[i].Amount + ": " + itemsList[i].CrAcc.Name + ": " + itemsList[i].DrAcc.Name);
                    break;
                }
            }

        }


        private void SearchTransactionFromTb(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return) SearchTransaction(null, null);
        }

        private void SearchTransaction(object sender, RoutedEventArgs e)
        {
            if (searchTransactionTB.Text.Length > 0)
            {
                var a = parent.TransactionDb.GetTransactionByID(int.Parse(searchTransactionTB.Text));

                if (a != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewTransaction(parent, a));
                }
                else
                {
                    MessageBox.Show("No Transaction exist with ID: \"" + searchTransactionTB.Text + "\".");
                    searchTransactionTB.Focus();
                }
            }
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            parent.TextChanged(sender,e);
        }
    }
}
