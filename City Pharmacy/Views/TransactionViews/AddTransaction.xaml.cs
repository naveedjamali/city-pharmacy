﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Transaction;

namespace City_Pharmacy.Views.TransactionViews
{
    /// <summary>
    /// Interaction logic for AddTransaction.xaml
    /// </summary>
    public partial class AddTransaction : Page
    {
        private Transaction transaction;
        private TransactionDB transDB;
        private readonly AccountDB accountDb;
        private MainWindow parent;
        private readonly ObservableCollection<Account> AllAccountsForCr;
        private readonly ObservableCollection<Account> AllAccountsForDr;

        public AddTransaction(MainWindow window)
        {
            InitializeComponent();
            parent = window;
            accountDb = new AccountDB();
            AllAccountsForCr = accountDb.GetAllAccounts();
            CrAccCombobox.ItemsSource = AllAccountsForCr;
            AllAccountsForDr = accountDb.GetAllAccounts();
            DrAccCombobox.ItemsSource = AllAccountsForDr;
            transDB = new TransactionDB();
            transaction = new Transaction();
            DrAccCombobox.SelectedIndex = 0;
            CrAccCombobox.SelectedIndex = 0;
            AmountTB.Text = "0";
            DateTB.SelectedDate = DateTime.Now;
            DateTB.DisplayDate = DateTB.SelectedDate.Value;
        }

        private void ResetFields(object sender, RoutedEventArgs e)
        {
            DrAccCombobox.SelectedIndex = 0;
            CrAccCombobox.SelectedIndex = 0;
            AmountTB.Text = "0";
            DateTB.SelectedDate = DateTime.Now;
            DateTB.DisplayDate = DateTB.SelectedDate.Value;
            descriptionTB.Text = "";
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Error: ";
            if (AmountTB.Text.Length > 0)
            {
                decimal Amount;
                if (decimal.TryParse(AmountTB.Text, NumberStyles.Currency, null, out Amount))
                {
                    if (Amount >= 0)
                    {
                        if (((Account) CrAccCombobox.SelectedItem).ID == ((Account) DrAccCombobox.SelectedItem).ID)
                        {
                            MessageBox.Show("Crediting Account and Debiting Account must be different", "Error",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                        if (DateTB.SelectedDate.Value < DateTime.Now)
                        {
                            transaction.User = parent.GetAppUser();
                            transaction.Description = descriptionTB.Text;
                            transaction.Amount = Amount;
                            transaction.Date = DateTB.SelectedDate.Value;
                            transaction.Status = true;
                            transaction.CrAcc = (Account) CrAccCombobox.SelectedItem;
                            transaction.DrAcc = (Account) DrAccCombobox.SelectedItem;
                            try
                            {
                                int NewTransID = transDB.AddTransaction(transaction);
                                if (NewTransID > 0)
                                {
                                    MessageBoxResult result = MessageBox.Show("Transaction Added successfully", "Added",
                                        MessageBoxButton.OK, MessageBoxImage.Information);
                                    parent.bodyFrame.NavigationService.Navigate(
                                        new ViewTransaction(parent, transDB.GetTransactionByID(NewTransID)));
                                    return;
                                }
                            }
                            catch (Exception exception)
                            {
                                MessageBox.Show(exception.Message);
                            }
                        }
                        else
                        {
                            DateTB.Focus();
                            errorMsg += "Date must be today or previous date";
                        }
                    }
                    else
                    {
                        AmountTB.Focus();
                        errorMsg += "Amount cannot be negative";
                    }
                }
                else
                {
                    AmountTB.Focus();
                    errorMsg += "Value entered in Amount is not a number. Please re-enter the price ";
                }
            }
            else
            {
                AmountTB.Focus();
                errorMsg += "Enter amount";
            }

            MessageBox.Show("Error: " + errorMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}

