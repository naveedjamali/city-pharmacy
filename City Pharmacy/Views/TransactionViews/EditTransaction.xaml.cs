﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Transaction;

namespace City_Pharmacy.Views.TransactionViews
{
    /// <summary>
    /// Interaction logic for EditTransaction.xaml
    /// </summary>
    public partial class EditTransaction : Page
    {
        private Transaction transaction;
        private TransactionDB transDB;
        private readonly AccountDB accountDb;
        private readonly TransactionDB transactionDb;
        private MainWindow parent;

        private readonly ObservableCollection<Account> AllAccountsForCr;
        private readonly ObservableCollection<Account> AllAccountsForDr;

        public EditTransaction(MainWindow window, Transaction t)
        {
            InitializeComponent();
            parent = window;
            
            accountDb = new AccountDB();
            transactionDb = new TransactionDB();

            AllAccountsForCr = accountDb.GetAllAccounts();
            CrAccCombobox.ItemsSource = AllAccountsForCr;

            AllAccountsForDr = accountDb.GetAllAccounts();
            DrAccCombobox.ItemsSource = AllAccountsForDr;
            

            transaction = t;
            transDB = new TransactionDB();


            IDTB.Text = transaction.ID.ToString();
            AmountTB.Text = transaction.Amount.ToString();

            DateTB.DisplayDate = transaction.Date.Date;
            ;
            DateTB.SelectedDate = transaction.Date;

            descriptionTB.Text = transaction.Description;
            foreach (Account a in AllAccountsForCr)
            {
                if (a.ID == transaction.CrAcc.ID)
                {
                    CrAccCombobox.SelectedIndex = AllAccountsForCr.IndexOf(a);
                    break;
                }

                
            }
            foreach (Account a in AllAccountsForDr)
            {
                

                if (a.ID == transaction.DrAcc.ID)
                {
                    DrAccCombobox.SelectedIndex = AllAccountsForDr.IndexOf(a);
                    break;
                }
            }
            


        }

      

        private void updateTransaction(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Error: ";

            if (transaction.Date < DateTB.SelectedDate.Value || DateTB.SelectedDate.Value < DateTime.Now)
            {


                if (AmountTB.Text.Length > 0)
                {

                    decimal Amount;
                    if (decimal.TryParse(AmountTB.Text, NumberStyles.Currency, null, out Amount))
                    {

                        if (Amount >= 0)
                        {
                            if (!(
                                    (
                                        (
                                            Amount == transaction.Amount)
                                        &&
                                        (transaction.CrAcc.ID == ((Account) CrAccCombobox.SelectedItem).ID)
                                        &&
                                        (transaction.DrAcc.ID == ((Account) DrAccCombobox.SelectedItem).ID)
                                    )
                                    ||
                                    (
                                        ((Account) DrAccCombobox.SelectedItem).ID ==
                                        ((Account) CrAccCombobox.SelectedItem).ID
                                    )
                                )
                            )
                            {

                                /**
                                * Cancel original Transaction.
                                */


                                /**
                                 * Create a completely new transaction.
                                 */

                                Transaction mTrans = new Transaction();
                                mTrans.ID = transaction.ID;
                                mTrans.User = parent.GetAppUser();
                                mTrans.Description = descriptionTB.Text;
                                mTrans.Amount = Amount;
                                mTrans.Date = DateTime.Now;
                                mTrans.Status = true;
                                mTrans.CrAcc = (Account) CrAccCombobox.SelectedItem;
                                mTrans.DrAcc = (Account) DrAccCombobox.SelectedItem;



                                try
                                {
                                    int cancelID = transDB.CancelTransaction(transaction, parent.GetAppUser());

                                    if (cancelID > 0)
                                    {
                                        try
                                        {

                                            int NewTransID = transDB.AddTransaction(mTrans);

                                            if (NewTransID > 0)
                                            {
                                                MessageBoxResult result = MessageBox.Show(
                                                    "Transaction Updated successfully",
                                                    "Updated",
                                                    MessageBoxButton.OK, MessageBoxImage.Information);
                                                parent.bodyFrame.NavigationService.Navigate(
                                                    new ViewTransaction(parent,
                                                        transDB.GetTransactionByID(NewTransID)));
                                                return;
                                            }
                                        }
                                        catch (Exception exception)
                                        {

                                            MessageBox.Show(exception.Message);
                                        }


                                    }
                                }
                                catch (Exception exception)
                                {

                                    MessageBox.Show("Error occured while handling transaction", "Error",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                                }




                            }
                            else
                            {

                                AmountTB.Focus();
                                errorMsg += "Change Crediting Account or Debiting Account to update transaction";

                            }


                        }
                        else
                        {
                            AmountTB.Focus();
                            errorMsg += "Price cannot be negative";
                        }
                    }
                    else
                    {
                        AmountTB.Focus();
                        errorMsg += "Value entered in price is not a number. Please re-enter the price ";
                    }
                }
                else
                {
                    AmountTB.Focus();
                    errorMsg += "Please enter Price";
                }
            }
            else
            {
                DateTB.Focus();
                errorMsg += "Date must not be lesser than or equal to original date and greater than today!";
            }



            MessageBox.Show("Error: " + errorMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            


        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            

            IDTB.Text = transaction.ID.ToString();
            AmountTB.Text = transaction.Amount.ToString();

            DateTB.DisplayDate = transaction.Date.Date;
            ;
            DateTB.SelectedDate = transaction.Date;

            descriptionTB.Text = transaction.Description;
            foreach (Account a in AllAccountsForCr)
            {
                if (a.ID == transaction.CrAcc.ID)
                {
                    CrAccCombobox.SelectedIndex = AllAccountsForCr.IndexOf(a);
                    break;
                }


            }
            foreach (Account a in AllAccountsForDr)
            {


                if (a.ID == transaction.DrAcc.ID)
                {
                    DrAccCombobox.SelectedIndex = AllAccountsForDr.IndexOf(a);
                    break;
                }
            }

            

        }

        private void CancelTransaction(object sender, RoutedEventArgs e)
        {

            if (ConfirmCheckBox.IsChecked.Value)
            {
                MessageBoxResult result =
                    MessageBox.Show("Are you sure to cancel the transaction? This will create a reverse transaction",
                        "Confirm Cancellation", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result.Equals(MessageBoxResult.Yes))
                {
                    int updatedID = transDB.CancelTransaction(transaction, parent.GetAppUser());
                    if (updatedID > 0)
                    {
                        MessageBox.Show("Transaction Canceled successfully", "Updated",
                            MessageBoxButton.OK, MessageBoxImage.Information);

                        parent.bodyFrame.NavigationService.Navigate(new ViewTransaction(parent,
                            transDB.GetTransactionByID(updatedID)));

                    }
                }
            }
            else
            {
                MessageBox.Show("Check the confirm CheckBox first","Check box is unchecked",MessageBoxButton.OK,MessageBoxImage.Error);
                ConfirmCheckBox.Focus();
            }

           

            
        }

        
    }
}
