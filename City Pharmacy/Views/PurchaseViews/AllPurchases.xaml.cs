﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Purchase;

namespace City_Pharmacy.Views.PurchaseViews
{
    /// <summary>
    /// Interaction logic for AllPurchases.xaml
    /// </summary>
    public partial class AllPurchases : Page
    {
        private ObservableCollection<PurchaseOrder> itemsList;
        private MainWindow parent;
        private StockDB purchaseDb;


        public AllPurchases(MainWindow _parent)
        {
            InitializeComponent();

            parent = _parent;

            purchaseDb = new StockDB();

            itemsList = purchaseDb.GetAllPurchases();

            purchaseListView.ItemsSource = itemsList;
            


        }

        private void viewPurchase(object sender, RoutedEventArgs routedEventArgs)
        {
            ContentControl b = (ContentControl)sender;
            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewPurchase(itemsList.ElementAt(i)));

                    break;
                }
            }

        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            
            parent.TextChanged(sender,e);
        }


        private void SearchPurchaseByID(object sender, RoutedEventArgs routedEventArgs)
        {
            if (searchPurchase.Text.Length > 0)
            {
                var p = new StockDB().GetPurchaseByID(int.Parse(searchPurchase.Text));

                if (p != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewPurchase(p));
                }
                else
                {
                    MessageBox.Show("No Purchase exist with ID: \"" + searchPurchase.Text + "\".");
                    searchPurchase.Focus();
                }
            }
        }

        private void SearchPurchaseTV(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return) SearchPurchaseByID(sender, null);
        }

    }
}
