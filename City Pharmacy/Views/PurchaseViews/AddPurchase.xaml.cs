﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Product;
using City_Pharmacy.Models.Purchase;
using City_Pharmacy.Models.Transaction;
using MessageBox = System.Windows.MessageBox;

namespace City_Pharmacy.Views.PurchaseViews
{
    /// <summary>
    /// Interaction logic for AddPurchase.xaml
    /// </summary>
    public partial class AddPurchase : Page
    {
        private readonly ProductDB productDb;
        public ObservableCollection<Product> AllProductsList;
        public int purchaseQTY;
        public decimal purchaseCost;
        public DateTime purchaseDate;
        public ObservableCollection<PurchaseDetail> OrderDetailList;

        public AddPurchase()
        {
            InitializeComponent();
            productDb = new ProductDB();

            AllProductsList = productDb.GetAllProducts();
            SelectProductCombobox.ItemsSource = AllProductsList;

            purchaseDate = DateTime.Now;
            
            OrderDetailList = new ObservableCollection<PurchaseDetail>();

            OrderDetailListView.ItemsSource = OrderDetailList;







        }

        private void EditPurchaseDetailItem(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            int tag = (Int32)b.Tag;
            PurchaseDetail p;

            for (int i = 0; i < OrderDetailList.Count; i++)
            {
                if (OrderDetailList.ElementAt(i).Product.ID == tag)
                {
                    p = new PurchaseDetail(OrderDetailList.ElementAt(i));
                    SelectProductCombobox.SelectedItem = p.Product;
                    ProductPurchaseQuantityTB.Text = p.Quantity + "";
                    ProductPurchaseCostTB.Text = p.Cost + "";

                    break;
                }

            }
            SetTotal();

        }

        private void AddItemToPurchaseList(object sender, RoutedEventArgs routedEventArgs)
        {
            
            Product p;
            int qty;
            decimal cost;

            if (IsFormValid())
            {
                p = (Product) SelectProductCombobox.SelectionBoxItem;
                qty = Int32.Parse(ProductPurchaseQuantityTB.Text);
                cost = decimal.Parse(ProductPurchaseCostTB.Text);

                

                if (ExistInList(p))
                {
                    MessageBoxResult result = 
                    MessageBox.Show("Selected product already Exist in the list. Do you want to replace the product?\n\nPress\n" +
                                    "*YES:\tReplace Existing\n" +
                                    "*No: \tDon't Replace", "Selected Product already exist in the list", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                    if (result==MessageBoxResult.Yes)
                    {
                        PurchaseDetail purchase = new PurchaseDetail();
                        purchase.Product = p;
                        purchase.Cost = cost;
                        purchase.Quantity = qty;

                        for (int i = 0; i < OrderDetailList.Count; i++)
                        {
                            if (OrderDetailList.ElementAt(i).Product.ID == p.ID)
                            {
                                OrderDetailList.RemoveAt(i);
                                break;
                            }
                        }

                        AddItemInPurchaseList(purchase);

                        SelectProductCombobox.SelectedIndex = -1;
                        ProductPurchaseCostTB.Text = "";
                        ProductPurchaseQuantityTB.Text = "";
                        SelectProductCombobox.Focus();
                    }
                }
                else
                {


                    PurchaseDetail purchase = new PurchaseDetail();
                    purchase.Product = p;
                    purchase.Cost = cost;
                    purchase.Quantity = qty;

                    AddItemInPurchaseList(purchase);

                    SelectProductCombobox.SelectedIndex = -1;
                    ProductPurchaseCostTB.Text = "";
                    ProductPurchaseQuantityTB.Text = "";
                    SelectProductCombobox.Focus();
                }
                
                
            }
  
        }

        private void SavePurchase(object sender, RoutedEventArgs e)
        {
            
            StockDB db = new StockDB();
            if (PurchaseDateDP.SelectedDate != null)
            {
                if (OrderDetailList.Count > 0)
                {
                    int OrderID = db.AddNewPurchase(OrderDetailList, purchaseCost, purchaseDate);
                    
                        db.GetPurchaseByID(OrderID);
                    

                    MessageBox.Show("Stock Added Successfully");

                    ResetAll(null, null);
                }
                else
                {
                    MessageBox.Show("Error: Purchase list is empty...", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Empty Date is not allowed...");
            }


        }

        private bool IsFormValid()
        {
            if (SelectProductCombobox.SelectedItem != null)
            {
                int q=0;
                if (Int32.TryParse(ProductPurchaseQuantityTB.Text, NumberStyles.Number, null, out q))
                {
                    decimal c;
                    if (Decimal.TryParse(ProductPurchaseCostTB.Text, NumberStyles.Currency, null, out c) )
                    {
                        return true;
                    }
                    else
                    {
                        ProductPurchaseCostTB.Focus();
                        MessageBox.Show("Cost must be greater than zero.", "invalid Cost", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return false;
                    }
                }
                else
                {
                    ProductPurchaseQuantityTB.Focus();
                    MessageBox.Show("Enter positive number in Quantity Field.", "invalid Quantity", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return false;
                }
            }
            else
            {
                SelectProductCombobox.Focus();
                MessageBox.Show("Choose product to add in the list.", "Product not selected", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            
            
        }

        private void AddItemInPurchaseList(PurchaseDetail detail)
        {
            PurchaseDetail d = new PurchaseDetail(detail);
            
            OrderDetailList.Add(d);

            SetTotal();
            
        }

        private void SelectProductCombobox_OnSelected(object sender, RoutedEventArgs e)
        {
            
        }

        private void ProductPurchaseQuantityTB_OnKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ProductPurchaseCostTB.Focus();
            }
        }

        private void ProductPurchaseCostTB_OnKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ProductPurchaseAddButton.Focus();
            }
        }

        private void ProductPurchaseAddButton_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                AddItemToPurchaseList(sender, null);
            }
        }

        private void SetTotal()
        {

            TotalItemsCountLbl.Content = OrderDetailList.Count;
            decimal cost = 0;
            foreach (PurchaseDetail p in OrderDetailList)
            {
                cost += p.Cost;
            }

            purchaseCost = cost;
            TotalItemsCostLbl.Content = cost + "";
           
        }

        private bool ExistInList(Product _p)
        {
            foreach (PurchaseDetail p in OrderDetailList)
            {
                if (p.Product.ID == _p.ID)
                {
                    return true;
                }
            }

            return false;
        }

        private void RemoveItemFromList(object sender, RoutedEventArgs e)
        {

            Button b = (Button) sender;

            int tag = (Int32)b.Tag;
            for (int i = 0; i < OrderDetailList.Count; i++)
            {
                if (OrderDetailList.ElementAt(i).Product.ID == tag)
                {
                    OrderDetailList.RemoveAt(i);
                    break;
                }
            }

            SetTotal();
        }

        private void ResetAll(object sender, RoutedEventArgs e)
        {
            OrderDetailList.Clear();
            SelectProductCombobox.SelectedIndex = -1;
            ProductPurchaseCostTB.Text = "";
            ProductPurchaseQuantityTB.Text = "";
            SelectProductCombobox.Focus();
            SetTotal();

        }
        
    }
}

