﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Purchase;

namespace City_Pharmacy.Views.PurchaseViews
{
    /// <summary>
    /// Interaction logic for ViewPurchase.xaml
    /// </summary>
    public partial class ViewPurchase : Page
    {
        private readonly PurchaseOrder Order;



        public ViewPurchase(PurchaseOrder _order)
        {
            InitializeComponent();
            Order = _order;

            DetailListLV.ItemsSource = Order.PurchaseList;

            OrderIdLabel.Content = Order.ID;
            PurchaseDateLabel.Content = Order.Transaction.Date.ToShortDateString();
            TotalCostLabel.Content = "Rs. "+Order.Transaction.Amount + "/-";
            TotalItemsLabel.Content = Order.PurchaseList.Count;

            ReturnPurchaseBtn.IsEnabled = Order.Status;
        }

        private void ReturnPurchase(object sender, RoutedEventArgs e)
        {

            MessageBoxResult result = MessageBox.Show("Are you sure you want to return all the Purchase?",
                "Confirm Return", MessageBoxButton.YesNo, MessageBoxImage.Hand);

            if (result == MessageBoxResult.Yes)
            {
                int success =
                    new StockDB().ReturnPurchase(Order);
                if (success > 1)
                {
                    MessageBox.Show("Purchase returned successfully", "Purchase Returned", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }
                else if(success==-1)
                {
                    MessageBox.Show("Stock is not available in such quantity", "Purchase return Failed", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
                
            
        }

        private void PrintPurchase(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented yet.");

        }
    }
}
