﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.User;
using City_Pharmacy.Views.ProductViews;

namespace City_Pharmacy.Views.GeneralViews
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        private MainWindow parent;
        private readonly ProductDB _productDb;
        public Login(MainWindow _parent, ProductDB productDb)
        {
            InitializeComponent();
            _productDb = productDb;
            parent = _parent;
            userNameTB.Focus();
        }

        

        
        private void GoBack(object sender, RoutedEventArgs routedEventArgs)
        {
            this.NavigationService.GoBack();
        }

        private async void initiateLogin(object sender, RoutedEventArgs e)
        {
            
            if (userNameTB.Text.Length > 0 && userPasswordTB.Password.Length > 0)
            {
                string uname = userNameTB.Text;
                string pword = userPasswordTB.Password;
                
                EnableLoginButton(false);

                bool b;
                LoginProgressBar.Visibility = Visibility.Visible;
                b  = await UserDB.getInstance().Login(uname, pword);
                    
                if (b)
                {
                    LoginSuccess();
                    EnableLoginButton(true);
                }
                else
                {

                    LoginProgressBar.Visibility = Visibility.Hidden;
                    MessageBox.Show("Parameter did not match, try again with correct UserName and Password");
                    EnableLoginButton(true);
                }
                

                
            }
            else
            {
                MessageBox.Show("User Name and Password cannot be empty fields","Error",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        public void LoginSuccess()
        {
            parent.LabelUsername.Content = ((App)Application.Current).user.Name;
            parent.labelUserRole.Content = ((App) Application.Current).user.Role.Name;
            
            parent.bodyFrame.Navigate(new SearchProduct(_productDb));
            parent.FeaturesTabContainer.Visibility = Visibility.Visible;
            parent.LabelUsername.Visibility = Visibility.Visible;
            parent.labelUserRole.Visibility = Visibility.Visible;
            parent.loggedIn = true;
            parent.labelLogin.Content = "Logout";
            setTabs(((App)Application.Current).user.Role);
        }

        private void changeFocus(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                if (userNameTB.IsFocused)
                {
                    userPasswordTB.Focus();
                }
                else if (userPasswordTB.IsFocused)
                {
                    initiateLogin(sender,e);
                }
            }
           
        }

        private void setTabs(UserRole role)
        {
           
            if (role.Name.Equals("ADMIN"))
            {
                parent.FeaturesTabContainer.Visibility = Visibility.Visible;
                foreach (var tab in parent.FeaturesTabContainer.Items)
                {
                    TabItem item = (TabItem) tab;
                    item.Visibility = Visibility.Visible;
                }

                parent.SalesTab.IsSelected = true;

            }
            else 
            {

                foreach (var tab in parent.FeaturesTabContainer.Items)
                {
                    TabItem item = (TabItem)tab;
                    item.Visibility = Visibility.Collapsed;
                }

                if (role.Name.Equals("STOCK CLERK"))
                {
                    parent.StockTab.Visibility = Visibility.Visible;
                    parent.ProductsTab.Visibility = Visibility.Visible;
                    parent.StockTab.IsSelected = true;
                }
                else if (role.Name.Equals("ACCOUNTANT"))
                {
                    parent.AccountsTab.Visibility = Visibility.Visible;
                    parent.AccountsTab.IsSelected = true;
                }
                else if (role.Name.Equals("SALESMAN"))
                {
                    parent.SalesTab.Visibility = Visibility.Visible;
                    parent.SalesTab.IsSelected = true;
                }
                

            }
        }

        public void EnableLoginButton(bool b)
        {
            parent.labelLogin.IsEnabled = b;
        }

    }
}
