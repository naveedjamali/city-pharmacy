﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Views.ProductViews;

namespace City_Pharmacy.Views.AccountViews
{
    /// <summary>
    /// Interaction logic for AllAccounts.xaml
    /// </summary>
    public partial class AllAccounts : Page
    {
        private ObservableCollection<Account> itemsList;
        private MainWindow parent;

        public AllAccounts(MainWindow _parent, AccountDB accountDb)
        {
            InitializeComponent();
            parent = _parent;
            itemsList = accountDb.GetAllAccounts();
            accountsList.ItemsSource = itemsList;
        }

        private void viewAccount(object sender, RoutedEventArgs e)
        {

            ContentControl b = (ContentControl)sender;
            int tag = Int32.Parse(b.Tag.ToString());


            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemsList.ElementAt(i).ID == tag)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewAccount(parent, itemsList.ElementAt(i)));
                    
                    break;
                }
            }
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            parent.TextChanged(sender,e);
        }

        private void SearchAccountFromTb(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return) SearchAccount(null, null);
        }

        private void SearchAccount(object sender, RoutedEventArgs e)
        {
            if (searchAccountTB.Text.Length > 0)
            {
                var a = parent.AccountDb.SearchAccountByID(int.Parse(searchAccountTB.Text));
                if (a != null)
                {
                    parent.bodyFrame.NavigationService.Navigate(new ViewAccount(parent, a));
                }
                else
                {
                    MessageBox.Show("No Account exist with ID: \"" + searchAccountTB.Text + "\".");
                    searchAccountTB.Focus();
                }
            }
        }

    }
}
