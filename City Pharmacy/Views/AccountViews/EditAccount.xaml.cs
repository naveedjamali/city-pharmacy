﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Product;

namespace City_Pharmacy.Views.AccountViews
{
    /// <summary>
    /// Interaction logic for EditAccount.xaml
    /// </summary>
    public partial class EditAccount : Page
    {
        private Account account;
        private readonly ObservableCollection<AccountType> accountTypes;
        
        private readonly AccountDB _accountDb;

        public EditAccount(Account _account)
        {
            InitializeComponent();

            _accountDb = new AccountDB();
            account = _account;
            accountTypes = _accountDb.GetAllAccountTypes();
            accountTypeCombobox.ItemsSource = accountTypes;

            IDTB.Text = account.ID.ToString();
            NameTB.Text = account.Name;
           
            foreach (AccountType t in accountTypeCombobox.ItemsSource)
            {
                if (account.AccType.ID == t.ID)
                {
                    accountTypeCombobox.SelectedItem = t;
                }
            }



            StatusCombobox.SelectedIndex = !account.Status ? 0 : 1;



        }

        private void resetFields(object sender, RoutedEventArgs e)
        {

            IDTB.Text = account.ID.ToString();
            NameTB.Text = account.Name;

            foreach (AccountType t in accountTypeCombobox.ItemsSource)
            {
                if (account.AccType.ID == t.ID)
                {
                    accountTypeCombobox.SelectedItem = t;
                }
            }



            StatusCombobox.SelectedIndex = !account.Status ? 0 : 1;


        }

        private void updateAccount(object sender, RoutedEventArgs e)
        {

            if (NameTB.Text.Length>0)
            {
                Account a = new Account();

                a.ID = account.ID;
                a.Balance = account.Balance;

                a.Name = NameTB.Text;

                if (StatusCombobox.SelectedIndex == 1)
                {
                    a.Status = true;
                }
                else
                {
                    a.Status = false;
                }

              
                a.AccType = (AccountType)accountTypeCombobox.SelectedItem;
               
                try
                {

                    if (_accountDb.UpdateAccount(a))
                    {
                        MessageBox.Show("\'" + a.Name + "\' updated successfully!", "Operation Successful", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show("\'" + a.Name + "\' updating failed due to some error!", "Operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (SqlException sqlException)
                {

                    MessageBox.Show("Some Database Error; Please try again...");
                }

            }
        }
    }
}
