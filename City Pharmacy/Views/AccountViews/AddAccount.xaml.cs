﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.Account;

namespace City_Pharmacy.Views.AccountViews
{
    /// <summary>
    /// Interaction logic for AddAccount.xaml
    /// </summary>
    public partial class AddAccount : Page
    {
        private Account account;
        private readonly ObservableCollection<AccountType> accountTypes;
        private readonly AccountDB _accountDb;

        public AddAccount()
        {
            InitializeComponent();

            _accountDb = new AccountDB();
            accountTypes = _accountDb.GetAllAccountTypes();
            TypeCombobox.ItemsSource = accountTypes;

            TypeCombobox.SelectedIndex = 0;

        }

        private void Save(object sender, RoutedEventArgs e)
        {

            if (NameTB.Text.Length > 0)
            {
                Account a = new Account();
                a.Name = NameTB.Text;
                a.AccType = (AccountType) TypeCombobox.SelectedItem;
                a.Balance = 0;
                a.Status = AccountStatusToggleButton.IsChecked.Value;
                

                try
                {
                    if (_accountDb.AddNewAccount(a))
                    {
                        MessageBox.Show("\'" + a.Name + "\' added successfully!", "Operation Successful", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        a = null;
                    }
                    else
                    {
                        MessageBox.Show("\'" + a.Name + "\' not added. Account with same name exists already", "Operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (SqlException sqlException)
                {

                    MessageBox.Show("Some Database Error; Please try again...");
                }
            }
        }

        private void resetFields(object sender, RoutedEventArgs e)
        {
            TypeCombobox.SelectedIndex = 0;
            AccountStatusToggleButton.IsChecked = false;


        }
    }
}
