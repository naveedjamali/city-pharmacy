﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Views.TransactionViews;

namespace City_Pharmacy.Views.AccountViews
{
    /// <summary>
    /// Interaction logic for ViewAccount.xaml
    /// </summary>
    public partial class ViewAccount : Page
    {
        private MainWindow parent;
        private Account account;
        public ViewAccount(MainWindow window, Account ac)
        {
            InitializeComponent();


            parent = window;
            account = ac;


            IdLabel.Content = account.ID;
            nameLBL.Content = account.Name;
            statusLbl.Content = account.Status ? "Enabled" : "Disabled";
            BalanceLbl.Content = account.Balance.ToString();
            TypeLbl.Content = account.AccType.Name;

        }

        private void EditAccount(object sender, RoutedEventArgs e)
        {

            parent.bodyFrame.NavigationService.Navigate(new EditAccount(account));
        }

        private void ViewTransactions(object sender, RoutedEventArgs e)
        {

            parent.bodyFrame.NavigationService.Navigate(new AllTransactions(parent, account));
        }
    }
}
