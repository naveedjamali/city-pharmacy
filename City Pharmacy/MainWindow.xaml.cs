﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using City_Pharmacy.DBLayer;
using City_Pharmacy.Models.User;
using City_Pharmacy.Views.AccountViews;
using City_Pharmacy.Views.GeneralViews;
using City_Pharmacy.Views.ProductViews;
using City_Pharmacy.Views.PurchaseViews;
using City_Pharmacy.Views.ReportViews;
using City_Pharmacy.Views.SaleViews;
using City_Pharmacy.Views.StockViews;
using City_Pharmacy.Views.TransactionViews;
using City_Pharmacy.Views.UserViews;

namespace City_Pharmacy
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly App myApp;
        public bool loggedIn;
        public TransactionDB TransactionDb { get; }
        public ProductDB ProductDb { get; }
        public AccountDB AccountDb { get; }

        public MainWindow()
        {
            InitializeComponent();

            myApp = (App) Application.Current;
            ProductDb = new ProductDB();
            AccountDb = new AccountDB();
            TransactionDb = new TransactionDB();
            loggedIn = false;
            setInterface();
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            if (!loggedIn)
            {
                bodyFrame.Navigate(new Login(this, ProductDb));
            }
            else if (loggedIn)
            {
                myApp.user = null;
                loggedIn = false;
                myApp.mainWindow = this;

                FeaturesTabContainer.Visibility = Visibility.Collapsed;
                LabelUsername.Visibility = Visibility.Collapsed;
                labelUserRole.Visibility = Visibility.Collapsed;

                labelLogin.Content = "Login Here";

                bodyFrame.Navigate(new Login(this, ProductDb));
            }
        }

        public void setInterface()
        {
            LabelUsername.Content = "Please Login";
            labelDate.Content = DateTime.Today.Date.ToString("MMM dd, yyyy");
            bodyFrame.Navigate(new SearchProduct(ProductDb));
            FeaturesTabContainer.Visibility = Visibility.Collapsed;
        }

        private void addNewUser(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new NewUser(this));
        }

        private void openAllUsers(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllUsers(this));
        }

        private void tabChanged(object sender, SelectionChangedEventArgs e)
        {
            // Page p = new SearchProduct();

            var bc = new BrushConverter();

            //Brush b= (Brush)bc.ConvertFrom("#FFXXXXXX");

            if (SalesTab.IsSelected)
                bodyFrame.NavigationService.Navigate(new AddSale(this));
            else if (StockTab.IsSelected)
                bodyFrame.NavigationService.Navigate(new CurrentStockValueView(this, new ProductDB()));
            else if (AccountsTab.IsSelected)
                bodyFrame.NavigationService.Navigate(new AllAccounts(this, new AccountDB()));
            else if (ProductsTab.IsSelected)
                bodyFrame.NavigationService.Navigate(new AllProducts(this, new ProductDB()));
            else if (usersTab.IsSelected) bodyFrame.NavigationService.Navigate(new AllUsers(this));
        }

        /**
        * To check the input for numbers only input.
        */
        public void TextChanged(object sender, TextChangedEventArgs e)
        {
            ((TextBox) sender).Text = Regex.Replace(((TextBox) sender).Text, "[^0-9]+", "");
        }

        public User GetAppUser()
        {
            return myApp.user;
        }

        private void ViewLogedInUserProfile(object sender, MouseButtonEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new ViewUser(this, myApp.user));
        }

        private void OpenAllProducts(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllProducts(this, ProductDb));
        }

        public App getApp()
        {
            return myApp;
        }

        private void AddNewProduct(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddProduct());
        }

        private void OpenAllProductTypes(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllProductTypes(this, ProductDb));
        }

        private void AddNewProductType(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddProductType());
        }

        private void OpenAllProductVendors(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllProductVendors(this, ProductDb));
        }

        private void AddNewProductVendors(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddProductVendor());
        }

        private void OpenAllAccounts(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllAccounts(this, AccountDb));
        }

        private void AddNewAccount(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddAccount());
        }

        
        private void OpenAllTransactions(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllTransactions(this));
        }

        private void AddNewTransaction(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddTransaction(this));
        }

        private void AllPurchases(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllPurchases(this));
        }

        private void CurrentStockValue(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new CurrentStockValueView(this, ProductDb));
        }

        private void AddNewStock(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddPurchase());
        }

        private void OpenAllSales(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AllSales(this));
        }

        private void AddNewSale(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new AddSale(this));
        }

        private void OpenProfitView(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new ProfitView());
        }

        private void SearchRackNumber(object sender, RoutedEventArgs e)
        {
            bodyFrame.NavigationService.Navigate(new SearchProduct(ProductDb));
        }
    }
}