﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using City_Pharmacy.Models.User;

namespace City_Pharmacy
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public Window mainWindow;
        public User user { get; set; }
        public static string ConnString;
        private string server;
        private int port;
        private string username;
        private string database;
        private string password;

        public string ConnectionString()
        {
            server = Settings.Default.server;
            port = Settings.Default.port;
            database = Settings.Default.dbName;
            username = Settings.Default.dbUsername;
            password = Settings.Default.dbPassword;
            
            return ConnString = "Data Source=tcp:" + server + "," + port + ";" + "Initial Catalog=" + database + ";Trusted_Connection=Yes;";;
        }

        public SqlConnection SqlConnection()
        {
            return new SqlConnection(ConnectionString());
        }

      
    }
    
}
