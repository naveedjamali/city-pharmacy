﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using City_Pharmacy.Models.Account;
using City_Pharmacy.Models.Sale;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.Printing;
using Font = MigraDoc.DocumentObjectModel.Font;

namespace City_Pharmacy.Print
{
   

    public class Print
    {
        public Print()
        {

        }

        public void PrintSale(SaleOrder order)
        {
            Document document = new Document();
            document.Info.Author = "Naveed Jamali";
            document.Info.Keywords = "City Pharmacy";

            Unit mWidth, height;

            PageSetup.GetPageSize(PageFormat.A5,out mWidth, out height);
            Unit width = mWidth;
            width.Value = mWidth.Value +30;
            Section section = document.AddSection();
            section.PageSetup.PageHeight = height;
            section.PageSetup.PageWidth = width;
            section.PageSetup.LeftMargin = 20;
            section.PageSetup.RightMargin = 10;
            section.PageSetup.TopMargin =10;

            Table Headtable = new Table();

            Column column = Headtable.AddColumn(width-40);
            column.Format.Alignment = ParagraphAlignment.Center;

            MigraDoc.DocumentObjectModel.Font headfont = new MigraDoc.DocumentObjectModel.Font("Bauhaus 93", 18);
            MigraDoc.DocumentObjectModel.Font addressfont = new MigraDoc.DocumentObjectModel.Font("Times New Roman", 9);
            //// Set Title
            Row row = Headtable.AddRow();

            Cell cell = row.Cells[0];
            cell.Format.Font.ApplyFont(headfont);

            cell.Borders.Top.Visible = false;
            cell.Borders.Left.Visible = false;
            cell.Borders.Right.Visible = false;
            cell.Borders.Bottom.Visible = false;

            cell.AddParagraph("City Pharmacy");

            //// Set Address
            row = Headtable.AddRow();
            cell = row.Cells[0];

            cell.Format.Font.Color = Colors.Black;
            cell.Format.Alignment = ParagraphAlignment.Center;
            cell.Format.Font.ApplyFont(addressfont);

            cell.Borders.Bottom.Visible = false;
            cell.Borders.Left.Visible = false;
            cell.Borders.Right.Visible = false;
            cell.Borders.Top.Visible = false;

            cell.AddParagraph("Mall Road Branch, Phone: +924236521478");

            //// INVOICE
            addressfont = new MigraDoc.DocumentObjectModel.Font("Calibri", 16);
            row = Headtable.AddRow();
            cell = row.Cells[0];

            cell.Format.Font.Color = Colors.Black;
            cell.Format.Alignment = ParagraphAlignment.Center;
            cell.Format.Font.ApplyFont(addressfont);
            cell.Borders.Bottom.Visible = false;
            cell.Borders.Left.Visible = false;
            cell.Borders.Right.Visible = false;
            cell.Borders.Top.Visible = false;

            cell.AddParagraph("--SALE INVOICE--");

            
            
            document.LastSection.Add(Headtable);


            ////
            /// Write Body Section
            ///

            Table invoiceTable = new Table();


            Column C1 = invoiceTable.AddColumn(width / 4 - 20);
            Column C2 = invoiceTable.AddColumn(width / 4 - 20);
            Column C3 = invoiceTable.AddColumn(width / 4 - 20);
            Column C4 = invoiceTable.AddColumn(width / 4 - 20);


            MigraDoc.DocumentObjectModel.Font headerFont = new MigraDoc.DocumentObjectModel.Font("Century Gothic    ", 11);
            MigraDoc.DocumentObjectModel.Font ItemsFont = new MigraDoc.DocumentObjectModel.Font("Times New Roman", 9);

            /***
             * Add row and cell for Invoice ID: ID Date: Date
             */

            Row IDRow = invoiceTable.AddRow();

            Cell a1 = IDRow.Cells[0];
            Cell a2 = IDRow.Cells[1];
            Cell a3 = IDRow.Cells[2];
            Cell a4 = IDRow.Cells[3];

            a1.Format.Font.Color = Colors.Black;
            a1.Format.Alignment = ParagraphAlignment.Left;
            a1.Format.Font.ApplyFont(headerFont);
            a1.Borders.Bottom.Visible = false;
            a1.Borders.Left.Visible = false;
            a1.Borders.Right.Visible = false;
            a1.Borders.Top.Visible = false;
            a1.AddParagraph("Invoice ID: ");

            a2.Format.Font.Color = Colors.Black;
            a2.Format.Alignment = ParagraphAlignment.Left;
            a2.Format.Font.ApplyFont(headerFont);
            a2.Borders.Bottom.Visible = false;
            a2.Borders.Left.Visible = false;
            a2.Borders.Right.Visible = false;
            a2.Borders.Top.Visible = false;
            a2.AddParagraph(order.ID+" ");

            a3.Format.Font.Color = Colors.Black;
            a3.Format.Font.Bold = true;
            a3.Format.Alignment = ParagraphAlignment.Right;
            a3.Format.Font.ApplyFont(headerFont);
            a3.Borders.Bottom.Visible = false;
            a3.Borders.Left.Visible = false;
            a3.Borders.Right.Visible = false;
            a3.Borders.Top.Visible = false;
            a3.AddParagraph("Date: ");

            a4.Format.Font.Color = Colors.Black;
            a4.Format.Font.Bold = true;
            a4.Format.Alignment = ParagraphAlignment.Right;
            a4.Format.Font.ApplyFont(headerFont);
            a4.Borders.Bottom.Visible = false;
            a4.Borders.Left.Visible = false;
            a4.Borders.Right.Visible = false;
            a4.Borders.Top.Visible = false;
            a4.AddParagraph(order.Transaction.Date.ToString("dd-MMM-yyyy"));

            


            document.LastSection.Add(invoiceTable);

            ////
            /// Write Body Section
            ///

            Table BodyTable = new Table();


            Column D1 = BodyTable.AddColumn(width / 2 - 20);
            Column D2 = BodyTable.AddColumn(width / 6 - 20);
            Column D3 = BodyTable.AddColumn(width / 6 - 20);
            Column D4 = BodyTable.AddColumn(width / 6 - 20);
            

            /***
             * Add row and cell for total amount: amount, Total items: items
             */

            Row HeaderRow = BodyTable.AddRow();

            Cell c1 = HeaderRow.Cells[0];
            Cell c2 = HeaderRow.Cells[1];
            Cell c3 = HeaderRow.Cells[2];
            Cell c4 = HeaderRow.Cells[3];


            c1.Format.Font.Color = Colors.Black;
            c1.Format.Alignment = ParagraphAlignment.Left;
            c1.Format.Font.ApplyFont(headerFont);
            c1.Borders.Bottom.Visible = true;
            c1.Borders.Left.Visible = true;
            c1.Borders.Right.Visible = false;
            c1.Borders.Top.Visible = true;
            c1.AddParagraph("Item Name");

            c2.Format.Font.Color = Colors.Black;
            c2.Format.Alignment = ParagraphAlignment.Right;
            c2.Format.Font.ApplyFont(headerFont);
            c2.Borders.Bottom.Visible = true;
            c2.Borders.Left.Visible = false;
            c2.Borders.Right.Visible = false;
            c2.Borders.Top.Visible = true;
            c2.AddParagraph("Unit Price");


            c3.Format.Font.Color = Colors.Black;
            c3.Format.Alignment = ParagraphAlignment.Right;
            c3.Format.Font.ApplyFont(headerFont);
            c3.Borders.Bottom.Visible = true;
            c3.Borders.Left.Visible = false;
            c3.Borders.Right.Visible = false;
            c3.Borders.Top.Visible = true;
            c3.AddParagraph( "Quantity ");

            c4.Format.Font.Color = Colors.Black;
            c4.Format.Alignment = ParagraphAlignment.Left;
            c4.Format.Font.ApplyFont(headerFont);
            c4.Borders.Bottom.Visible = true;
            c4.Borders.Left.Visible = false;
            c4.Borders.Right.Visible = true;
            c4.Borders.Top.Visible = true;
            c4.AddParagraph("Amount");
            
            document.LastSection.Add(BodyTable);


            ////
            /// Write Body Section
            ///

            Table ItemTable = new Table();


            Column E1 = ItemTable.AddColumn(width / 2 - 20);
            Column E2 = ItemTable.AddColumn(width / 6 - 20);
            Column E3 = ItemTable.AddColumn(width / 6 - 20);
            Column E4 = ItemTable.AddColumn(width / 6 - 20);



            foreach (SaleDetail d in order.SaleList)
            {

                Row ItemRow = ItemTable.AddRow();

                Cell r = ItemRow.Cells[0];
                Cell s = ItemRow.Cells[1];
                Cell t = ItemRow.Cells[2];
                Cell u = ItemRow.Cells[3];


                r.Format.Font.Color = Colors.Black;
                r.Format.Alignment = ParagraphAlignment.Left;
                r.Format.Font.ApplyFont(ItemsFont);
                r.Borders.Bottom.Visible = false;
                r.Borders.Left.Visible = true;
                r.Borders.Right.Visible = false;
                r.Borders.Top.Visible = false;
                r.AddParagraph(d.Product.Name);

                s.Format.Font.Color = Colors.Black;
                s.Format.Alignment = ParagraphAlignment.Right;
                s.Format.Font.ApplyFont(ItemsFont);
                s.Borders.Bottom.Visible = false;
                s.Borders.Left.Visible = false;
                s.Borders.Right.Visible = false;
                s.Borders.Top.Visible = false;
                s.AddParagraph(d.UnitPrice+"");


                t.Format.Font.Color = Colors.Black;
                t.Format.Alignment = ParagraphAlignment.Right;
                t.Format.Font.ApplyFont(ItemsFont);
                t.Borders.Bottom.Visible = false;
                t.Borders.Left.Visible = false;
                t.Borders.Right.Visible = false;
                t.Borders.Top.Visible = false;
                t.AddParagraph(d.Quantity+"");

                u.Format.Font.Color = Colors.Black;
                u.Format.Alignment = ParagraphAlignment.Right;
                u.Format.Font.ApplyFont(ItemsFont);
                u.Borders.Bottom.Visible = false;
                u.Borders.Left.Visible = false;
                u.Borders.Right.Visible = true;
                u.Borders.Top.Visible = false;
                u.Format.Font.Bold = true;
                u.AddParagraph(d.TotalAmount+"");


            }



            document.LastSection.Add(ItemTable);


            Table TotalTable = new Table();


            Column F1 = TotalTable.AddColumn(width / 4 - 20);
            Column F2 = TotalTable.AddColumn(width / 4 - 20);
            Column F3 = TotalTable.AddColumn(width / 4 - 20);
            Column F4 = TotalTable.AddColumn(width / 4 - 20);

            /***
             * Add row and cell for total amount: amount, Total items: items
             */

            Row TotalRow = TotalTable.AddRow();

            Cell b1 = TotalRow.Cells[0];
            Cell b2 = TotalRow.Cells[1];
            Cell b3 = TotalRow.Cells[2];
            Cell b4 = TotalRow.Cells[3];

            b1.Format.Font.Color = Colors.Black;
            b1.Format.Alignment = ParagraphAlignment.Left;
            b1.Format.Font.ApplyFont(headerFont);
            b1.Borders.Bottom.Visible = false;
            b1.Borders.Left.Visible = false;
            b1.Borders.Right.Visible = false;
            b1.Borders.Top.Visible = true;
            b1.AddParagraph("Total Items:");

            b2.Format.Font.Color = Colors.Black;
            b2.Format.Alignment = ParagraphAlignment.Left;
            b2.Format.Font.ApplyFont(headerFont);
            b2.Borders.Bottom.Visible = false;
            b2.Borders.Left.Visible = false;
            b2.Borders.Right.Visible = false;
            b2.Borders.Top.Visible = true;
            b2.AddParagraph(order.SaleList.Count + " ");

            b3.Format.Font.Color = Colors.Black;
            b3.Format.Alignment = ParagraphAlignment.Right;
            b3.Format.Font.ApplyFont(headerFont);
            b3.Borders.Bottom.Visible = false;
            b3.Borders.Left.Visible = false;
            b3.Borders.Right.Visible = false;
            b3.Borders.Top.Visible = true;
            b3.Format.Font.Bold = true;
            b3.AddParagraph("Total Amount:");

            b4.Format.Font.Color = Colors.Black;
            b4.Format.Font.Bold = true;
            b4.Format.Alignment = ParagraphAlignment.Right;
            b4.Format.Font.ApplyFont(headerFont);
            b4.Borders.Bottom.Visible = false;
            b4.Borders.Left.Visible = false;
            b4.Borders.Right.Visible = false;
            b4.Borders.Top.Visible = true;
            b4.AddParagraph("Rs. " + order.Amount + "/-");



            document.LastSection.Add(TotalTable);


            MigraDoc.DocumentObjectModel.Font thankufont = new MigraDoc.DocumentObjectModel.Font("Century Gothic", 9);

            Table ThankTable = new Table();

            ThankTable.TopPadding = 20;

            Column t1 = ThankTable.AddColumn(width-40);
            t1.Format.Alignment = ParagraphAlignment.Center;

            Row z = ThankTable.AddRow();

            Cell y = z.Cells[0];

            y.Format.Font.Color = Colors.Black;
            y.Format.Alignment = ParagraphAlignment.Center;
            y.Format.Font.ApplyFont(thankufont);
            y.Borders.Bottom.Visible = false;
            y.Borders.Left.Visible = false;
            y.Borders.Right.Visible = false;
            y.Borders.Top.Visible = false;

            y.AddParagraph("*** THANK YOU FOR PURCHASING ***");



            /*
             * Comment Table Start
             */

            Table CommentsTable = new Table();
            CommentsTable.AddColumn(width - 100);
            Row CommentsRow = CommentsTable.AddRow();
            CommentsRow.TopPadding = 20;

            Cell commentCell = CommentsRow.Cells[0];

            commentCell.Borders.Visible = false;
            commentCell.Format.Font.ApplyFont(new Font("Calibri", 8));
            commentCell.AddParagraph("* - This is a computer generated document and does not require any signature");

            document.LastSection.Add(CommentsTable);

            /**
             * Comment Table End
             */



            document.LastSection.Add(ThankTable);





            /////

            // Create a renderer
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer();

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;

            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            // Save and show the document
            try
            {
                string fileName = "SaleInvoice_" + order.ID + "_" + DateTime.Now.Millisecond + "_.pdf";
                pdfRenderer.PdfDocument.Save(fileName);
                Process.Start(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            





        }

        public void PrintProfit(
            DateTime startDate, 
            DateTime endDate, 
            decimal sales, 
            decimal purchases, 
            decimal grossProfit, 
            decimal expenses, 
            decimal netProfit, 
            ObservableCollection<Account> accountList
            )
        {
            Document document = new Document();
            document.Info.Author = "Naveed Jamali";
            document.Info.Keywords = "City Pharmacy";

            Unit mWidth, height;

            PageSetup.GetPageSize(PageFormat.A5, out mWidth, out height);
            Unit width = mWidth;
            width.Value = mWidth.Value + 30;
            MigraDoc.DocumentObjectModel.Section section = document.AddSection();
            section.PageSetup.PageHeight = height;
            section.PageSetup.PageWidth = width;
            section.PageSetup.LeftMargin = 20;
            section.PageSetup.RightMargin = 10;
            section.PageSetup.TopMargin = 10;

            MigraDoc.DocumentObjectModel.Tables.Table TitleTable = new Table();

            Column TitleColumn = TitleTable.AddColumn(width - 40);
            TitleColumn.Format.Alignment = ParagraphAlignment.Center;

            MigraDoc.DocumentObjectModel.Font titleFont = new MigraDoc.DocumentObjectModel.Font("Bauhaus 93", 18);
            MigraDoc.DocumentObjectModel.Font Addressfont = new MigraDoc.DocumentObjectModel.Font("Times New Roman", 9);
            //// Set Title
            Row TitleRow = TitleTable.AddRow();

            Cell TitleCell = TitleRow.Cells[0];
            TitleCell.Format.Font.ApplyFont(titleFont);

            TitleCell.Borders.Top.Visible = false;
            TitleCell.Borders.Left.Visible = false;
            TitleCell.Borders.Right.Visible = false;
            TitleCell.Borders.Bottom.Visible = false;

            TitleCell.AddParagraph("City Pharmacy");

            //// Set Address
            Row AddressRow = TitleTable.AddRow();
            Cell AddressCell = AddressRow.Cells[0];

            AddressCell.Format.Font.Color = MigraDoc.DocumentObjectModel.Colors.Black;
            AddressCell.Format.Alignment = ParagraphAlignment.Center;
            AddressCell.Format.Font.ApplyFont(Addressfont);

            AddressCell.Borders.Bottom.Visible = false;
            AddressCell.Borders.Left.Visible = false;
            AddressCell.Borders.Right.Visible = false;
            AddressCell.Borders.Top.Visible = false;

            AddressCell.AddParagraph("Mall Road Branch, Phone: +924236521478");

            //// INVOICE
            Font SubjectFont = new MigraDoc.DocumentObjectModel.Font("Calibri", 16);
            Row SubjectRow = TitleTable.AddRow();

            SubjectRow.TopPadding = 20;
            Cell SubjectCell = SubjectRow.Cells[0];

            SubjectCell.Format.Font.Color = MigraDoc.DocumentObjectModel.Colors.Black;
            SubjectCell.Format.Alignment = ParagraphAlignment.Center;
            SubjectCell.Format.Font.ApplyFont(SubjectFont);
            SubjectCell.Borders.Bottom.Visible = false;
            SubjectCell.Borders.Left.Visible = false;
            SubjectCell.Borders.Right.Visible = false;
            SubjectCell.Borders.Top.Visible = false;


            SubjectCell.AddParagraph("Profit and Loss Statement");



            document.LastSection.Add(TitleTable);

            ////
            /// Write Strart End Dates
            ///

            Table DateTable = new Table();


            DateTable.AddColumn(width / 4 - 20);
            DateTable.AddColumn(width / 4 - 20);
            DateTable.AddColumn(width / 4 - 20);
            DateTable.AddColumn(width / 4 - 20);


            MigraDoc.DocumentObjectModel.Font DateFont = new MigraDoc.DocumentObjectModel.Font("Century Gothic    ", 11);
            MigraDoc.DocumentObjectModel.Font ItemsFont = new MigraDoc.DocumentObjectModel.Font("Times New Roman", 9);

            /***
             * Add row and cell for Invoice ID: ID Date: Date
             */

            Row IDRow = DateTable.AddRow();

            Cell FromLbl = IDRow.Cells[0];
            Cell FromDate = IDRow.Cells[1];
            Cell ToLbl = IDRow.Cells[2];
            Cell ToDate = IDRow.Cells[3];

            FromLbl.Format.Font.Color = Colors.Black;
            FromLbl.Format.Alignment = ParagraphAlignment.Left;
            FromLbl.Format.Font.ApplyFont(DateFont);
            FromLbl.Borders.Bottom.Visible = false;
            FromLbl.Borders.Left.Visible = false;
            FromLbl.Borders.Right.Visible = false;
            FromLbl.Borders.Top.Visible = false;
            FromLbl.Format.Font.Bold = true;
            FromLbl.AddParagraph("Start Date: ");

            FromDate.Format.Font.Color = Colors.Black;
            FromDate.Format.Alignment = ParagraphAlignment.Left;
            FromDate.Format.Font.ApplyFont(DateFont);
            FromDate.Borders.Bottom.Visible = false;
            FromDate.Borders.Left.Visible = false;
            FromDate.Borders.Right.Visible = false;
            FromDate.Format.Font.Bold = true;
            FromDate.Borders.Top.Visible = false;
            FromDate.AddParagraph(startDate.ToString("dd-MMM-yyyy"));

            ToLbl.Format.Font.Color = Colors.Black;
            ToLbl.Format.Font.Bold = true;
            ToLbl.Format.Alignment = ParagraphAlignment.Right;
            ToLbl.Format.Font.ApplyFont(DateFont);
            ToLbl.Borders.Bottom.Visible = false;
            ToLbl.Borders.Left.Visible = false;
            ToLbl.Borders.Right.Visible = false;
            ToLbl.Borders.Top.Visible = false;
            ToLbl.AddParagraph("Date: ");

            ToDate.Format.Font.Color = Colors.Black;
            ToDate.Format.Font.Bold = true;
            ToDate.Format.Alignment = ParagraphAlignment.Right;
            ToDate.Format.Font.ApplyFont(DateFont);
            ToDate.Borders.Bottom.Visible = false;
            ToDate.Borders.Left.Visible = false;
            ToDate.Borders.Right.Visible = false;
            ToDate.Borders.Top.Visible = false;
            ToDate.AddParagraph(endDate.ToString("dd-MMM-yyyy"));


            document.LastSection.Add(DateTable);
            /**
             * Add Sale Header
             */

            Font SectionHeaderFont = new Font("Century Gothic    ", 12);

            Table SalesSectionTitleTable = new Table();
            SalesSectionTitleTable.AddColumn(width);

            Row SaleSectionTitleRow = SalesSectionTitleTable.AddRow();

            Cell SaleTitle = SaleSectionTitleRow.Cells[0];

            SaleTitle.Format.Font.Color = Colors.Black;
            SaleTitle.Format.Alignment = ParagraphAlignment.Left;
            SaleTitle.Format.Font.ApplyFont(SectionHeaderFont);
            SaleTitle.Borders.Bottom.Visible = false;
            SaleTitle.Borders.Left.Visible = false;
            SaleTitle.Borders.Right.Visible = false;
            SaleTitle.Borders.Top.Visible = false;
            SaleTitle.AddParagraph("Sales ");

            document.LastSection.Add(SalesSectionTitleTable);


            /*
             * Add sale body
             */

            Font AccountFont = new Font("Calibri", 11);
            Table SalesBodyTable = new Table();

            SalesBodyTable.AddColumn(width / 4); SalesBodyTable.AddColumn(width / 4); SalesBodyTable.AddColumn(width / 4); SalesBodyTable.AddColumn(width / 4 - 50);

            Row SaleBodyAccountRow = SalesBodyTable.AddRow();

            Cell SaleAccountTitle = SaleBodyAccountRow.Cells[0], SaleAccountAmount = SaleBodyAccountRow.Cells[2];

            SaleAccountTitle.Format.Font.Color = Colors.Black;
            SaleAccountTitle.Format.Alignment = ParagraphAlignment.Left;
            SaleAccountTitle.Format.Font.ApplyFont(AccountFont);
            SaleAccountTitle.Borders.Bottom.Visible = false;
            SaleAccountTitle.Borders.Left.Visible = false;
            SaleAccountTitle.Borders.Right.Visible = false;
            SaleAccountTitle.Borders.Top.Visible = false;
            SaleAccountTitle.AddParagraph("Sales");

            SaleAccountAmount.Format.Font.Color = Colors.Black;
            SaleAccountAmount.Format.Alignment = ParagraphAlignment.Right;
            SaleAccountAmount.Format.Font.ApplyFont(AccountFont);
            SaleAccountAmount.Borders.Bottom.Visible = false;
            SaleAccountAmount.Borders.Left.Visible = false;
            SaleAccountAmount.Borders.Right.Visible = false;
            SaleAccountAmount.Borders.Top.Visible = false;
            SaleAccountAmount.AddParagraph(sales.ToString());



            Row SalesTotalRow = SalesBodyTable.AddRow();

            Font SectionTotalFont = new Font("Calibri",11);
            SectionTotalFont.Bold = true;
            Cell SalesTotalTitle = SalesTotalRow.Cells[0]; Cell SalesTotalAmount = SalesTotalRow.Cells[3];

            SalesTotalTitle.Format.Font.Color = Colors.Black;
            SalesTotalTitle.Format.Alignment = ParagraphAlignment.Left;
            SalesTotalTitle.Format.Font.ApplyFont(SectionTotalFont);
            SalesTotalTitle.Borders.Bottom.Visible = false;
            SalesTotalTitle.Borders.Left.Visible = false;
            SalesTotalTitle.Borders.Right.Visible = false;
            SalesTotalTitle.Borders.Top.Visible = false;
            SalesTotalTitle.AddParagraph("Total Sales");

            SalesTotalAmount.Format.Font.Color = Colors.Black;
            SalesTotalAmount.Format.Alignment = ParagraphAlignment.Right;
            SalesTotalAmount.Format.Font.ApplyFont(SectionTotalFont);
            SalesTotalAmount.Borders.Bottom.Visible = false;
            SalesTotalAmount.Borders.Left.Visible = false;
            SalesTotalAmount.Borders.Right.Visible = false;
            SalesTotalAmount.Borders.Top.Visible = false;
            SalesTotalAmount.AddParagraph(sales.ToString());


            document.LastSection.Add(SalesBodyTable);

            ///End  of sales
            ///


            /**
             * Purchase Section
             */
            /**
             * Add Purchase Header
             */
            Table PurchaseSectionTitleTable = new Table();
            PurchaseSectionTitleTable.AddColumn(width);

            Row PurchaseSectionTitleRow = PurchaseSectionTitleTable.AddRow();

            Cell PurchaseTitle = PurchaseSectionTitleRow.Cells[0];

            PurchaseTitle.Format.Font.Color = Colors.Black;
            PurchaseTitle.Format.Alignment = ParagraphAlignment.Left;
            PurchaseTitle.Format.Font.ApplyFont(SectionHeaderFont);
            PurchaseTitle.Borders.Bottom.Visible = false;
            PurchaseTitle.Borders.Left.Visible = false;
            PurchaseTitle.Borders.Right.Visible = false;
            PurchaseTitle.Borders.Top.Visible = false;
            PurchaseTitle.AddParagraph("Cost of Sales ");

            document.LastSection.Add(PurchaseSectionTitleTable);


            /*
             * Add Purchase body
             */
             
            Table PurchaseBodyTable = new Table();

            PurchaseBodyTable.AddColumn(width / 4); PurchaseBodyTable.AddColumn(width / 4); PurchaseBodyTable.AddColumn(width / 4); PurchaseBodyTable.AddColumn(width / 4 - 50);

            Row PurchaseBodyAccountRow = PurchaseBodyTable.AddRow();

            Cell PurchaseAccountTitle = PurchaseBodyAccountRow.Cells[0]; Cell PurchaseAccountAmount = PurchaseBodyAccountRow.Cells[2];

            PurchaseAccountTitle.Format.Font.Color = Colors.Black;
            PurchaseAccountTitle.Format.Alignment = ParagraphAlignment.Left;
            PurchaseAccountTitle.Format.Font.ApplyFont(AccountFont);
            PurchaseAccountTitle.Borders.Bottom.Visible = false;
            PurchaseAccountTitle.Borders.Left.Visible = false;
            PurchaseAccountTitle.Borders.Right.Visible = false;
            PurchaseAccountTitle.Borders.Top.Visible = false;

            PurchaseAccountTitle.AddParagraph("Purchases OR Cost of sales");

            PurchaseAccountAmount.Format.Font.Color = Colors.Black;
            PurchaseAccountAmount.Format.Alignment = ParagraphAlignment.Right;
            PurchaseAccountAmount.Format.Font.ApplyFont(AccountFont);
            PurchaseAccountAmount.Borders.Bottom.Visible = false;
            PurchaseAccountAmount.Borders.Left.Visible = false;
            PurchaseAccountAmount.Borders.Right.Visible = false;
            PurchaseAccountAmount.Borders.Top.Visible = false;
            PurchaseAccountAmount.AddParagraph(purchases.ToString());

            /**
             * Total Purchases
             */

            Row PurchaseTotalRow = PurchaseBodyTable.AddRow();

            Cell PurchaseTotalTitle = PurchaseTotalRow.Cells[0]; Cell PurchaseTotalAmount = PurchaseTotalRow.Cells[3];

            PurchaseTotalTitle.Format.Font.Color = Colors.Black;
            PurchaseTotalTitle.Format.Alignment = ParagraphAlignment.Left;
            PurchaseTotalTitle.Format.Font.ApplyFont(SectionTotalFont);
            PurchaseTotalTitle.Borders.Bottom.Visible = false;
            PurchaseTotalTitle.Borders.Left.Visible = false;
            PurchaseTotalTitle.Borders.Right.Visible = false;
            PurchaseTotalTitle.Borders.Top.Visible = false;
            PurchaseTotalTitle.AddParagraph("Total for Cost of Sales");


            PurchaseTotalAmount.Format.Font.Color = Colors.Black;
            PurchaseTotalAmount.Format.Alignment = ParagraphAlignment.Right;
            PurchaseTotalAmount.Format.Font.ApplyFont(SectionTotalFont);
            PurchaseTotalAmount.Borders.Bottom.Visible = false;
            PurchaseTotalAmount.Borders.Left.Visible = false;
            PurchaseTotalAmount.Borders.Right.Visible = false;
            PurchaseTotalAmount.Borders.Top.Visible = false;
            PurchaseTotalAmount.AddParagraph(purchases.ToString());


            document.LastSection.Add(PurchaseBodyTable);

            //// End of Purchases
            ///

            /*
             * Gross Profit Section
             */

            /**
             * Add Gross Profit Header
             */
           
            Table GrossProfitTable = new Table();
            GrossProfitTable.AddColumn(width / 2);
            GrossProfitTable.AddColumn(width / 2 - 50);

            Row GrossProfitRow = GrossProfitTable.AddRow();

            Cell GrossProfitTitle = GrossProfitRow.Cells[0];
            Cell GrossProfitAmount = GrossProfitRow.Cells[1];

            GrossProfitTitle.Format.Font.Color = Colors.Black;
            GrossProfitTitle.Format.Alignment = ParagraphAlignment.Left;
            GrossProfitTitle.Format.Font.ApplyFont(SectionHeaderFont);
            GrossProfitTitle.Borders.Bottom.Visible = true;
            GrossProfitTitle.Borders.Left.Visible = true;
            GrossProfitTitle.Borders.Right.Visible = false;
            GrossProfitTitle.Borders.Top.Visible = true;
            GrossProfitTitle.AddParagraph("Gross Profit");

            GrossProfitAmount.Format.Font.Color = Colors.Black;
            GrossProfitAmount.Format.Alignment = ParagraphAlignment.Right;
            GrossProfitAmount.Format.Font.ApplyFont(SectionHeaderFont);
            GrossProfitAmount.Borders.Bottom.Visible = true;
            GrossProfitAmount.Borders.Left.Visible = false;
            GrossProfitAmount.Borders.Right.Visible = true;
            GrossProfitAmount.Borders.Top.Visible = true;
            GrossProfitAmount.AddParagraph(grossProfit.ToString());

            document.LastSection.Add(GrossProfitTable);


            /**
             * End Gross Profit
             */


            /**
             * Start Expense Section
             */

            /*
             * Expense Header
             */
            

            Table ExpenseSectionTitleTable = new Table();
            ExpenseSectionTitleTable.AddColumn(width);

            Row ExpenseSectionTitleRow = ExpenseSectionTitleTable.AddRow();

            Cell ExpenseSectionTitle = ExpenseSectionTitleRow.Cells[0];

            ExpenseSectionTitle.Format.Font.Color = Colors.Black;
            ExpenseSectionTitle.Format.Alignment = ParagraphAlignment.Left;
            ExpenseSectionTitle.Format.Font.ApplyFont(SectionHeaderFont);
            ExpenseSectionTitle.Borders.Bottom.Visible = false;
            ExpenseSectionTitle.Borders.Left.Visible = false;
            ExpenseSectionTitle.Borders.Right.Visible = false;
            ExpenseSectionTitle.Borders.Top.Visible = false;
            ExpenseSectionTitle.AddParagraph("Expenses");

            document.LastSection.Add(ExpenseSectionTitleTable);


            /**
             * Expese list start
             */

            Font AccountListFont = new Font("Calibri", 10);

            Table ExpenseSectionBodyTable = new Table();
            ExpenseSectionBodyTable.AddColumn(width / 2);
            ExpenseSectionBodyTable.AddColumn(width / 4);

            foreach (Account acc in accountList)
            {
                if (acc.Balance != 0)
                {
                


                Row ExpenseAccountRow = ExpenseSectionBodyTable.AddRow();

                Cell ExpenseAccountTitle = ExpenseAccountRow.Cells[0];
                Cell ExpenseAccountAmount = ExpenseAccountRow.Cells[1];

                ExpenseAccountTitle.Format.Font.Color = Colors.Black;
                ExpenseAccountTitle.Format.Alignment = ParagraphAlignment.Left;
                ExpenseAccountTitle.Format.Font.ApplyFont(AccountListFont);
                ExpenseAccountTitle.Borders.Bottom.Visible = false;
                ExpenseAccountTitle.Borders.Left.Visible = false;
                ExpenseAccountTitle.Borders.Right.Visible = false;
                ExpenseAccountTitle.Borders.Top.Visible = false;
                ExpenseAccountTitle.AddParagraph(acc.Name);

                ExpenseAccountAmount.Format.Font.Color = Colors.Black;
                ExpenseAccountAmount.Format.Alignment = ParagraphAlignment.Right;
                ExpenseAccountAmount.Format.Font.ApplyFont(AccountListFont);
                ExpenseAccountAmount.Borders.Bottom.Visible = false;
                ExpenseAccountAmount.Borders.Left.Visible = false;
                ExpenseAccountAmount.Borders.Right.Visible = false;
                ExpenseAccountAmount.Borders.Top.Visible = false;
                ExpenseAccountAmount.AddParagraph(acc.Balance.ToString());
                }

            }


            document.LastSection.Add(ExpenseSectionBodyTable);

            /*
             *
             * Expense list end
             */

            /*
             * Expense Total
             */
            
            Table ExpenseTotalTable = new Table();
            ExpenseTotalTable.AddColumn(width / 2);
            ExpenseTotalTable.AddColumn(width / 2 - 50);

            Row ExpenseTotalRow = ExpenseTotalTable.AddRow();

            Cell ExpenseTotalTitle = ExpenseTotalRow.Cells[0];
            Cell ExpenseTotalAmount = ExpenseTotalRow.Cells[1];

            ExpenseTotalTitle.Format.Font.Color = Colors.Black;
            ExpenseTotalTitle.Format.Alignment = ParagraphAlignment.Left;
            ExpenseTotalTitle.Format.Font.ApplyFont(SectionTotalFont);
            ExpenseTotalTitle.Borders.Bottom.Visible = false;
            ExpenseTotalTitle.Borders.Left.Visible = false;
            ExpenseTotalTitle.Borders.Right.Visible = false;
            ExpenseTotalTitle.Borders.Top.Visible = false;
            ExpenseTotalTitle.AddParagraph("Total Expenses");

            ExpenseTotalAmount.Format.Font.Color = Colors.Black;
            ExpenseTotalAmount.Format.Alignment = ParagraphAlignment.Right;
            ExpenseTotalAmount.Format.Font.ApplyFont(SectionTotalFont);
            ExpenseTotalAmount.Borders.Bottom.Visible = false;
            ExpenseTotalAmount.Borders.Left.Visible = false;
            ExpenseTotalAmount.Borders.Right.Visible = false;
            ExpenseTotalAmount.Borders.Top.Visible = false;
            ExpenseTotalAmount.AddParagraph(expenses.ToString());

            document.LastSection.Add(ExpenseTotalTable);



            /**
             * Expense Section End
             */

            /*
             * Net Profit Start
             */

            Table NetProfitTable = new Table();
            NetProfitTable.AddColumn(width / 2);
            NetProfitTable.AddColumn(width / 2 - 50);

            Row NetProfitRow = NetProfitTable.AddRow();

            Cell NetProfitTitle = NetProfitRow.Cells[0];
            Cell NetProfitAmount = NetProfitRow.Cells[1];

            NetProfitTitle.Format.Font.Color = Colors.Black;
            NetProfitTitle.Format.Alignment = ParagraphAlignment.Left;
            NetProfitTitle.Format.Font.ApplyFont(SectionHeaderFont);
            NetProfitTitle.Borders.Bottom.Visible = true;
            NetProfitTitle.Borders.Left.Visible = true;
            NetProfitTitle.Borders.Right.Visible = false;
            NetProfitTitle.Borders.Top.Visible = true;
            NetProfitTitle.AddParagraph("Net Profit");

            NetProfitAmount.Format.Font.Color = Colors.Black;
            NetProfitAmount.Format.Alignment = ParagraphAlignment.Right;
            NetProfitAmount.Format.Font.ApplyFont(SectionHeaderFont);
            NetProfitAmount.Borders.Bottom.Visible = true;
            NetProfitAmount.Borders.Left.Visible = false;
            NetProfitAmount.Borders.Right.Visible = true;
            NetProfitAmount.Borders.Top.Visible = true;
            NetProfitAmount.AddParagraph(netProfit.ToString());

            document.LastSection.Add(NetProfitTable);


            /*
             * Net Profit End
             */

            /*
             * Comment Table Start
             */

            Table CommentsTable = new Table();
            CommentsTable.AddColumn(width - 100);
            Row CommentsRow = CommentsTable.AddRow();
            CommentsRow.TopPadding = 20;

            Cell commentCell = CommentsRow.Cells[0];

            commentCell.Borders.Visible = false;
            commentCell.Format.Font.ApplyFont(new Font("Calibri",8));
            commentCell.AddParagraph("* - This is a computer generated document and does not require any signature");

            document.LastSection.Add(CommentsTable);

            /**
             * Comment Table End
             */


            /////

            // Create a renderer
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer();

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;

            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            string fileName = "ProfitLossStatement_" + DateTime.Now.Millisecond + ".pdf";

            // Save and show the document
            try
            {

                pdfRenderer.PdfDocument.Save(fileName);
                Process.Start(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }







        }

    }
}
