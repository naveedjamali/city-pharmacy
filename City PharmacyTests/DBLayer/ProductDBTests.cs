﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using City_Pharmacy.DBLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City_Pharmacy.DBLayer.Tests
{
    [TestClass()]
    public class ProductDBTests
    {
        private const string _connString = "Data Source=tcp:localhost;Initial Catalog=CityPharmacyDB;Trusted_Connection=Yes;";

        [TestMethod()]
        public void SearchProductByNameTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAllProductsTest()
        {
            ProductDB productDb = new ProductDB();
            var actual = productDb.GetAllProducts();
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Count > 0);
        }

        [TestMethod()]
        public void SearchProductByIdTest()
        {
            ProductDB productDb = new ProductDB();
            var actual = productDb.SearchProductById(3);
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.ID, 3);
        }
    }
}