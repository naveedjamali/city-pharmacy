USE [master]
GO
/****** Object:  Database [CityPharmacyDB]    Script Date: 10/7/2018 10:42:03 AM ******/
CREATE DATABASE [CityPharmacyDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CityPharmacyDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\CityPharmacyDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CityPharmacyDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\CityPharmacyDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CityPharmacyDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CityPharmacyDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CityPharmacyDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CityPharmacyDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CityPharmacyDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CityPharmacyDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CityPharmacyDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET RECOVERY FULL 
GO
ALTER DATABASE [CityPharmacyDB] SET  MULTI_USER 
GO
ALTER DATABASE [CityPharmacyDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CityPharmacyDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CityPharmacyDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CityPharmacyDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CityPharmacyDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CityPharmacyDB', N'ON'
GO
USE [CityPharmacyDB]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 10/7/2018 10:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](50) NOT NULL,
	[AccountTypeID] [int] NOT NULL,
	[AccountStatus] [bit] NOT NULL,
	[AccountBalance] [decimal](18, 3) NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 10/7/2018 10:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[AccountTypeID] [int] IDENTITY(1,1) NOT NULL,
	[AccountTypeName] [varchar](50) NOT NULL,
	[AccountTypeStatus] [bit] NOT NULL,
 CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[AccountTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AllTransactions]    Script Date: 10/7/2018 10:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AllTransactions](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionDate] [date] NOT NULL,
	[TransactionAmount] [decimal](18, 3) NOT NULL,
	[TransactionStatus] [bit] NOT NULL,
	[TransactionCrAccID] [int] NOT NULL,
	[TransactionDrAccID] [int] NOT NULL,
	[TransactionUserID] [int] NOT NULL,
	[TransactionDescription] [varchar](50) NULL,
	[TransactionModifiedID] [int] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AllUsers]    Script Date: 10/7/2018 10:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AllUsers](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[JoiningDate] [date] NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Address] [varchar](100) NULL,
	[CNIC] [varchar](13) NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[RoleID] [int] NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 10/7/2018 10:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](50) NOT NULL,
	[ProductStatus] [bit] NULL,
	[RackNumber] [varchar](10) NULL,
	[LastModifiedDate] [datetime] NULL,
	[MinTemperature] [int] NULL,
	[MaxTemperature] [int] NULL,
	[ProductDescription] [varchar](200) NULL,
	[PriceOfTotalStock]  AS ([ProductPrice]*[AvailableStock]),
	[ProductPrice] [decimal](18, 2) NOT NULL,
	[AvailableStock] [int] NULL,
	[ProductTypeID] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 10/7/2018 10:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductType](
	[ProductTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeName] [varchar](50) NOT NULL,
	[ProductTypeStatus] [bit] NULL,
 CONSTRAINT [PK_ProductType_1] PRIMARY KEY CLUSTERED 
(
	[ProductTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductVendor]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductVendor](
	[VendorID] [int] IDENTITY(1,1) NOT NULL,
	[ProductVendorName] [varchar](50) NOT NULL,
	[ProductVendorStatus] [bit] NULL,
 CONSTRAINT [PK_ProductVender] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseDetail]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseDetail](
	[PurchaseDetailID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseOrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Cost] [float] NOT NULL,
	[CostPerItem]  AS ([Cost]/[Quantity]),
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_PurchaseDetail] PRIMARY KEY CLUSTERED 
(
	[PurchaseDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseOrder]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseOrder](
	[PurchaseOrderID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseOrderDate] [date] NOT NULL,
	[PurchaseOrderTransactionID] [int] NOT NULL,
	[PurchaseOrderStatus] [bit] NOT NULL,
 CONSTRAINT [PK_PurchaseOrder] PRIMARY KEY CLUSTERED 
(
	[PurchaseOrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SaleDetail]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaleDetail](
	[SaleDetailID] [int] IDENTITY(1,1) NOT NULL,
	[SaleOrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[PricePerUnit] [float] NOT NULL,
	[Discount] [float] NOT NULL,
	[TotalAmount]  AS ([Quantity]*[PricePerUnit]-[Discount]),
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_SaleDetail_1] PRIMARY KEY CLUSTERED 
(
	[SaleDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SaleOrder]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaleOrder](
	[SaleOrderID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[TID] [int] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_SaleOrder] PRIMARY KEY CLUSTERED 
(
	[SaleOrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (1, N'Sales', 2, 1, CAST(-4244.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (2, N'Purchase', 4, 1, CAST(150.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (3, N'Utilities', 3, 1, CAST(0.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (4, N'Salaries', 3, 1, CAST(0.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (5, N'Cash', 1, 1, CAST(99862.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (6, N'HBL 1215 Manora Branch', 1, 1, CAST(0.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (7, N'Investment Money', 3, 1, CAST(100000.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (8, N'Profit', 1, 1, CAST(0.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (12, N'NBP Sakrand Road Branch', 1, 1, CAST(0.000 AS Decimal(18, 3)))
INSERT [dbo].[Account] ([AccountID], [AccountName], [AccountTypeID], [AccountStatus], [AccountBalance]) VALUES (15, N'Soneri Bank', 1, 1, CAST(0.000 AS Decimal(18, 3)))
SET IDENTITY_INSERT [dbo].[Account] OFF
SET IDENTITY_INSERT [dbo].[AccountType] ON 

INSERT [dbo].[AccountType] ([AccountTypeID], [AccountTypeName], [AccountTypeStatus]) VALUES (1, N'ASSET', 1)
INSERT [dbo].[AccountType] ([AccountTypeID], [AccountTypeName], [AccountTypeStatus]) VALUES (2, N'REVENUE', 1)
INSERT [dbo].[AccountType] ([AccountTypeID], [AccountTypeName], [AccountTypeStatus]) VALUES (3, N'LIABILITY', 1)
INSERT [dbo].[AccountType] ([AccountTypeID], [AccountTypeName], [AccountTypeStatus]) VALUES (4, N'EXPENSE', 1)
SET IDENTITY_INSERT [dbo].[AccountType] OFF
SET IDENTITY_INSERT [dbo].[AllTransactions] ON 

INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (4, CAST(N'2018-01-05' AS Date), CAST(100000.000 AS Decimal(18, 3)), 1, 7, 5, 1, N'Initial Investment', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (5, CAST(N'2018-01-06' AS Date), CAST(150.000 AS Decimal(18, 3)), 0, 5, 2, 1, N'first purchase', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (6, CAST(N'2018-01-07' AS Date), CAST(12.000 AS Decimal(18, 3)), 0, 1, 5, 1, N'first sale', 9)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (8, CAST(N'2018-09-19' AS Date), CAST(150.000 AS Decimal(18, 3)), 1, 2, 5, 1, N'Modified', 5)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (9, CAST(N'2018-09-20' AS Date), CAST(12.000 AS Decimal(18, 3)), 0, 5, 1, 1, N'Modified', 10)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (15, CAST(N'2018-09-20' AS Date), CAST(5454.000 AS Decimal(18, 3)), 0, 1, 2, 1, N'', 16)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (17, CAST(N'2018-09-20' AS Date), CAST(2000.000 AS Decimal(18, 3)), 0, 1, 5, 1, N'', 18)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (18, CAST(N'2018-09-21' AS Date), CAST(2000.000 AS Decimal(18, 3)), 0, 5, 5, 1, N'Cancel transaction of TID: 17', 19)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (19, CAST(N'2018-09-21' AS Date), CAST(2000.000 AS Decimal(18, 3)), 1, 5, 5, 1, N'Cancel transaction of TID: 18', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (20, CAST(N'2018-09-21' AS Date), CAST(2100.000 AS Decimal(18, 3)), 1, 1, 15, 1, N'Reverse Transaction ID: 19
New Description:Cancel', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (21, CAST(N'2018-09-21' AS Date), CAST(5000.000 AS Decimal(18, 3)), 0, 5, 7, 1, N'Some Simple Transaction', 22)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (22, CAST(N'2018-09-21' AS Date), CAST(5000.000 AS Decimal(18, 3)), 0, 7, 5, 1, N'Cancel transaction of TID: 21', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (23, CAST(N'2018-09-22' AS Date), CAST(1500.000 AS Decimal(18, 3)), 0, 2, 3, 1, N'Hello Trans', 24)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (24, CAST(N'2018-09-22' AS Date), CAST(1500.000 AS Decimal(18, 3)), 0, 3, 2, 1, N'Cancel transaction of TID: 23', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (26, CAST(N'2018-09-22' AS Date), CAST(15040.000 AS Decimal(18, 3)), 0, 3, 2, 1, N'Cancel transaction of TID: 25', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (27, CAST(N'2018-09-22' AS Date), CAST(1200.000 AS Decimal(18, 3)), 0, 5, 8, 1, N'1 profit', 30)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (28, CAST(N'2018-09-22' AS Date), CAST(300.000 AS Decimal(18, 3)), 1, 5, 8, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (29, CAST(N'2018-09-22' AS Date), CAST(100.000 AS Decimal(18, 3)), 1, 8, 5, 1, N'cashback', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (30, CAST(N'2018-09-22' AS Date), CAST(1200.000 AS Decimal(18, 3)), 0, 8, 5, 1, N'Cancel transaction of TID: 27', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (36, CAST(N'2018-09-22' AS Date), CAST(100000.000 AS Decimal(18, 3)), 1, 5, 7, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (37, CAST(N'2018-09-23' AS Date), CAST(5200.000 AS Decimal(18, 3)), 0, 1, 5, 1, N'dummy transaction', 38)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (38, CAST(N'2018-09-23' AS Date), CAST(5200.000 AS Decimal(18, 3)), 0, 5, 1, 1, N'Cancel transaction of TID: 37', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (40, CAST(N'2018-09-23' AS Date), CAST(3500.000 AS Decimal(18, 3)), 0, 5, 8, 1, N'', 41)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (41, CAST(N'2018-09-23' AS Date), CAST(3500.000 AS Decimal(18, 3)), 0, 8, 5, 1, N'Cancel transaction of TID: 40', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (42, CAST(N'2018-09-23' AS Date), CAST(3500.000 AS Decimal(18, 3)), 1, 5, 6, 1, N'Reverse Transaction ID: 41
New Description:', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (43, CAST(N'2018-09-23' AS Date), CAST(12821.000 AS Decimal(18, 3)), 1, 5, 2, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (44, CAST(N'2018-09-23' AS Date), CAST(10000.000 AS Decimal(18, 3)), 1, 5, 2, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (45, CAST(N'2018-09-23' AS Date), CAST(10000.000 AS Decimal(18, 3)), 1, 4, 2, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (46, CAST(N'2018-09-23' AS Date), CAST(10000.000 AS Decimal(18, 3)), 1, 4, 2, 1, N'kj', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (47, CAST(N'2018-09-23' AS Date), CAST(10000.000 AS Decimal(18, 3)), 1, 2, 3, 1, N'kj', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (49, CAST(N'2018-09-25' AS Date), CAST(122112121.000 AS Decimal(18, 3)), 1, 6, 5, 1, N'Brand new Transaction', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (50, CAST(N'2018-09-25' AS Date), CAST(122112121.000 AS Decimal(18, 3)), 1, 4, 5, 1, N'Brand new Transaction', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (51, CAST(N'2018-09-25' AS Date), CAST(3333.000 AS Decimal(18, 3)), 1, 5, 8, 1, N'A new transaction to check cancel feature', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (52, CAST(N'2018-09-25' AS Date), CAST(2222.000 AS Decimal(18, 3)), 1, 1, 5, 1, N'A new transaction to check date picker.', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (53, CAST(N'2018-09-25' AS Date), CAST(110011.000 AS Decimal(18, 3)), 1, 1, 8, 1, N'3rd sep selected', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (54, CAST(N'2018-09-15' AS Date), CAST(25000.000 AS Decimal(18, 3)), 0, 7, 5, 1, N'', 55)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (55, CAST(N'2018-09-25' AS Date), CAST(25000.000 AS Decimal(18, 3)), 0, 5, 7, 1, N'Cancel transaction of TID: 54', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (57, CAST(N'2018-09-25' AS Date), CAST(12200.000 AS Decimal(18, 3)), 1, 7, 1, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (58, CAST(N'2018-09-25' AS Date), CAST(1800.000 AS Decimal(18, 3)), 1, 7, 1, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (59, CAST(N'2018-09-25' AS Date), CAST(1200.000 AS Decimal(18, 3)), 0, 5, 4, 1, N'', 60)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (60, CAST(N'2018-09-25' AS Date), CAST(1200.000 AS Decimal(18, 3)), 0, 4, 5, 1, N'Cancel transaction of TID: 59', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (62, CAST(N'2018-09-25' AS Date), CAST(5400.000 AS Decimal(18, 3)), 0, 5, 15, 1, N'', 63)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (63, CAST(N'2018-09-25' AS Date), CAST(5400.000 AS Decimal(18, 3)), 0, 15, 5, 1, N'Cancel transaction of TID: 62', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (1048, CAST(N'2018-09-26' AS Date), CAST(5200.000 AS Decimal(18, 3)), 1, 5, 7, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (1049, CAST(N'2018-09-26' AS Date), CAST(5200.000 AS Decimal(18, 3)), 1, 2, 7, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (1050, CAST(N'2018-09-26' AS Date), CAST(5200.000 AS Decimal(18, 3)), 1, 2, 8, 1, N'', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (1051, CAST(N'2018-09-26' AS Date), CAST(6000.000 AS Decimal(18, 3)), 0, 5, 3, 1, N'', 1052)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (1052, CAST(N'2018-09-26' AS Date), CAST(6000.000 AS Decimal(18, 3)), 0, 3, 5, 1, N'Cancel transaction of TID: 1051', NULL)
INSERT [dbo].[AllTransactions] ([TransactionID], [TransactionDate], [TransactionAmount], [TransactionStatus], [TransactionCrAccID], [TransactionDrAccID], [TransactionUserID], [TransactionDescription], [TransactionModifiedID]) VALUES (1053, CAST(N'2018-09-26' AS Date), CAST(6000.000 AS Decimal(18, 3)), 1, 5, 4, 1, N'', NULL)
SET IDENTITY_INSERT [dbo].[AllTransactions] OFF
SET IDENTITY_INSERT [dbo].[AllUsers] ON 

INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (1, N'Muhammad Naveed Jamali', CAST(N'2018-06-08' AS Date), N'03024914536', N'Afzal Shah Town, Nawabshah', N'1451265234125', N'naveed', N'naveed', 1, 1)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (2, N'Usman', CAST(N'2018-06-15' AS Date), N'03006633221', N'Sanghar Road, Nawabshah', N'1254362514893', N'usman', N'usman', 2, 1)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (3, N'Kamran', CAST(N'2018-06-23' AS Date), N'03336251487', N'Sakrand Road, Nawabshah', N'5847133662255', N'kamran', N'kamran', 3, 1)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (6, N'Sami', CAST(N'2018-06-06' AS Date), N'0244606014', N'Esarpura, Sarhari, Nawabshah', N'5148723569814', N'sami', N'sami', 4, 1)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (9, N'Ali Ahmed', CAST(N'2018-01-01' AS Date), N'2154326542', N'Sahib Khan Jamali', N'6549876543212', N'ali', N'ali', 2, 1)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (15, N'Ramzan', CAST(N'2018-08-17' AS Date), N'654654654', N'adfadsfadf', N'3216549876541', N'ramzan', N'ramzan', 3, 1)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (16, N'Nadeem Jamali', CAST(N'2018-08-18' AS Date), N'654654654', N'Qazi Ahmed Road, Nawabshah', N'2154873698521', N'nadeem', N'nadeem', 2, 0)
INSERT [dbo].[AllUsers] ([UserID], [Name], [JoiningDate], [PhoneNumber], [Address], [CNIC], [UserName], [Password], [RoleID], [Status]) VALUES (17, N'Qurban Khan', CAST(N'2018-01-01' AS Date), N'326598741', N'ABC Road, XYZ City, KPK', N'8785421965823', N'qurban', N'qurban', 3, 0)
SET IDENTITY_INSERT [dbo].[AllUsers] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (1, N'Panadol', 1, N'A1', CAST(N'2018-01-07T00:00:00.000' AS DateTime), 0, 30, N'Tablet', CAST(2.00 AS Decimal(18, 2)), 138, 2, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (3, N'Panadol CF', 1, N'A2', CAST(N'2018-09-16T00:53:54.580' AS DateTime), 0, 30, N'Capsule', CAST(3.50 AS Decimal(18, 2)), 0, 3, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (4, N'Panadol Extra', 1, N'A3', CAST(N'2018-04-06T00:00:00.000' AS DateTime), 12, 40, N'Tablet', CAST(0.00 AS Decimal(18, 2)), 0, 2, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (6, N'Brufen BioTec', 0, N'B1', CAST(N'2018-09-15T22:01:52.427' AS DateTime), 5, 40, N'Ibuprofen', CAST(0.00 AS Decimal(18, 2)), 0, 1, 4)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (7, N'Calpol', 1, N'C1', CAST(N'2018-09-16T00:53:26.450' AS DateTime), 10, 30, N'Calpol', CAST(35.37 AS Decimal(18, 2)), 0, 1, 2)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (8, N'Tixylix', 1, N'T1', CAST(N'2018-06-13T00:00:00.000' AS DateTime), 0, 26, N'Tixylix', CAST(0.00 AS Decimal(18, 2)), 0, 1, 2)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (9, N'Regex', 1, N'R1', CAST(N'2018-06-13T00:00:00.000' AS DateTime), 5, 26, N'No Description Available', CAST(1.00 AS Decimal(18, 2)), 0, 1, 2)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (10, N'Hashmi Ispaghol', 1, N'I1', CAST(N'2018-09-16T01:42:47.567' AS DateTime), 0, 50, N'Hashmi Ispaghol 7 gram packet for one glass per packet', CAST(7.50 AS Decimal(18, 2)), 0, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (18, N'Colgate Herbal 20grams', 1, N'C1', CAST(N'2018-09-16T01:43:09.667' AS DateTime), 5, 26, N'No Description Available', CAST(52.00 AS Decimal(18, 2)), 0, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (19, N'Medicam 100 gram', 1, N'M1', CAST(N'2018-09-16T01:42:33.743' AS DateTime), 0, 0, N'', CAST(85.00 AS Decimal(18, 2)), 0, 9, 2)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (20, N'Nido Milk 100grams', 1, N'N1', CAST(N'2018-09-16T01:42:19.700' AS DateTime), 0, 0, N'', CAST(25.00 AS Decimal(18, 2)), 0, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (21, N'Marhaba Ispaghol', 1, N'M1', CAST(N'2018-09-16T01:42:06.223' AS DateTime), 0, 0, N'', CAST(5.00 AS Decimal(18, 2)), 0, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [ProductStatus], [RackNumber], [LastModifiedDate], [MinTemperature], [MaxTemperature], [ProductDescription], [ProductPrice], [AvailableStock], [ProductTypeID], [VendorID]) VALUES (22, N'SunyPlast', 1, N'S1', CAST(N'2018-09-16T01:07:17.257' AS DateTime), 0, 0, N'', CAST(2.00 AS Decimal(18, 2)), 0, 4, 4)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[ProductType] ON 

INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (1, N'Syrup', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (2, N'Tablet', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (3, N'Capsule', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (4, N'Surgical', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (5, N'Drops', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (6, N'Paste', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (7, N'Tube', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (8, N'Cream', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (9, N'Tooth Paste', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (10, N'Milk Powder', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (11, N'Ispaghol', 1)
INSERT [dbo].[ProductType] ([ProductTypeID], [ProductTypeName], [ProductTypeStatus]) VALUES (12, N'Acid', 0)
SET IDENTITY_INSERT [dbo].[ProductType] OFF
SET IDENTITY_INSERT [dbo].[ProductVendor] ON 

INSERT [dbo].[ProductVendor] ([VendorID], [ProductVendorName], [ProductVendorStatus]) VALUES (1, N'Suleman Pharma', 1)
INSERT [dbo].[ProductVendor] ([VendorID], [ProductVendorName], [ProductVendorStatus]) VALUES (2, N'Zafa Laboratiries', 1)
INSERT [dbo].[ProductVendor] ([VendorID], [ProductVendorName], [ProductVendorStatus]) VALUES (3, N'Hamdard Labs', 1)
INSERT [dbo].[ProductVendor] ([VendorID], [ProductVendorName], [ProductVendorStatus]) VALUES (4, N'Abbott Labs', 1)
INSERT [dbo].[ProductVendor] ([VendorID], [ProductVendorName], [ProductVendorStatus]) VALUES (5, N'Khayaban Pharma', 1)
INSERT [dbo].[ProductVendor] ([VendorID], [ProductVendorName], [ProductVendorStatus]) VALUES (6, N'Azhar Labs', 1)
SET IDENTITY_INSERT [dbo].[ProductVendor] OFF
SET IDENTITY_INSERT [dbo].[PurchaseDetail] ON 

INSERT [dbo].[PurchaseDetail] ([PurchaseDetailID], [PurchaseOrderID], [ProductID], [Quantity], [Cost], [Status]) VALUES (2, 1, 1, 150, 150, 1)
SET IDENTITY_INSERT [dbo].[PurchaseDetail] OFF
SET IDENTITY_INSERT [dbo].[PurchaseOrder] ON 

INSERT [dbo].[PurchaseOrder] ([PurchaseOrderID], [PurchaseOrderDate], [PurchaseOrderTransactionID], [PurchaseOrderStatus]) VALUES (1, CAST(N'2018-06-01' AS Date), 5, 1)
SET IDENTITY_INSERT [dbo].[PurchaseOrder] OFF
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleID], [Name], [Status]) VALUES (1, N'ADMIN', 1)
INSERT [dbo].[Role] ([RoleID], [Name], [Status]) VALUES (2, N'STOCK CLERK', 1)
INSERT [dbo].[Role] ([RoleID], [Name], [Status]) VALUES (3, N'ACCOUNTANT', 1)
INSERT [dbo].[Role] ([RoleID], [Name], [Status]) VALUES (4, N'SALESMAN', 1)
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[SaleDetail] ON 

INSERT [dbo].[SaleDetail] ([SaleDetailID], [SaleOrderID], [ProductID], [Quantity], [PricePerUnit], [Discount], [Status]) VALUES (1, 1, 1, 8, 1.5, 0, 1)
SET IDENTITY_INSERT [dbo].[SaleDetail] OFF
SET IDENTITY_INSERT [dbo].[SaleOrder] ON 

INSERT [dbo].[SaleOrder] ([SaleOrderID], [Date], [TID], [Status]) VALUES (1, CAST(N'2018-01-07' AS Date), 6, 1)
SET IDENTITY_INSERT [dbo].[SaleOrder] OFF
/****** Object:  Index [UN_AccountName]    Script Date: 10/7/2018 10:42:04 AM ******/
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [UN_AccountName] UNIQUE NONCLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UK_UniqueName]    Script Date: 10/7/2018 10:42:04 AM ******/
ALTER TABLE [dbo].[AccountType] ADD  CONSTRAINT [UK_UniqueName] UNIQUE NONCLUSTERED 
(
	[AccountTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AllUsers]    Script Date: 10/7/2018 10:42:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AllUsers] ON [dbo].[AllUsers]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UN_UniqueProductName]    Script Date: 10/7/2018 10:42:04 AM ******/
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [UN_UniqueProductName] UNIQUE NONCLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UN_UniqueProductTypeName]    Script Date: 10/7/2018 10:42:04 AM ******/
ALTER TABLE [dbo].[ProductType] ADD  CONSTRAINT [UN_UniqueProductTypeName] UNIQUE NONCLUSTERED 
(
	[ProductTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UN_UniqueProductVendorName]    Script Date: 10/7/2018 10:42:04 AM ******/
ALTER TABLE [dbo].[ProductVendor] ADD  CONSTRAINT [UN_UniqueProductVendorName] UNIQUE NONCLUSTERED 
(
	[VendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UN_UniqueUserRoleName]    Script Date: 10/7/2018 10:42:04 AM ******/
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [UN_UniqueUserRoleName] UNIQUE NONCLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllUsers] ADD  CONSTRAINT [DF_User_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_Status]  DEFAULT ((0)) FOR [ProductStatus]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_MinTemperature]  DEFAULT ((5)) FOR [MinTemperature]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_MaxTemperature]  DEFAULT ((26)) FOR [MaxTemperature]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_Description]  DEFAULT ('No Description Available') FOR [ProductDescription]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_Price]  DEFAULT ((0)) FOR [ProductPrice]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_AvailableStock]  DEFAULT ((0)) FOR [AvailableStock]
GO
ALTER TABLE [dbo].[ProductType] ADD  CONSTRAINT [DF_ProductType_ProductTypeStatus]  DEFAULT ((1)) FOR [ProductTypeStatus]
GO
ALTER TABLE [dbo].[ProductVendor] ADD  CONSTRAINT [DF_ProductVendor_ProductVendorStatus]  DEFAULT ((1)) FOR [ProductVendorStatus]
GO
ALTER TABLE [dbo].[PurchaseDetail] ADD  CONSTRAINT [DF_PurchaseDetail_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[PurchaseOrder] ADD  CONSTRAINT [DF_PurchaseOrder_Status]  DEFAULT ((1)) FOR [PurchaseOrderStatus]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_Role_status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[SaleDetail] ADD  CONSTRAINT [DF_SaleDetail_Status_1]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[SaleOrder] ADD  CONSTRAINT [DF_SaleOrder_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([AccountTypeID])
REFERENCES [dbo].[AccountType] ([AccountTypeID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountType]
GO
ALTER TABLE [dbo].[AllTransactions]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_CR] FOREIGN KEY([TransactionCrAccID])
REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[AllTransactions] CHECK CONSTRAINT [FK_Transaction_Account_CR]
GO
ALTER TABLE [dbo].[AllTransactions]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_DR] FOREIGN KEY([TransactionDrAccID])
REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[AllTransactions] CHECK CONSTRAINT [FK_Transaction_Account_DR]
GO
ALTER TABLE [dbo].[AllTransactions]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_User] FOREIGN KEY([TransactionUserID])
REFERENCES [dbo].[AllUsers] ([UserID])
GO
ALTER TABLE [dbo].[AllTransactions] CHECK CONSTRAINT [FK_Transaction_User]
GO
ALTER TABLE [dbo].[AllUsers]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([RoleID])
GO
ALTER TABLE [dbo].[AllUsers] CHECK CONSTRAINT [FK_User_Role]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductType] FOREIGN KEY([ProductTypeID])
REFERENCES [dbo].[ProductType] ([ProductTypeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductType]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductVender] FOREIGN KEY([VendorID])
REFERENCES [dbo].[ProductVendor] ([VendorID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductVender]
GO
ALTER TABLE [dbo].[PurchaseDetail]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseDetail_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PurchaseDetail] CHECK CONSTRAINT [FK_PurchaseDetail_Product]
GO
ALTER TABLE [dbo].[PurchaseDetail]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseDetail_PurchaseOrder] FOREIGN KEY([PurchaseOrderID])
REFERENCES [dbo].[PurchaseOrder] ([PurchaseOrderID])
GO
ALTER TABLE [dbo].[PurchaseDetail] CHECK CONSTRAINT [FK_PurchaseDetail_PurchaseOrder]
GO
ALTER TABLE [dbo].[PurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrder_Transaction] FOREIGN KEY([PurchaseOrderTransactionID])
REFERENCES [dbo].[AllTransactions] ([TransactionID])
GO
ALTER TABLE [dbo].[PurchaseOrder] CHECK CONSTRAINT [FK_PurchaseOrder_Transaction]
GO
ALTER TABLE [dbo].[SaleDetail]  WITH CHECK ADD  CONSTRAINT [FK_SaleDetail_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SaleDetail] CHECK CONSTRAINT [FK_SaleDetail_Product]
GO
ALTER TABLE [dbo].[SaleOrder]  WITH CHECK ADD  CONSTRAINT [FK_SaleOrder_Transaction] FOREIGN KEY([TID])
REFERENCES [dbo].[AllTransactions] ([TransactionID])
GO
ALTER TABLE [dbo].[SaleOrder] CHECK CONSTRAINT [FK_SaleOrder_Transaction]
GO
ALTER TABLE [dbo].[AllTransactions]  WITH CHECK ADD  CONSTRAINT [CK_TransactionAmount] CHECK  (([TransactionAmount]>(0)))
GO
ALTER TABLE [dbo].[AllTransactions] CHECK CONSTRAINT [CK_TransactionAmount]
GO
ALTER TABLE [dbo].[PurchaseDetail]  WITH CHECK ADD  CONSTRAINT [CK_PurchaseCost] CHECK  (([Cost]>=(0)))
GO
ALTER TABLE [dbo].[PurchaseDetail] CHECK CONSTRAINT [CK_PurchaseCost]
GO
ALTER TABLE [dbo].[PurchaseDetail]  WITH CHECK ADD  CONSTRAINT [CK_PurchaseDetail] CHECK  (([Quantity]>=(0)))
GO
ALTER TABLE [dbo].[PurchaseDetail] CHECK CONSTRAINT [CK_PurchaseDetail]
GO
ALTER TABLE [dbo].[SaleDetail]  WITH CHECK ADD  CONSTRAINT [CK_SaleDetailDiscount] CHECK  (([Discount]>=(0)))
GO
ALTER TABLE [dbo].[SaleDetail] CHECK CONSTRAINT [CK_SaleDetailDiscount]
GO
ALTER TABLE [dbo].[SaleDetail]  WITH CHECK ADD  CONSTRAINT [CK_SaleDetailQuantity] CHECK  (([Quantity]>(0)))
GO
ALTER TABLE [dbo].[SaleDetail] CHECK CONSTRAINT [CK_SaleDetailQuantity]
GO
/****** Object:  StoredProcedure [dbo].[AddTransaction]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROC [dbo].[AddTransaction]

@amount decimal,
@cID int,
@date Date,
@desc varchar(50),
@dID int,
@status bit,
@uID int

AS

BEGIN

INSERT INTO [CityPharmacyDB].dbo.AllTransactions(
TransactionAmount,
TransactionCrAccID,
TransactionDate,
TransactionDescription,
TransactionDrAccID,
TransactionStatus,
TransactionUserID)

OUTPUT inserted.TransactionID

VALUES (
@amount,
@cID,
@date,
@desc,
@dID,
@status,
@uID)

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllTransactions]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllTransactions] AS
SELECT * FROM [CityPharmacyDB].[dbo].AllTransactions
Go;
GO
/****** Object:  StoredProcedure [dbo].[GetTransactionByAccountType]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetTransactionByAccountType] @TypeID int
AS
Select * From AllTransactions T,Account A
Where ((A.AccountTypeID=@TypeID) And (T.TransactionCrAccID=A.AccountID OR T.TransactionDrAccID=A.AccountID));
GO
/****** Object:  StoredProcedure [dbo].[UpdateTransactions]    Script Date: 10/7/2018 10:42:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateTransactions]
@modifiedID int,
@amount decimal,
@cID int,
@date Date,
@desc varchar(50),
@dID int,
@status bit,
@uID int

AS

BEGIN

Declare @newID int

INSERT INTO AllTransactions (
TransactionAmount,
TransactionCrAccID,
TransactionDate,
TransactionDescription,
TransactionDrAccID,
TransactionStatus,
TransactionUserID)

OUTPUT inserted.TransactionID

VALUES (
@amount,
@cID,
@date,
@desc,
@dID,
@status,
@uID)

SET @newID = @@IDENTITY

UPDATE AllTransactions

SET

AllTransactions.TransactionStatus=0,
TransactionModifiedID = @newID
WHERE

TransactionID = @modifiedID


END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of Vendor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductVendor', @level2type=N'COLUMN',@level2name=N'ProductVendorName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductVendor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id of User Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'RoleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of User Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status of user role active or inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Status'
GO
USE [master]
GO
ALTER DATABASE [CityPharmacyDB] SET  READ_WRITE 
GO
